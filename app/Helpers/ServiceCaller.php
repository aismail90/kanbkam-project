<?php
/**
 * Created by PhpStorm.
 * User: ahmed
 * Date: 8/3/16
 * Time: 11:52 AM
 */

namespace App\Helpers;


class ServiceCaller //this a service to call api
{

    public function connect($url, $type = 'get', $parameters = null){ //connect to api and return response
        $url = config('constants.API_URL') . $url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $data_json = json_encode($parameters);
        if ($type == 'post') {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $result = $response;
        $result = json_decode($result);

        return $result;
    }



}



