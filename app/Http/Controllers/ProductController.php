<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Config;
use Input;
use Illuminate\Http\Response;
use Exception;


class ProductController extends Controller
{

    private $product;
    private $request;


    public function __construct(Product $product, Request $request)
    {
        $this->product = $product; //object of product model
        $this->request = $request; // object of request to use in all methods
    }

    public function defaultRoute()
    {
        // default route to get the country where user open website
        switch (geoip_country_code3_by_name($this->request->ip())) {
            case "SAU":
                $country = "sa";
                break;
            case "ARE":
                $country = "ae";
                break;
            default:
                $country = "eg";
        }
        $path = route('homepage', ['country' => $country, 'local' => 'en']);
        return redirect($path);
    }

    // action of list of products
    public function index($country = 'eg', $local = 'ar', $category = 'perfumes-fragrances')
    {
        $cachedCategoryArray = \Cache::get($category); //get categories from the cache
        $meattags = config('metatags.index');

        if ($category != 'best-deals' && $category != 'popular') { // add  category and description to  my metatags
            $title = str_replace('#title', $cachedCategoryArray['text'][$local], $meattags[$local]['title']);
            $description = str_replace('#description', $cachedCategoryArray['text'][$local], $meattags[$local]['description']);

        } else {
            $title = str_replace('#title', \Lang::get('trans.layout_' . $category), $meattags[$local]['title']);
            $description = str_replace('#description', \Lang::get('trans.layout_' . $category), $meattags[$local]['description']);

        }

        $metaData['title'] = $title;
        $metaData['description'] = $description;
        $data['metaData'] = $metaData; // add metadata to array of data passed to view


        /////////////////////////////////////////////////////////////
        $brand = $this->request->input('brand'); // get the brand from request
        $price_range = $this->request->input('price_range');
        $price_from = $this->request->input('price_from');
        $price_to = $this->request->input('price_to');
        $price_drop = $this->request->input('price_drop');
        $seller = $this->request->input('seller');
        $target = $this->request->input('target');
        $filterCategory = null;
        if ($category == 'best-deals' || $category == 'popular') {
            $filterCategory = $this->request->input('filter_category');
            $categories = $this->product->getCategoriesOfSearch(null, $country, $brand, $price_range, $price_drop, $seller, $target, $local, $category, $price_from, $price_to);
            $data['categories'] = $categories;
        }

        $brands = $this->product->getBrandsOfSearch(null, $country, $category, $price_range, $price_drop, $seller, $target, $local, $filterCategory, $price_from, $price_to);
        $data['brands'] = $brands;

        ////////////////////////////////////////////////////////////////////

        $sortApi = config('constants.searchSortApi');
        $urls = \cache::get('urls');
        $sort = $this->request->input('sort');
        if ($sort == null || !array_key_exists($sort, $sortApi)) {
            if ($category == 'best-deals') {
                $sort = 'chan_desc';
            } elseif ($category == 'popular') {
                $sort = 'nv_desc';
            } elseif ($this->request->session()->get('sort-type')) {
                $sort = $this->request->session()->get('sort-type');
            } else {
                $sort = 'nv_desc';
            }

        } else {
            if ($category != 'best-deals' && $category != 'popular') {
                $this->request->session()->put('sort-type', $sort);
            }
        }


        $numberOfproductsPerPage = config('constants.PRODUCTS_NUMBER_LIST_PAGE');
        $page = $this->request->input('page', 1);
        $sortType = $sortApi[$sort]['type'];
        $sortField = $sortApi[$sort]['field'];
        $perPage = config('constants.PRODUCTS_NUMBER_LIST_PAGE');

        $productsWithPage = $this->product->getListOfProductsPerPage($country, $page, $local, $category, $sortField, $sortType, $numberOfproductsPerPage, $brand, $price_range, $price_drop, $seller, $target, $filterCategory, $price_from, $price_to);
        if ($productsWithPage) {
            $data['items'] = $productsWithPage;
            $data['selected'] = $sort;

            $cachCategory = \Cache::get($category);

            $data['categoryText'] = $cachCategory['text'][$local];

            $data['urls'] = $urls;
        } else {
            $data['items'] = null;
        }


        return \View::make('products.listing', $data);

    }



    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($country = 'eg', $local = 'ar', $id)
    {
        $searchData = $this->product->getProductByID($id, $country, $local);
        if ($searchData != null) {
            $metaData['title'] = $searchData[0]['title'];
            $metaData['description'] = $searchData[0]['description'];
            $data['detailsMetaData'] = $metaData;
            $data['searchData'] = $searchData;
            $listByArray = array('listed_by_souq', 'listed_by_3rd Party');
            if ($searchData[0]['statsSouqInventory'] == 0 || $searchData[0]['statsOthersInventory'] == 0) {

                if ($searchData[0]['statsOthersInventory'] == 0) {
                    $defaultListValue = 'listed_by_souq';
                } else {
                    $defaultListValue = 'listed_by_3rd Party';
                }
            } else {
                if ($searchData[0]['souqCurrentValue'] <= $searchData[0]['othersCurrentValue']) {
                    $defaultListValue = 'listed_by_souq';
                } else {
                    $defaultListValue = 'listed_by_3rd Party';
                }
            }


            if (\Cookie::get('user_show_product') != $searchData[0]['id']) {
                $increased = $this->product->increaseNumberOfWatched($searchData[0]['id']);
            }
            if (\Session::has('user_data')) {
                $userData = $this->request->session()->get('user_data');
                $user_id = $userData->id;
                $product_id = array();
                array_push($product_id, $searchData[0]['id']);
                $watchData = $this->product->getProductWatchData($user_id, $product_id);
                if ($watchData != null && $watchData[0]->attribute == 'has watch values') {
                    $result = $watchData[0]->result;
                    $data['watchData'] = $result[0]->watch;
                } else {
                    $data['watchData'] = null;
                }
            }
//            $related=array();
//
            $relatedProductsByCategory = $this->product->getListOfProductsByCategory($country, $local, $searchData[0]['category'], $searchData[0]['item_id'], Config::get('constants.numberOFRelatedProducts'));
            $relatedProductsByBrand = $this->product->getListOfProductsByBrand($country, $local, $searchData[0]['brand'], $searchData[0]['item_id'], Config::get('constants.numberOFRelatedProducts'));
            $data['defaultListValue'] = $defaultListValue;
            $data['listByArray'] = $listByArray;
//            $data['related'] = $related;
            $data['local'] = $local;
            $data['relatedProductsByCategory'] = $relatedProductsByCategory;
            $data['relatedProductsByBrand'] = $relatedProductsByBrand;
            $cachCategory = \Cache::get($searchData[0]['category']);
            $data['categoryText'] = $cachCategory['text'][$local];
            $view = \View::make('products.details', $data);
            $response = new Response($view);


        } else {
            $components['id'] = $id;
            $components['type'] = 'merchantLink';
            $id = $this->product->searchProduct($country, $components, $local, null, null, null, null, null, null, null, null, null, null, null, null);

            if (is_int($id)) {
                $path = route('product.show', ['country' => $country, 'local' => $local, 'id' => $id]);
                return redirect($path);
            } else {
                $data['searchData'] = null;
            }

        }

        $view = \View::make('products.details', $data);
        $response = new Response($view);
        $response->withCookie('user_show_product', $searchData[0]['id'], 60);
        return $response;
    }

    public function search($country = 'eg', $local = 'ar')
    {
        $meattags = config('metatags.search');
        $productsCountsPerPage = config('constants.PRODUCTS_NUMBER_SEARCH_PAGE'); //number of products per page (pagination)
        $searchText = $this->request->input('search_text'); //get search text from request
        $searchText = trim($searchText); //remove spaces from search text
        $searchText = urldecode($searchText);


        $urls = \cache::get('urls');
        $category = $this->request->input('filter_category');
        $brand = $this->request->input('brand');
        $price_range = $this->request->input('price_range');
        $price_drop = $this->request->input('price_drop');
        $seller = $this->request->input('seller');
        $target = $this->request->input('target');
        $price_from = $this->request->input('price_from');
        $price_to = $this->request->input('price_to');

        if ($searchText != null) {


            if (isURL($searchText)) {

                $components = getSouqLinkComponetnts($searchText);
                if ($components == 'wrong-url') {
                    $data['items'] = null;
                    $data['categories'] = null;
                    $data['brands'] = null;
                    return \View::make('products.listing', $data);

                } else {
                    if ($country != $components['country']) {
                        $path = route('search', ['country' => $components['country'], 'local' => $local]) . '?search_text=' . $searchText . '';
                        return redirect($path);
                    }
                }
            } elseif (ctype_digit($searchText)) {
                $components['id'] = $searchText;
                $components['type'] = 'merchantLink';

            } else {
//                echo('<pre>');
//                var_dump($searchText);
//                exit;
                $sort = $this->request->input('sort');
                if ($sort == null) {
                    $sort = 'Popularity';
                }
                $data = $this->searchTextAndUnFormattedLink($searchText, $country, $local, $productsCountsPerPage, $category, $brand, $price_range, $price_drop, $seller, $target, $price_from, $price_to);
                $title = str_replace('#search_text', $searchText, $meattags[$local]['title']);
                $description = str_replace('#search_text', $searchText, $meattags[$local]['title']);
                if ($data['items'] == null) {
                    $title = $title . " , no results found";
                    $description = $description . " ,no results found";
                }
                $metaData = $this->setMetaData($category, $brand, $price_range, $price_drop, $seller, $target, $price_from, $price_to, $title, $description);
//                $metaData['title'] = $title;
//                $metaData['description'] = $description;

                $data['metaData'] = $metaData;
//
                $data['urls'] = $urls;

                return \View::make('products.listing', $data);
            }

            $data = $this->product->searchProduct($country, $components, $local, null, null, null, null, null, null, null, null, null, null, null, null);
            if (is_int($data)) {
                $path = route('product.show', ['country' => $country, 'local' => $local, 'id' => $data]);
                return redirect($path);
            } else {

                $data['items'] = null;
                $urls = \cache::get('urls');
                $data['urls'] = $urls;

                return \View::make('products.listing', $data);
            }

        } else {
            $path = route('homepage', ['country' => $country, 'local' => $local]);
            return redirect($path);
        }

    }

    public function addWatchValues()
    {

        $watchValue = $this->request->input('price');
        $object_id = $this->request->input('productId');
        $merchant = $this->request->input('merchant');
        $type = $this->request->input('type');
        $userData = $this->request->session()->get('user_data');
        $user_id = $userData->id;
        $parameters['owner'] = $user_id;
        $merchant = $this->request->input('merchant');
        $parameters['watch'] = $watchValue;
        $parameters['type'] = $type;
        $parameters['merchant'] = $merchant;
        $parameters['product'] = $object_id;
        $parameters['status'] = 0;
        $parameters['date'] = date("d-m-Y H:i:s");
        $response = $this->product->addWatchPriceUserProduct($parameters);
        return response()->json(['response' => $response]);
    }


    public function deleteWatchValues()
    {
        $watchValue = $this->request->input('price');
        $object_id = $this->request->input('productId');
        $merchant = $this->request->input('merchant');
        $type = $this->request->input('type');
        $userData = $this->request->session()->get('user_data');
        $user_id = $userData->id;
        $parameters['owner'] = $user_id;
        $parameters['product'] = $object_id;
        $merchant = $this->request->input('merchant');
        $parameters['watch'] = $watchValue;
        $parameters['type'] = $type;
        $parameters['merchant'] = $merchant;

        $response = $this->product->deleteWatchPriceUserProduct($parameters);
        return response()->json(['response' => $response]);
    }

    public function searchTextAndUnFormattedLink($searchText, $country, $local, $productsCountsPerPage, $category, $brand, $price_range, $price_drop, $seller, $target, $price_from, $price_to)
    {
        $sortApi = config('constants.searchSortApi');
        $sort = $this->request->input('sort');
        if ($sort == null || !array_key_exists($sort, $sortApi)) {
            $sort = 'relevant';
        }
        $sortType = $sortApi[$sort]['type'];
        $sortField = $sortApi[$sort]['field'];
        $page = $this->request->input('page', 1);
        $searchText1 = str_replace("/", "", $searchText);
        $searchText1 = str_replace(" ", "-", $searchText);
        $components['text'] = $searchText1;
        $components['product_count'] = $productsCountsPerPage;
        $data = $this->product->searchProduct($country, $components, $local, $page, $sortField, $sortType, $category, $brand, $price_range, $price_drop, $seller, $target, $price_from, $price_to);

        if ($data != null) {
            $categories = $this->product->getCategoriesOfSearch($searchText1, $country, $brand, $price_range, $price_drop, $seller, $target, $local, null, $price_from, $price_to);

            $brands = $this->product->getBrandsOfSearch($searchText1, $country, $category, $price_range, $price_drop, $seller, $target, $local, $category, $price_from, $price_to);
            $productsWithPage = $data['searchData'];
//            $productsCount = $data['count'];
//            $items = new LengthAwarePaginator($productsWithPage, $productsCount, $productsCountsPerPage, $page, ['path' => $this->request->url(), 'query' => $this->request->query()]);
            $data = [];
            $data['items'] = $productsWithPage;
            $data['searchText'] = $searchText;
            $data['productsCount'] = 20;
            $data['selected'] = $sort;
            $data['categories'] = $categories;
            $data['brands'] = $brands;
        } else {
            $data['items'] = null;
            $data['categories'] = null;
            $data['brands'] = null;

            $data['searchText'] = $searchText;
        }

        return $data;
    }


    public function findUserWatchValues($country = 'eg', $local = 'ar')
    {
        if ($this->request->route()->getName() == 'finduserwatchvalues') {
            $alert = "false";
        } else {
            $alert = "true";
        }
        $page = $this->request->input('page', 1);
        $userData = $this->request->session()->get('user_data');
        $user_id = $userData->id;
        $countOfWatchData = $this->product->getCountUserWatchedProductData($user_id, $country, $local, $alert);

        if (!empty($countOfWatchData)) {
            $count = $countOfWatchData[0]->count;
            $perPage = 6;
            $dataWithPage = $this->product->getUserWatchedProductData($user_id, $country, $page, $perPage, $local, $alert);
            $items = new LengthAwarePaginator($dataWithPage, $count, $perPage, $page, ['path' => $this->request->url(), 'query' => $this->request->query()]);
            $data['items'] = $items;
        } else {
            $data['items'] = null;
        }

        return \View::make('products.alerts', $data);
    }

    public function viewHomePage($country = 'eg', $local = 'ar')
    {
        $homePageLimitNumbers = config('constants.homePageLimitNumbers');
        $defaultCategory = 'perfumes-fragrances';
        $productsOrderedBYPriceDrops = $this->product->getListOfProductsOrderedByPriceDrops($country, $local, $defaultCategory, $homePageLimitNumbers['limitNumberOfCategories']);
//        echo('<pre>');
//        var_dump($productsOrderedBYPriceDrops);
//        exit;
        $productsOrderedBYViews = $this->product->getListOfProductsOrderedByHeighViews($country, $local, $homePageLimitNumbers['limitNumberOfPopular']);
        $data['productsOrderedBYPriceDrops'] = $productsOrderedBYPriceDrops;
        $data['productsOrderedBYViews'] = $productsOrderedBYViews;
        $data['defaultCategory'] = $defaultCategory;
        $this->request->session()->forget('selected_sort_value_home_page');
        return \View::make('products.homepage', $data);
    }


    public function getChartData($data)
    {  // get chart data  (values and dates )
        $returnData = [];
        $dates = [];
        $values = [];
        foreach ($data as $value) {
            if ($value->value != 0) {
                array_push($values, round($value->value));
                array_push($dates, formateDate($value->date));
            }
        }
        if (count($values) < 10) {  // count of data less than ten
            $returnData['dates'] = $dates;
            $returnData['values'] = $values;
        } else {
            $returnData = $this->limitChartValues($values, $dates); //limit the data and choose important values
        }

        return $returnData;

    }


    public function limitChartValues($values, $dates)
    { //choose furthest points

        $differences = array();
        foreach ($values as $k => $v) {  //loop for points
            if ($k != 0) {
                $difference = $values[$k] - $values[$k - 1]; // get the difference between points
                if ($difference < 0) {
                    $difference = $difference * -1;
                }
                array_push($differences, $difference);  //push the differences in array
            }
        }
        uasort($differences, function ($a, $b) {
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        }); // sort the array by key and value

        $numberOfDeleted = count($values) - 10; //number of deleted points
        $i = 0;
        foreach ($differences as $key => $difference) { // remove points form array of values and dates

            if ($i > $numberOfDeleted) {
                break; //break if iterator greater than number of points should be deleted
            } else {
                unset($values[$key]);    // remove the key from array of values
                unset($dates[$key]);     // remove the key from array of dates
            }
            $i += 1;   // increase iterator
        }

        $returnData['dates'] = $dates;         //return array of dates and values
        $returnData['values'] = $values;
        return $returnData;

    }


    // get image of chart -parameters : id of product , type of chart (new  ,used ) and merchant (souq , others)
    public function getChartImage($id, $type, $merchant)
    {  //get chart image of one product with data
        $productPrices = $this->product->getProductPrics($id, $type, $merchant); //get product prices from model
        $data = $this->getChartData($productPrices[0]->product_prices->price->$type->$merchant); //  get the data of chart
        if (!empty($data['values'])) {   // check if this product have data
            $fontName = Config::get('constants.chart_font_path') . 'verdana.ttf'; //get font name from configuration
            $MyData = new \pData();                 // create data of chart (pData library to create image of chart )
            $MyData->addPoints($data['values'], "Label2");
            $MyData->setAxisName(0, "Prices");
            $MyData->addPoints($data['dates'], "Labels");
            $MyData->setSerieDescription("Labels", "Months");
            $MyData->setAbscissa("Labels");
            $serieSettings = array("R" => 1, "G" => 219, "B" => 212);
            $MyData->setPalette("Label2", $serieSettings);
            $myPicture = new \pImage(550, 210, $MyData);
            /* Create the 1st chart*/
//            $myPicture->setGraphArea(20, 20, 550, 190);
            $myPicture->setGraphArea(30, 20, 530, 190);
            // $myPicture->drawFilledRectangle(10,10,548,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
            $myPicture->setFontProperties(array("FontName" => $fontName, "FontSize" => 6));
            $myPicture->drawScale(array("DrawSubTicks" => TRUE));
            $myPicture->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));
            $myPicture->setFontProperties(array("FontName" => $fontName, "FontSize" => 10));
            $myPicture->drawSplineChart(array("DisplayColor" => array("R" => 2, "G" => 2, "B" => 3), "DisplayValues" => TRUE, "DisplayColor" => array("R" => 255, "G" => 255, "B" => 255)));
            $myPicture->setShadow(TRUE);
            $myPicture->stroke();
        } else { // if no data return image of no data available
            $img = \Image::make(Config::get('constants.not_found_image_cahrt_path')); //make image with class Image
            return $img->response();  // return image as response
        }

    }

    public function checkProductPriceHits()
    {
        $productId = $this->request->input('product');
        $merchant = $this->request->input('type');
        $type = $this->request->input('merchant');
        $price = $this->request->input('price');
        $usersWatchThisProduct = $this->product->getUsersWatch($productId, $merchant, $type, $price);
        if (!empty($usersWatchThisProduct)) {
            $productDetails = $this->product->findProductTitle($productId);
            $userIds = [];
            foreach ($usersWatchThisProduct as $user) {
                array_push($userIds, $user->owner);
            }
            $usersString = implode(', ', $userIds);
            $data['productTitle'] = $productDetails->title->en;
            $users = $this->product->findUsersbyId($usersString);
            $usersEmails = [];
            foreach ($users as $user) {
                array_push($usersEmails, $user->email);
            }
            ////////////////////////////////////////////////////////////////
            $subject = 'Price Drop Alert : ' . $data['productTitle'] . '';

            \Mail::send('email_templates.alert', $data, function ($message) use ($usersEmails, $subject) {
                $message->to($usersEmails, $this->request->input('name'))
                    ->subject($subject);
            });


        }


    }


    public function addNewsLetter()
    {
        $email = $this->request->input('email');
        $result = $this->product->addNewsLetter($email);
        if (strpos($result->message, 'A record with that `email` already exists') !== false) {
            $response = '1';
        } elseif ($result->message == 'email inserted successfully') {
            $response = '2';
        } else {
            $response = '3';
        }

        return response()->json(['response' => $response]);

    }


    public function getFilterData()
    {  // ajax get filter data
        $local = $this->request->input('local');
        $category = $this->request->input('category');
        $country = $this->request->input('country');
        $brand = $this->request->input('brand');
        $price_from = $this->request->input('price_from');
        $price_to = $this->request->input('price_to');
        $price_drop = $this->request->input('price_drop');
        $seller = $this->request->input('seller');
        $target = $this->request->input('target');
        $page = $this->request->input('page', 1);
        $sort = $this->request->input('sort');
        $price_range = $this->request->input('price_range');
        $filterCategory = $this->request->input('filter_category');
        $numberOfproductsPerPage = config('constants.PRODUCTS_NUMBER_LIST_PAGE');
        $sortApi = config('constants.searchSortApi');
        $sort = $this->request->input('sort');
        $searchText = $this->request->input('search_text');

        if ($sort == null || !array_key_exists($sort, $sortApi)) {
            if ($searchText) {
                $sort = 'relevant';
            } else {
                if ($category == 'best-deals') {
                    $sort = 'chan_desc';
                } elseif ($category == 'popular') {
                    $sort = 'nv_desc';
                } elseif ($this->request->session()->get('sort-type')) {
                    $sort = $this->request->session()->get('sort-type');

                } else {

                    $sort = 'nv_desc';

                }
            }

        } else {
            if ($category != 'best-deals' && $category != 'popular' && $searchText != null) {
                $this->request->session()->put('sort-type', $sort);
            }
        }
        $sortType = $sortApi[$sort]['type'];
        $sortField = $sortApi[$sort]['field'];

        if ($searchText) {
            $searchText1 = str_replace("/", "", $searchText);
            $searchText1 = str_replace(" ", "-", $searchText1);
            $components['text'] = $searchText1;
            $components['product_count'] = config('constants.PRODUCTS_NUMBER_SEARCH_PAGE');
            $products = $this->product->searchProduct($country, $components, $local, $page, $sortField, $sortType, $filterCategory, $brand, $price_range, $price_drop, $seller, $target, $price_from, $price_to);
            $data['items'] = $products['searchData'];

        } else {
            $products = $this->product->getListOfProductsPerPage($country, $page, $local, $category, $sortField, $sortType, $numberOfproductsPerPage, $brand, $price_range, $price_drop, $seller, $target, $filterCategory, $price_from, $price_to);
            $data['items'] = $products;
        }

        $currencies = Config::get('constants.countries');
        $data['currency'] = $currencies[$country]['currency'][$local];


        $returnHTML = view('partials.loadDataProducts', $data)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));


    }

    public function setMetaData($category, $brand, $price_range, $price_drop, $seller, $target, $price_from, $price_to, $title, $description)
    {
        if ($category && $brand) {
            $title = $title . " In " . $brand . " " . $category;
            $description = $title . " In " . $brand . " " . $category;
        } elseif ($brand) {
            $title = $title . " In " . $brand;
            $description = $title . " In " . $brand;
        } elseif ($category) {
            $title = $title . " In " . $category;
            $description = $title . "In " . $category;
        }
        $metatags['title'] = $title;
        $metatags['description'] = $description;

        return $metatags;
    }


    public function ajaxGetHomePageCategory($country, $local)
    {
        $category = $this->request->input('category');
        $homePageLimitNumbers = config('constants.homePageLimitNumbers');
        $data['defaultCategory'] = $category;
        $data['productsOrderedBYPriceDrops'] = $this->product->getListOfProductsOrderedByPriceDrops($country, $local, $category, $homePageLimitNumbers['limitNumberOfCategories']);
        $returnHTML = view('partials.homePageCategoryData', $data)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));

    }

}



















