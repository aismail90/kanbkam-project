<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/12/17
 * Time: 2:58 PM
 */

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Product;



class SiteMapController extends Controller { //this controller to generate site map for google analytics

    private $product;
    private $request;



    public function __construct(Product $product,Request $request){
        $this->product = $product;
        $this->request = $request;

    }


    public function siteMap($country,$local)
    {
        $sitemap = \App::make("sitemap");
        $limit=$this->request->input('limit');
        $limit_test_result=$this->request->input('limit_test_result');

        // get all products from db
//        $products = $this->product->getProductsForsiteMap($limit,$limit_test_result,$country);
        $products = $this->product->getProductsForsiteMap($limit,$limit_test_result,$country);


//        var_dump(count($products));exit;

        // counters
        $counter = 0;
        $sitemapCounter = intval($this->request->input('sitemap_counter'));

        // add every product to the sitemap
        foreach ($products as $product)
        {
            if ($counter == 50000)
            {
                // generate new sitemap file
                $sitemap->store('xml','Sitemap/sitemap-'.$country.'-'.$local.$sitemapCounter);
                // add the file to the sitemaps array
                $sitemap->addSitemap(secure_url('Sitemap/sitemap-'.$country.'-'.$local.$sitemapCounter.'.xml.gz'));
                // reset items array (clear memory)
                $this->gzCompressFile('/opt/lampp/htdocs/souq-price-indexing/public/Sitemap/sitemap-'.$country.'-'.$local.$sitemapCounter.'.xml');
                unlink('/opt/lampp/htdocs/souq-price-indexing/public/Sitemap/sitemap-'.$country.'-'.$local.$sitemapCounter.'.xml');
                $sitemap->model->resetItems();
                // reset the counter
                $counter = 0;
                // count generated sitemap
                $sitemapCounter++;
            }


//            $title = checkProperty('title', $product);
//            if (!empty($title)) {
//                $localofTitle=$local;
//                if (!property_exists($product->title, $local)) {
//                    if ($local == 'ar') $localofTitle = 'en';
//                    else $localofTitle = 'ar';
//                }
//                $title=$product->title->$localofTitle;
//            }


//            $images = [
//                ['url' => $product->image, 'title' => $title]
//            ];

             $updatedAt=date("Y-m-d H:i:s");
             if(property_exists($product,'updatedAt')){
                 $updatedAt = $product->updatedAt;
             }

            //$sitemap->add(route('product.show',['country'=>$country,'local'=>$local,'number'=>$product->item_id]), $product->updatedAt, '1.0', 'daily');


                $sitemap->add('http://www.kanbkam.com/'.$country.'/'.$local.'/product/show/'.$product->item_id, $updatedAt, '1.0', 'daily');


            $counter++;

        }
        $items=$sitemap->model->getItems();
        if (!empty($items))
        {
            // generate sitemap with last items
            $sitemap->store('xml','Sitemap/sitemap-'.$country.'-'.$local.$sitemapCounter);
            // add sitemap to sitemaps array
            $sitemap->addSitemap(secure_url('Sitemap/sitemap-'.$country.'-'.$local.$sitemapCounter.'.xml.gz'));
            $this->gzCompressFile('/opt/lampp/htdocs/souq-price-indexing/public/Sitemap/sitemap-'.$country.'-'.$local.$sitemapCounter.'.xml');
            unlink('/opt/lampp/htdocs/souq-price-indexing/public/Sitemap/sitemap-'.$country.'-'.$local.$sitemapCounter.'.xml');
            // reset items array
            $sitemap->model->resetItems();
        }

        // generate new sitemapindex that will contain all generated sitemaps above
        $sitemap->store('sitemapindex','sitemap-'.$country.'-'.$local);
        $this->gzCompressFile('/opt/lampp/htdocs/souq-price-indexing/public/sitemap-'.$country.'-'.$local.'.xml');
        unlink('/opt/lampp/htdocs/souq-price-indexing/public/sitemap-'.$country.'-'.$local.'.xml');



    }




    function gzCompressFile($source, $level = 9){
//        var_dump($source);
//        exit;
        $dest = $source . '.gz';
        $mode = 'wb' . $level;
        $error = false;
        if ($fp_out = gzopen($dest, $mode)) {
            if ($fp_in = fopen($source,'rb')) {
                while (!feof($fp_in))
                    gzwrite($fp_out, fread($fp_in, 1024 * 512));
                fclose($fp_in);
            } else {
                $error = true;
            }
            gzclose($fp_out);
        } else {
            $error = true;
        }
        if ($error)
            return false;
        else
            return $dest;
    }

} 