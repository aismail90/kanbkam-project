<?php
/**
 * Created by PhpStorm.
 * User: ahmed
 * Date: 8/7/16
 * Time: 3:44 PM
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
class UserController extends Controller
{
    private $user;
    private $request;
    public function __construct(User $user, Request $request)
    {
        $this->user = $user;
        $this->request = $request;
    }
    public function redirectToProvider()
    {

        return Socialite::driver('facebook')->redirect();
    }
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
            $authUser = $this->findOrCreateUser($user);
            if ($authUser) {
                return redirect(route($this->request->session()->get('nameOfRoute'), $this->request->session()->get('parametersOfRoute')).($this->request->session()->get('queryString') ? '?' : '').$this->request->session()->get('queryString'));
            } else {
                $this->request->session()->put('message', "there is an error try again");
                return redirect(route($this->request->session()->get('nameOfRoute'), $this->request->session()->get('parametersOfRoute')).($this->request->session()->get('queryString') ? '?' : '').$this->request->session()->get('queryString'));
            }
        } catch (Exception $e) {
            return redirect(route($this->request->session()->get('nameOfRoute'), $this->request->session()->get('parametersOfRoute')).($this->request->session()->get('queryString') ? '?' : '').$this->request->session()->get('queryString'));
        }

        return redirect(route($this->request->session()->get('nameOfRoute'), $this->request->session()->get('parametersOfRoute')).($this->request->session()->get('queryString') ? '?' : '').$this->request->session()->get('queryString'));
    }
    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $facebookUser
     * @return User
     */
    private function findOrCreateUser($facebookUser){
        $parameters['facebook_id'] = $facebookUser->id;
        $message = $this->user->checkExistance($parameters, 'facebook');
        if ($message != null && $message[0]->message == 'user founded') {
            $this->request->session()->put('user_data', $message[0]->user);
            return true;
        } else {
            $parameters['fullname'] = $facebookUser->name;
            $parameters['email'] = $facebookUser->email;
            $parameters['status'] = 1;
            $parameters['method'] = 'facebook';
            $message1 = $this->user->confirmRegisteration($parameters);
            if ($message1 != null && ($message1[0]->message == 'user inserted successfully' || $message1[0]->message == 'user updated successfully')) {
                $this->request->session()->put('user_data', $message1[0]->user);
                return true;
            } else {
                return false;
            }
        }
    }
    public function forgot($country, $local){

        if (Session::has('user_data')) {
            return redirect(route('homepage', ['local' => $local, 'country' => $country]));
        }
        else{
            return \View::make('user.forget');
        }

    }

    public function doForget($country, $local){
        if (Session::has('user_data')) {
            return redirect(route('homepage', ['local' => $local, 'country' => $country]));
        }
        else{
            $email = $this->request->input('email');
            $data['email'] = $email;
            if ($email) {
                $parameters['email'] = $email;
                $response = $this->user->checkExistance($parameters, 'forget_info');

                if ($response != null) {
                    if ($response[0]->message == 'user founded and status activated') {
                        $token = bin2hex(random_bytes(25));
                        $data = [
                            'token' => $token ,
                            'name'  => $response[0]->name
                        ];
                        $parameters_token['email'] = $email;
                        $parameters_token['token'] = $token;
                        $response1 = $this->user->insertToken($parameters_token);
                        if ($response1[0]->message == 'user updated sucssecfully') {
                            \Mail::send('email_templates.forgetmail', $data, function ($message) {
                                $message->to($this->request->input('email'), $this->request->input('name'))
                                    ->subject('reset your password');
                            });
                        }
                        $this->request->session()->put('message', "check your message for reset password link");
                    } elseif ($response[0]->message == 'user founded and status not activated') {
                        $this->request->session()->put('message', "your email not activated yet");
                    } else {
                        $this->request->session()->put('message', "this mail doesn't exist");
                    }
                }
            }
            return redirect(route('forget-info', ['local' => $local, 'country' => $country]));
        }
    }

//
    public function updateUserPassword($country, $local, $token = null)
    {
        $response = $this->user->verifyToken($token);
        if ($response[0]->message == 'token founded') {
            $data['data'] = $response[0]->id;
            return \View::make('user.resetpassword', $data);
        } else {
            $this->request->session()->flash('message', "invalid token");
            return redirect(route('forget-info', ['country' => $country, 'local' => $local]));
        }
    }
    public function resetPassword($country, $local)
    {
        $password = $this->request->input('password');
        if($password)
        {
            $id = $this->request->input('user_id');
            $parameters['id'] = $id;
            $parameters['password'] = $password;
            $response = $this->user->updateUserPassword($parameters);
            if ($response != null && $response[0]->message == 'user updated sucssecfully') {
                $this->request->session()->put('message', "update_password_success");
                return redirect(route('registeration', ['country' => $country, 'local' => $local]));
            } else {
                $this->request->session()->put('message', "there is an error");
                return redirect(route('registeration', ['country' => $country, 'local' => $local]));
            }
        }

    }
    public function registeration($country, $local){
        if (Session::has('user_data')) {
            return redirect(route('homepage', ['local' => $local, 'country' => $country]));
        } else {
            return \View::make('user.registeration');
        }
    }
    public function doLogin($country, $local){
        $userParameters['email'] = $this->request->input('email');
        $userParameters['password'] = $this->request->input('password');
        if ($userParameters['email'] != null && $userParameters['password'] != null) {
            $return = $this->user->checkExistance($userParameters, 'normal');
//            echo('<pre>');
//            var_dump($return);
//            exit;
            if ($return != null) {
                if ($return[0]->message == 'user founded') {
                    $this->request->session()->put('user_data', $return[0]->user);
//                    var_dump($this->request->session()->get('parametersOfRoute'));
//                    var_dump($this->request->session()->get('nameOfRoute'));
//                    exit;
//                    return redirect(route('homepage', ['country' => $country, 'local' => $local]));
//                    $message = 'Hello '.($user->is_logged_in() ? $user->get('first_name') : 'Guest');

                    return redirect(route($this->request->session()->get('nameOfRoute'), $this->request->session()->get('parametersOfRoute')).($this->request->session()->get('queryString') ? '?' : '').$this->request->session()->get('queryString'));

                } elseif ($return[0]->message == 'email exist and password mismatch') {
                    $this->request->session()->put('message', \Lang::get('trans.login_error_password_mismatch'));
                } elseif ($return[0]->message == \Lang::get('trans.login_error_mail_not_activated')) {
                    $this->request->session()->put('message', \Lang::get('trans.login_error_mail_not_activated'));
                } else {
                    $this->request->session()->put('message', \Lang::get('trans.login_error_mail_not_found'));
                }
            } else {
                $this->request->session()->put('message', \Lang::get('trans.register_error'));
            }
        }
        return redirect(route('registeration', ['country' => $country, 'local' => $local]));
    }
    public function logout($country, $local){
        $this->request->session()->forget('user_data');
        return redirect(route($this->request->session()->get('nameOfRoute'), $this->request->session()->get('parametersOfRoute')).($this->request->session()->get('queryString') ? '?' : '').$this->request->session()->get('queryString'));
    }
    public function doRegister($country, $local){
        $firstName = $this->request->input('first_name');
        $lastName = $this->request->input('last_name');
        $userParameters['fullname'] = $firstName . " " . $lastName;
        $userParameters['email'] = $this->request->input('email');
        $userParameters['password'] = $this->request->input('password');
        $token = bin2hex(random_bytes(25));
        $data = [
            'token' => $token
        ];
        if ($userParameters['fullname'] != null && $userParameters['email'] != null && $userParameters['password'] != null) {
            $userParameters['status'] = 0;
            $userParameters['token'] = $token;
            $userParameters['method'] = 'normal';
            $return = $this->user->confirmRegisteration($userParameters);
            if ($return != null && $return[0]->message == 'user inserted successfully') {
                \Mail::send('email_templates.welcome', $data, function ($message) {
                    $message->to($this->request->input('email'), $this->request->input('name'))
                        ->subject('Verify your email address');
                });
                $this->request->session()->put('message', 'register_success');
            } elseif ($return != null && strpos($return[0]->message, '`email` already exists') !== false) {
                $this->request->session()->put('message', \Lang::get('trans.register_error_email_exist'));
            } else {
                $this->request->session()->put('message', \Lang::get('trans.register_error'));
            }
            return redirect(route('registeration', ['country' => $country, 'local' => $local]));
        }
    }
    function verify($country, $local, $token = NULL){
        $return = $this->user->verifyEmailID($token);
        if ($return != null && $return[0]->message == 'user activated sucssecfully') {
            $this->request->session()->put('register_message', 'Your Email Address is successfully verified! Please login to access your account!');
            return redirect(route('registeration', ['country' => $country, 'local' => $local]));
        } else {
            $this->request->session()->put('register_message', 'Sorry! invalid token!');
            return redirect(route('registeration', ['country' => $country, 'local' => $local]));
        }
    }

} 