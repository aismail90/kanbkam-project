<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use App\Product;
use Illuminate\Support\Facades\Config;

class Language  {

    public function __construct(Application $app, Redirector $redirector, Request $request,Product $product) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
        $this->product=$product;
    }



    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $country = $request->segment(1);
        $locale = $request->segment(2);
        $parametersOfRoute=$request->route()->parameters();
        $nameOfRoute=$request->route()->getName();
        $queryString=$request->getQueryString();

        if($request->route()->getName()!='doRegister' && $request->route()->getName()!='registeration'&&$request->route()->getName()!='doLogin'&&$request->route()->getName()!='logout'&&$request->route()->getName()!='getHomePageCategory'
            &&$request->route()->getName()!='forgetPassword'&&$request->route()->getName()!='forgetuserinfo' &&$request->route()->getName()!='resetPassword' && $request->route()->getName()!='doResetPassword' && array_key_exists($country,config('constants.countries')) && in_array($locale,config('constants.languages') )){
            $nameOfRoute=$request->route()->getName();
            \Session::put('parametersOfRoute', $parametersOfRoute);
            \Session::put('nameOfRoute', $nameOfRoute);
            \Session::put('queryString', $queryString);
        }

        if(!array_key_exists($country,config('constants.countries')) || !in_array($locale,config('constants.languages') ))
        {
        if(!array_key_exists($country,config('constants.countries') ))
        {

            return redirect(route($this->request->session()->get('nameOfRoute'), $this->request->session()->get('parametersOfRoute')).($this->request->session()->get('queryString') ? '?' : '').$this->request->session()->get('queryString'));
        }
        if(!in_array($locale,config('constants.languages')))
        {
            return redirect(route($this->request->session()->get('nameOfRoute'), $this->request->session()->get('parametersOfRoute')).($this->request->session()->get('queryString') ? '?' : '').$this->request->session()->get('queryString'));
        }
        }
        if (\Session::has('user_data')) {
            $userData = $this->request->session()->get('user_data');
            if(is_array($userData)){
                $user_id=$userData[0]->id;
            }
            else{
                $user_id = $userData->id;
            }
            $countOfWatchData=$this->product->getCountUserWatchedProductData($user_id,$country,$locale,'true');
            if(!empty($countOfWatchData))
            {
                \View::share('countOfAlerts', $countOfWatchData);

            }
            else{
                \View::share('countOfAlerts', null);
            }
        }


        if(array_key_exists($request->route()->getName(),Config::get('metatags')))
        {
        $metaTags=Config::get('metatags.'.$request->route()->getName());
        $metaData['title'] = $metaTags[$locale]['title'];
        $metaData['description'] = $metaTags[$locale]['description'];

            \View::share('metaData', $metaData);


        }
        else{
            \View::share('metaData', null);
        }


        $currencies = Config::get('constants.countries');

        $currency = $currencies[$country]['currency'][$locale];

        \View::share('local', $locale);
        \View::share('currency', $currency);
        $this->app->setLocale($locale);

        if(!\Cache::has('urls'))
        {

        $categories= $this->product->getAllCategories();
        $urls =array();
        foreach($categories as $category)
        {
            array_push($urls,$category->url);
            $textArray= $category->text;
            $textLanguageArray=[];
            $parentLanguageArray=[];
            foreach($textArray as $value){
                if(is_object($value)){
                    $textLanguageArray['en'] = $value->text;
                }
                else{
                    $textLanguageArray['ar'] = $value[0]->text;
                }

            }

            $parentArray=$category->parent_category;
            foreach($parentArray as $value){

                if(is_object($value)){
                    $parentLanguageArray['en'] = $value->text;
                }
                else{
                    $parentLanguageArray['ar'] = $value[0]->text;
                }

            }


            if(array_key_exists('ar',$parentLanguageArray)&&array_key_exists('en',$parentLanguageArray)){
                $cachedArray=array('text'=>array('en'=>$textLanguageArray['en'] , 'ar'=>$textLanguageArray['ar']),'parent'=>array('en'=>$parentLanguageArray['en'] ,'ar'=>$parentLanguageArray['ar'])) ;

            }
            \Cache::forever($category->url,$cachedArray);
        }
            \Cache::forever('urls',$urls);

        }


        $url = Config::get('constants.json_files_path').'EGY_categories.json';
        $content = file_get_contents($url);
        $json = json_decode($content, true);
        \View::share('menu_json', $json);
        \View::share('parameters', $parametersOfRoute);



        return $next($request);
    }

}