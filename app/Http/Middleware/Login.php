<?php

namespace App\Http\Middleware;

use Closure;

class Login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if($request->session()->has('user_data'))
        return $next($request);
        else
        return redirect(route('homepage', ['country' => $request->segment(1), 'local' => $request->segment(2)])) ;
    }
}
