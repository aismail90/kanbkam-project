<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class LogoutMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::get('name') != config('constants.USER_NAME') && Session::get('password') != config('constants.PASSWORD')) {
            $response= $next($request);
            return $response->header('Cache-Control','nocache, no-store, max-age=0, must-revalidate')
                ->header('Pragma','no-cache') //HTTP 1.0
                ->header('Expires','Sat, 01 Jan 1990 00:00:00 GMT'); // // Date in the past
    }
        else{
            return redirect(route('product.index', array('local' => 'ar')));
        }
    }
}