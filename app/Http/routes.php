<?php



Route::pattern('number', '[0-9]+');
Route::group(['middleware' => ['web']], function () {
    Route::get('/auth/facebook', ['as' => 'provider', 'uses' => 'UserController@redirectToProvider']); //route to redirct to Socialite provider
    Route::get('/auth/facebook/callback', ['as' => 'authfacebook', 'uses' => 'UserController@handleProviderCallback']); //handle facebook login
    Route::post('/sumbitRegisterData', ['as' => 'sumbit', 'uses' => 'UserController@sumbitDataAndSendMail']);
    Route::match(['get', 'post'], '/', ['as' => 'default', 'uses' => 'ProductController@defaultRoute']);
    Route::match(['get', 'post'], '/getChartImage/{id}/{type}/{merchant}', ['as' => 'getChartImage', 'uses' => 'ProductController@getChartImage']);
    Route::match(['get', 'post'], '/userproduct/checkproductpricehits', ['as' => 'checkproductpricehits', 'uses' => 'ProductController@checkProductPriceHits']);
    Route::match(['get', 'post'],'/addnewsletter', ['as' => 'addnewsletter', 'uses' => 'ProductController@addNewsLetter']); //ajax add news letter
    Route::match(['get', 'post'],'/getFilterData', ['as' => 'getFilterData', 'uses' => 'ProductController@getFilterData']); //ajax add news letter
    Route::match(['get', 'post'],'/auto', ['as' => 'auto', 'uses' => 'ProductController@test']); //ajax auto complete

});
Route::group(['middleware' => ['web', 'Login']], function () {
    Route::match(['get', 'post'], '/addWatchValues', ['as' => 'addWatchValues', 'uses' => 'ProductController@addWatchValues']); //ajax add watch values of user's product
    Route::post('/deleteWatchValues', ['as' => 'deleteWatchValues', 'uses' => 'ProductController@deleteWatchValues']); //ajax delete watch values of user's product

});
Route::group(['prefix' => '{country}/{local}','middleware' => ['web', 'Login','Language']], function () {
    Route::get('/watchedProducts', ['as' => 'finduserwatchvalues',
        'uses' => 'ProductController@findUserWatchValues']);
    Route::get('/alerts', ['as' => 'finduseralerts',
        'uses' => 'ProductController@findUserWatchValues']);
});


Route::group(['prefix' => '{country}/{local}', 'middleware' => ['web', 'Language']], function () {
    Route::match(['get', 'post'], '/{category}/list', ['as' => 'index',
        'uses' => 'ProductController@index']);
    Route::match(['get', 'post'], '/home', ['as' => 'homepage', 'uses' => 'ProductController@viewHomePage']);
    Route::match(['get', 'post'],'/search', ['as' => 'search',
        'uses' => 'ProductController@search']);
    Route::match(['get', 'post'], '/register', ['as' => 'registeration', 'uses' => 'UserController@registeration']);
    Route::match(['get', 'post'], '/login', ['as' => 'doLogin', 'uses' => 'UserController@doLogin']);
    Route::match(['get', 'post'], '/doRegister', ['as' => 'doRegister', 'uses' => 'UserController@doRegister']);
    Route::match(['get', 'post'], '/logout', ['as' => 'logout', 'uses' => 'UserController@logout']);
    Route::match(['get', 'post'], '/verify/{token}', ['as' => 'confirmRegisteration', 'uses' => 'UserController@verify']);
    Route::match(['get', 'post'], '/forgetuserinfo', ['as' => 'forget-info', 'uses' => 'UserController@forgot']);
    Route::match(['get', 'post'], '/doForget', ['as' => 'doForget', 'uses' => 'UserController@doForget']);
    Route::match(['get', 'post'], '/forgetPassword/{token}', ['as' => 'forgetPassword', 'uses' => 'UserController@updateUserPassword']);
    Route::match(['get', 'post'], '/product/show/{number?}', ['as' => 'product.show',
        'uses' => 'ProductController@show']);
    Route::match(['get', 'post'], '/resetPassword', ['as' => 'resetPassword', 'uses' => 'UserController@resetPassword']);
    Route::match(['get', 'post'], '/getHomePageCategory', ['as' => 'getHomePageCategory', 'uses' => 'ProductController@ajaxGetHomePageCategory']);
    Route::match(['get', 'post'],'/generateSitemap', ['as' => 'sitemap', 'uses' => 'SiteMapController@siteMap']); //ajax add news letter





});