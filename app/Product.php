<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class Product extends Model
{
    private $countries;
    private $currencies;

    public function __construct()
    {
        $this->countries = config('constants.countries');
        $this->currencies = config('constants.currencies');
    }


    public function  getProductByID($id, $country, $local)
    {
        $url = "/product/findProductWithHist?item_id=" . $id . "&country=" . $country . "";
        $productData = app('ApiCall')->connect($url);

        $data[0] = $productData;
        $type = 'details';
        $data = $this->excludeData($data, $local, $country, $type);
        return $data;
    }

    public function searchProduct($country, $component, $local, $skip, $sortField, $sortType, $category, $brand, $price_range, $price_drop, $seller, $target, $price_from, $price_to)
    {
        if (array_key_exists('text', $component)) {
            $urlSearchData = "/product/searchProduct?country=" . $this->countries[$country]['country'] . "&text=" . urlencode($component['text'])
                . "&category=" . $category . "&brand=" . urlencode($brand)
                . "&skip=" . $skip . "&sortField=" . $sortField . "&sortType=" . $sortType
                . "&limit=" . $component['product_count']
                . "&price_range=" . $price_range . "&price_drop=" . $price_drop
                . "&seller=" . $seller . "&target=" . ucfirst($target) . "&locale=" . $local
                . "&price_from=" . $price_from . "&price_to=" . $price_to .
                "&target_keywords=" . urlencode(config('target.keywords'));

            $searchData = app('ApiCall')->connect($urlSearchData);
            if ($searchData) {
                $data['searchData'] = $this->excludeData($searchData, $local, $country);
            } else {
                $data = null;
            }


        } else {
            $findIDUrl = '/product/searchProduct?type=' . $component['type'] . '&id=' . $component['id'] . '&country=' . $country;
            $productData = app('ApiCall')->connect($findIDUrl);
            $data = $productData;
        }

        return $data;
    }

    public function getListOfProductsPerPage($country, $skip, $local, $category, $sort, $sortType, $numberOfproductsPerPage, $brand, $price_range, $price_drop, $seller, $target, $filter_category, $price_from, $price_to)
    {
        $url = "/product/findAll?country=" . $this->countries[$country]['country'] . "&skip=" . $skip . "&category="
            . urlencode($category) . "&sortField=" . $sort . "&sortType=" . $sortType . "&limit=" . $numberOfproductsPerPage
            . "&price_range=" . $price_range . "&price_drop=" . $price_drop
            . "&seller=" . $seller . "&target=" . ucfirst($target) . "&locale=" . $local . "&brand=" . urlencode($brand)
            . "&filter_category=" . urlencode($filter_category)
            . "&price_from=" . $price_from . "&price_to=" . $price_to .
            "&target_keywords=" . urlencode(config('target.keywords'));
        $productsWithPage = app('ApiCall')->connect($url);
        $data = $this->excludeData($productsWithPage, $local, $country);
        return $data;
    }


    public function getCountOfProducts($country, $category, $local, $brand, $price_range, $price_drop, $seller, $target, $filter_category, $price_from, $price_to)
    {

        $url = "/product/countAll?country=" . $this->countries[$country]['country'] . "&category=" . urlencode($category)
            . "&price_range=" . $price_range . "&price_drop=" . $price_drop
            . "&seller=" . $seller . "&target=" . ucfirst($target) . "&locale=" . $local . "&brand=" . urlencode($brand)
            . "&filter_category=" . urlencode($filter_category)
            . "&price_from=" . $price_from . "&price_to=" . $price_to;
        $countOfProducts = app('ApiCall')->connect($url) .
            "&target_keywords=" . urlencode(config('target.keywords'));
        return $countOfProducts;
    }


    public function getCategoriesOfSearch($text, $country, $brand, $price_range, $price_drop, $seller, $target, $local, $category, $price_from, $price_to)
    {
        $url = '/product/getCategoryOfSearch?country=' . $this->countries[$country]['country'] . "&text=" . urlencode($text)
            . "&brand=" . urlencode($brand)
            . "&price_range=" . $price_range . "&price_drop=" . $price_drop
            . "&seller=" . $seller . "&target=" . ucfirst($target) . "&locale=" . $local . "&category=" . $category
            . "&price_from=" . $price_from . "&price_to=" . $price_to .
            "&target_keywords=" . urlencode(config('target.keywords'));
        $response = app('ApiCall')->connect($url);
        return $response;
    }

    public function getBrandsOfSearch($text, $country, $category, $price_range, $price_drop, $seller, $target, $locale, $filter_category, $price_from, $price_to)
    {
        $url = '/product/getbrandsOfSearch?country=' . $this->countries[$country]['country'] . "&text=" . urlencode($text)
            . "&category=" . $category
            . "&price_range=" . $price_range . "&price_drop=" . $price_drop
            . "&seller=" . $seller . "&target=" . ucfirst($target) . "&locale=" . $locale
            . "&filter_category=" . urlencode($filter_category)
            . "&price_from=" . $price_from . "&price_to=" . $price_to .
            "&target_keywords=" . urlencode(config('target.keywords'));
        $response = app('ApiCall')->connect($url);
        return $response;
    }


    public function addWatchPriceUserProduct($parameters)
    {
        $type = 'post';
        $url = "/userproduct/updateUserProducts";
//        var_dump($parameters);
//        exit;
        $response = app('ApiCall')->connect($url, $type, $parameters);
        return $response;

    }

    public function deleteWatchPriceUserProduct($parameters)
    {
        $type = 'post';
        $url = "/userproduct/removeUserProducts";
        $response = app('ApiCall')->connect($url, $type, $parameters);
        return $response;

    }


    public function getProductWatchData($user_id, $product_id)
    {
        $parameters['user_id'] = $user_id;
        $parameters['product_ids'] = $product_id;
        $type = 'post';
        $url = "/userproduct/getwatchValuesofProduct";
        $response = app('ApiCall')->connect($url, $type, $parameters);
        return $response;

    }

    public function getUserWatchedProductData($user_id, $country, $skip, $limit, $local, $alert)
    {

        if (array_key_exists($country, $this->countries)) {
            $url = "/userproduct/getUserProducts?owner=" . $user_id . "&country=" . $this->countries[$country]['country'] . "&skip=" . $skip . "&limit=" . $limit . "&alert=" . $alert . "";
            $response = app('ApiCall')->connect($url);
            return $response;
        }

    }

    public function getCountUserWatchedProductData($user_id, $country, $local, $alert)
    {
        $url = "/userproduct/countUserProducts?owner=" . $user_id . "&country=" . $this->countries[$country]['country'] . "&alert=" . $alert . "";
        $response = app('ApiCall')->connect($url);
        return $response;

    }


    public function getListOfProductsOrderedByPriceDrops($country, $local, $category, $limit)
    {
        $url = "/product/getProductSortedByChangePercent?country=" . $this->countries[$country]['country'] . "&category=" . urlencode($category) . "&limit=" . $limit . "";
        $response = app('ApiCall')->connect($url);
        $data = $this->excludeData($response, $local, $country);
        $cachedData = \Cache::put('price_drops_products', $data, 10);
        return $data;
    }

    public function increaseNumberOfWatched($productId)
    {
        $url = "/product/increaseNumberofShowOfProduct?id=" . $productId;
        $response = app('ApiCall')->connect($url);
        return $response;
    }

    public function getListOfProductsOrderedByHeighViews($country, $local, $limit)
    {
        $url = '/product/getProductsWithHeighWatchData?country=' . $this->countries[$country]['country'] . "&limit=" . $limit . "";
        $response = app('ApiCall')->connect($url);
        $data = $this->excludeData($response, $local, $country);
        return $data;
    }

    public function getListOfProductsByCategory($country, $local, $category, $id, $limit)
    {
        $url = '/product/getProductsbyCategory?country=' . $this->countries[$country]['country'] . "&category=" . urlencode($category) . "&item_id=" . $id . "&limit=" . $limit . "";
        $response = app('ApiCall')->connect($url);
        $data = $this->excludeData($response, $local, $country);
        return $data;
    }

    public function getListOfProductsByBrand($country, $local, $brand, $id, $limit)
    {
        $url = '/product/getProductsbyBrand?country=' . $this->countries[$country]['country'] . "&brand=" . urlencode($brand) . "&local=" . $local . "&limit=" . $limit . "&item_id=" . $id . "";
        $response = app('ApiCall')->connect($url);
        $data = $this->excludeData($response, $local, $country);
        return $data;
    }

    public function getAllCategories()
    {
        $url = '/category/findAllCategories';
        $response = app('ApiCall')->connect($url);
        return $response;
    }


    public function getProductPrics($id, $type, $merchant) {
        $url = '/product/getProductPrices?id=' . $id . '&type=' . $type . "&merchant=" . $merchant . ""; //url to call api
        $response = app('ApiCall')->connect($url);  // call service that connect to call api
        return $response;   // return response

    }


    public function getUsersWatch($productId, $merchant, $type, $price)
    {
        $url = '/userproduct/findUsersWatchProduct?product=' + $productId + "&merchant=" + $merchant + "&type=" + $type + "&price=" + $price + "";
        $response = app('ApiCall')->connect($url);
        return $response;
    }

    public function findProductTitle($productId)
    {
        $url = '/product/findOneProd?id=' . $productId . "";
        $response = app('ApiCall')->connect($url);
        return $response;
    }

    public function findUsersbyId($usersString)
    {
        $url = '/user/findUsersbyId?owners=' . $usersString . '';
        $response = app('ApiCall')->connect($url);
        return $response;

    }

    public function addNewsLetter($email)
    {
        $url = '/newsletter/addNewsLetter?email=' . $email . "";
        $response = app('ApiCall')->connect($url);
        return $response;

    }

    public function getMatchProducts($text)
    {
        $url = '/product/autoComplete?text=' . urlencode($text) . "";
        $response = app('ApiCall')->connect($url);
        return $response;

    }

    public function getProductsForsiteMap($limit,$limit_test_result,$country)
    {

        $url = "/product/getSitemapData?limit=".$limit."&limit_test_result=".$limit_test_result."&country=".$this->countries[$country]['country'];
        $productData = app('ApiCall')->connect($url);
        return $productData;
    }


    public function excludeData($products, $local, $country, $type = 'normal')
    {
        $status = config('constants.productStatus');
        $data = array();
        if (isset($products)) {
            foreach ($products as $product) {
                $pieceOfData['category'] = checkProperty('category', $product);
                if ($type != 'details') {
                    if (property_exists($product, 'id')) {
                        $pieceOfData['id'] = $product->id;
                    } else {
                        $pieceOfData['id'] = $product->_id;
                    }
                }
                $pieceOfData['item_id'] = checkProperty('item_id', $product);
                $pieceOfData['souqLink'] = 'http://' . $this->countries[$country]['headlink'] . '.souq.com/' . $country . '-' . $local . '/-' . $pieceOfData['item_id'];
                $title = checkProperty('title', $product);
                $fullTitle = "";
                if (!empty($title)) {
                    if (!property_exists($product->title, $local)) {
                        if ($local == 'ar') $local = 'en';
                        else $local = 'ar';
                    }
                    $title = str_limit($product->title->$local, 55);
                    $fullTitle = $product->title->$local;

                }
                $pieceOfData['title'] = $title;
                $pieceOfData['fullTitle'] = $fullTitle;
                $brand = checkProperty('brand', $product);
                if (!empty($brand)) {
                    $brand = $product->brand->$local;
                    $pieceOfData['brand'] = $brand;
                }
                $stats = checkProperty('stats', $product);
                $bestValues = checkProperty('best_prices', $product);
                if (!empty($bestValues)) {


                    if (is_object($product->best_prices->new->price)) {
                        if ($product->best_prices->new->price->value == 0) {
                            $pieceOfData['currentValue'] = $status['out_of_stock'];
                            if (is_object($product->best_prices->new->change_percent)) {
                                $pieceOfData['changePercent'] = $product->best_prices->new->change_percent->value;

                            } else {
                                $pieceOfData['changePercent'] = $product->best_prices->new->change_percent;

                            }
                        } else {
                            $pieceOfData['currentValue'] = $product->best_prices->new->price->value;
                            if (is_object($product->best_prices->new->change_percent)) {
                                $pieceOfData['changePercent'] = $product->best_prices->new->change_percent->value;

                            } else {
                                $pieceOfData['changePercent'] = $product->best_prices->new->change_percent;

                            }
                        }

                    } else {
                        if ($product->best_prices->new->price == 0) {
                            $pieceOfData['currentValue'] = "1";
                            if (is_object($product->best_prices->new->change_percent)) {
                                $pieceOfData['changePercent'] = $product->best_prices->new->change_percent->value;

                            } else {
                                $pieceOfData['changePercent'] = $product->best_prices->new->change_percent;

                            }
                        } else {
                            $pieceOfData['currentValue'] = $product->best_prices->new->price;
                            if (is_object($product->best_prices->new->change_percent)) {
                                $pieceOfData['changePercent'] = $product->best_prices->new->change_percent->value;

                            } else {
                                $pieceOfData['changePercent'] = $product->best_prices->new->change_percent;

                            }
                        }
                    }
                } else {
                    $pieceOfData['currentValue'] = "";
                    $pieceOfData['changePercent'] = "";

                }
                if (!empty($stats)) {

                    if ($product->stats->new->souq->current->value != 0) {
                        $pieceOfData['souqCurrentValue'] = $product->stats->new->souq->current->value;
                        $pieceOfData['souqChangePercent'] = $product->stats->new->souq->change_percent;
                    } else {
                        $pieceOfData['souqCurrentValue'] = $status['out_of_stock'];
                        $pieceOfData['souqChangePercent'] = 0;
                    }
                    if ($product->stats->new->others->current->value != 0) {
                        $pieceOfData['othersCurrentValue'] = $product->stats->new->others->current->value;
                        $pieceOfData['othersChangePercent'] = $product->stats->new->others->change_percent;
                    } else {
                        $pieceOfData['othersCurrentValue'] = $status['out_of_stock'];
                        $pieceOfData['othersChangePercent'] = 0;
                    }
                    $pieceOfData['newSouqStats'] = $stats->new->souq;
                    $pieceOfData['newOthersStats'] = $stats->new->others;
                    $pieceOfData['statsSouqInventory'] = $stats->new->souq->inventory;
                    $pieceOfData['statsOthersInventory'] = $stats->new->others->inventory;

                } else {
                    $pieceOfData['souqCurrentValue'] = "";
                    $pieceOfData['othersCurrentValue'] = "";
                    $pieceOfData['othersCurrentValue'] = "";
                    $pieceOfData['othersCurrentValue'] = "";
                    $pieceOfData['newSouqStats'] = null;
                    $pieceOfData['newOthersStats'] = null;
                    $pieceOfData['statsSouqInventory'] = null;
                    $pieceOfData['statsOthersInventory'] = null;
                }
                $pieceOfData['image'] = checkProperty('image', $product);
                if ($type == 'details') {
                    $description = checkProperty('description', $product);
                    if (!empty($description))
                        if (property_exists($product->description, $local)) {
                            if ($type != 'details') {
                                $description = str_limit($product->description->$local, 160);

                            } else {
                                $description = $product->description->$local;
                            }
                        } else {
                            if ($local == 'ar') $local = 'en';
                            else $local = 'ar';
                            $description = str_limit($product->description->$local, 160);
                        }
                    $pieceOfData['description'] = $description;
                    $pieceOfData['id'] = $product->_id;
                    $pieceOfData['specification'] = checkProperty('specifications', $product, $local);
                    $prouctPrices = checkProperty('product_prices', $product);
                    if (!empty($prouctPrices)) {
                        $pieceOfData['souqPrices'] = $prouctPrices[0]->price->new->souq;
                        $pieceOfData['otherPrices'] = $prouctPrices[0]->price->new->others;
                        $pieceOfData['souqInventory'] = $prouctPrices[0]->inventory->new->souq;
                        $pieceOfData['othersInventory'] = $prouctPrices[0]->inventory->new->others;
                    } else {
                        $pieceOfData['souqPrices'] = 'NA';
                        $pieceOfData['souqInventory'] = 'NA';
                        $pieceOfData['othersInventory'] = 'NA';
                        $pieceOfData['otherPrices'] = 'NA';
                    }

                }

                array_push($data, $pieceOfData);
            }
        }


        return $data;


    }

}
