<?php

namespace App\Providers;

use App\Helpers\ServiceCaller;
use Illuminate\Support\ServiceProvider;

class CallerServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
            $this->app->singleton('ApiCall', function ($app) {
                return new ServiceCaller();
            });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['App\Helpers\ServiceCaller'];
    }
}
