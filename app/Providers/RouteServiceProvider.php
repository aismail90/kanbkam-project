<?php
//
//namespace App\Providers;
//
//use Illuminate\Routing\Router;
//use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
//use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Config;
//
//class RouteServiceProvider extends ServiceProvider
//{
//    /**
//     * This namespace is applied to your controller routes.
//     *
//     * In addition, it is set as the URL generator's root namespace.
//     *
//     * @var string
//     */
//    protected $namespace = 'App\Http\Controllers';
//
//    /**
//     * Define your route model bindings, pattern filters, etc.
//     *
//     * @param  \Illuminate\Routing\Router  $router
//     * @return void
//     */
//    public function boot(Router $router)
//    {
//        //
//
//        parent::boot($router);
//    }
//
//    /**
//     * Define the routes for the application.
//     *
//     * @param  \Illuminate\Routing\Router  $router
//     * @return void
//     */
//    public function map(Router $router,Request $request)
//    {
//
//        $country = $request->segment(1);
//
//        $locale = $request->segment(2);
//
//        if(!array_key_exists($country,config('constants.countries') ))
//
//        {
//            $country='eg';
//        }
//        if(!in_array($locale,config('constants.languages')))
//        {
//            $locale='ar';
//        }
//        var_dump($country);
//        var_dump($locale);
////        exit;
//        $this->app->setLocale($locale);
//
//        $router->group(['namespace' => $this->namespace, 'prefix' => $country.'/'.$locale], function($router) {
//            require app_path('Http/routes.php');
//        });
//        $this->mapWebRoutes($router);
//
//        //
//    }
//
//    /**
//     * Define the "web" routes for the application.
//     *
//     * These routes all receive session state, CSRF protection, etc.
//     *
//     * @param  \Illuminate\Routing\Router  $router
//     * @return void
//     */
//    protected function mapWebRoutes(Router $router)
//    {
//        $router->group([
//            'namespace' => $this->namespace, 'middleware' => 'web',
//        ], function ($router) {
//            require app_path('Http/routes.php');
//        });
//    }
//}


namespace App\Providers;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //
        parent::boot($router);
    }
    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router ,Request $request)
    {
        $this->mapWebRoutes($router);
//        $metaTags=\Config::get('constants.metaTags');
//        if(array_key_exists($request->route()->getName(),$metaTags))
//        {
//            \View::share('metaData', $metaTags[$request->route()->getName()][$locale]);
//        }
//        else{
//            \View::share('metaData', null);
//        }
        //
    }
    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}

