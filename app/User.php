<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public function confirmRegisteration($parameters){


        $url ="/user/insertData";
        $type='post';

        $response = app('ApiCall')->connect($url,$type,$parameters) ;

        return $response;

    }
    public function  checkExistance($parameters,$logintype){
        if($logintype=='normal'){
            $url ="/user/login";
        }
        elseif($logintype=='facebook'){
            $url="/user/checkExistanceForFacebokID";

        }
        else{
            $url="/user/checkExistanceForEmailForForgetPassword";
        }


        $type='post';
        $response = app('ApiCall')->connect($url,$type,$parameters);

        return $response;
    }

    public function verifyEmailID($token){
        $url ="/user/verifyEmail?token=".$token."";
        $type='get';
        $response = app('ApiCall')->connect($url,$type,$token);
        return $response;
    }

    public function verifyToken($token) {
        $url ="/user/verifyToken?token=".$token."";
        $type='get';
        $response = app('ApiCall')->connect($url,$type,$token);
        return $response;
    }

public function getWatchedProducts($userId){
    $url="/userproduct/getUserProducts";
    $parameters['owner'] =$userId;
    $type='post';
    $response = app('ApiCall')->connect($url,$type,$parameters);
//    echo('<pre>');
//    var_dump($response);
//    exit;
    return $response;

}
public function updateUserPassword($parameters){
    $type="post";
    $url="/user/updatePassword";
    $response = app('ApiCall')->connect($url,$type,$parameters);
    return $response;
}

public function insertToken($parameters){
    $type="post";
    $url="/user/insertToken";
    $response = app('ApiCall')->connect($url,$type,$parameters);
//    var_dump($response);
//    exit;
    return $response;
}

}
