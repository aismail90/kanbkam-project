<?php
/**
 * Created by PhpStorm.
 * User: ahmed
 * Date: 7/31/16
 * Time: 1:45 PM
 */

return [
    'languages' =>array('ar','en'),
    'API_URL'   => 'http://localhost:1337',
    'json_files_path'=>'http://127.0.0.1/souq-price-indexing/public/json/',
    'chart_font_path'=>'/opt/lampp/htdocs/souq-price-indexing/app/libraries/pChart2.1.4/fonts/',
    'not_found_image_cahrt_path'=>'http://127.0.0.1/souq-price-indexing/public/img/NoDataAvailable.png',
    'portal_core_public_folder_path' => '/opt/lampp/htdocs//at-portal/src/Atcop/CoreBundle/Resources/public',
    'json_app_url'=>'EGY_categories.json',
    'PRODUCTS_NUMBER_LIST_PAGE' => 32 ,
    'PRODUCTS_NUMBER_SEARCH_PAGE' => 32 ,
    'merchants' => ['new'=>["souq","others"]],
    'countries' => array('eg'=>array('view'=>array('ar'=>'مصر','en'=>'Egypt'),'country'=>'EGY','currency'=>array('ar'=>'جنية مصرى','en'=>'EGP'),'headlink'=>'egypt'),
                          'sa'=>array('view'=>array('ar'=>'السعودية','en'=>'Saudi'),'country'=>'SAU','currency'=>array('ar'=>'ريال سعودى','en'=>'SAR'),'headlink'=>'saudi'),
                           'ae' =>array('view'=>array('ar'=>'الامارات','en'=>'UAE'),'country'=>'ARE','currency'=>array('ar'=>'درهم اماراتى','en'=>'AED'),'headlink'=>'uae')) ,
    'homePageCategories'=>array('perfumes-fragrances'=>array('ar'=>'العطور','en'=>'Perfumes') ,'mobile-phone'=>array('ar'=>'جوالات/موبايلات','en'=>'Mobiles') , 'watches'=>array('ar'=>'الساعات','en'=>'Watches' )) ,
    'homePageLimitNumbers'=>array('limitNumberOfCategories'=>20,'limitNumberOfPopular'=>18 ),
    'numberOFRelatedProducts' => 15 ,
    'searchSortTypes' =>array('index_popularity'=>'nv_desc' ,'index_price_low_to_high'=>'price_asc' , 'index_price_high_to_low'=>'price_desc','index_top_drop'=>'chan_desc' , 'index_relevant'=>'relevant'),
    'searchSortApi' =>array('nv_desc'=>array('field'=>'number_of_viewed' , 'type'=>-1) ,'price_asc'=>array('field'=>'best_prices','type'=>1) , 'price_desc'=>array('field'=>'best_prices' ,'type'=>-1),'chan_desc'=>array('field'=>'percentage','type'=>1 ), 'relevant'=>array('field'=>'Relevant','type'=>1 )),
    'productStatus' =>array('out_of_stock'=>"1" ,'in_stock'=>"2"),

    'assets_cache' =>'123456',

    'FACEBOOK_CLIENT_ID'=>'1822642248020341',








];