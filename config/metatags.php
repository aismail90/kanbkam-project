<?php
/**
 * Created by PhpStorm.
 * User: ahmed
 * Date: 7/31/16
 * Time: 1:45 PM
 */

return [

        'homepage' =>
            [
                'ar' => [
                    'title' => 'كان بكام ــ  عروض سوق دوت كوم – أسعار تاريخية – إنخفاض أسعار – تنبيهات',
                    'description' => 'تخفيضات و عروض سوق دوت كوم الحقيقية، أكثر الأسعار إنخفاضا، شاهد الأسعار التاريخية، إخلق تنبيهات سعرية لمنتجات سوق دوت كوم و استقبل رسائل لحظية'
                ],
                'en' => [
                    'title' => 'KanbKam - Souq.com Deals - Price History - Price Drops - Price Alerts',
                    'description' => 'Find Souq.com Real Deals and Discounts, Recent Top Price Drops, Check Historical Prices, Create Price Alerts for Souq.com Products and get instant Email Alerts.'
                ]
            ],


        'finduseralerts' =>
            [
                'ar' => [
                    'title' => 'تنبيهات',
                    'description' => 'تخفيضات و عروض سوق دوت كوم الحقيقية، أكثر الأسعار إنخفاضا، شاهد الأسعار التاريخية، إخلق تنبيهات سعرية لمنتجات سوق دوت كوم و استقبل رسائل لحظية'
                ],
                'en' => [
                    'title' => 'Price Alerts',
                    'description' => 'Find Souq.com Real Deals and Discounts, Recent Top Price Drops, Check Historical Prices, Create Price Alerts for Souq.com Products and get instant Email Alerts.']
            ],


        'finduserwatchvalues' =>
            [
                'ar' => [
                    'title' => 'تنبيهات',
                    'description' => 'تخفيضات و عروض سوق دوت كوم الحقيقية، أكثر الأسعار إنخفاضا، شاهد الأسعار التاريخية، إخلق تنبيهات سعرية لمنتجات سوق دوت كوم و استقبل رسائل لحظية'
                ],
                'en' => [
                    'title' => 'Price Alerts',
                    'description' => 'Find Souq.com Real Deals and Discounts, Recent Top Price Drops, Check Historical Prices, Create Price Alerts for Souq.com Products and get instant Email Alerts.']
            ],


        'index' =>
            [
                'ar' => [
                    'title' => ' كان بكام- أفضل عروض سوق دوت كوم و تنبيهات لإنخفاض الأسعارفى #title',
                    'description' => 'أفضل عروض سوق دوت كوم حسب أكبر نسبة إنخفاض في السعر – إخلق تنبيهات سعرية لمنتجات سوق دوت كوم و احصل علي رسالة لحظة إنخفاض السعر فى #description'
                ],
                'en' => [
                    'title' => 'Kanbkam -  Souq.com Best Deals and Top Price Drops In  #title',
                    'description' => 'The Best Souq.com Deals ranked by Largest Price Drops – Create Price Alerts for Souq.com Products to instantly know about Price Drops'

                ]
            ],


        'search' =>
            [
                'ar' => [
                    'title' => 'كان بكام نتائج البحث فى #search_text',
                    'description' => 'كان بكام نتائج البحث فى #search_text'
                ],
                'en' => [
                    'title' => 'kanbkam Search Results about #search_text',
                    'description' => 'kanbkam Search Results about #search_text'


                ]
            ],

        'registeration' =>
            [
                'ar' => [
                    'title' => 'كان بكام تسجيل البيانات او الدخول',
                    'description' => 'كان بكام تسجيل البيانات او الدخول'
                ],
                'en' => [
                    'title' => 'Registeration Or Login',
                    'description' => 'Registeration Or Login'


                ]
            ],


        'forget-info' =>
            [
                'ar' => [
                    'title' => 'كان بكام-لقد نسيت كلمــة الســر',
                    'description' => 'كان بكام-لقد نسيت كلمــة الســر'
                ],
                'en' => [
                    'title' => 'kanbkam-forgot password',
                    'description' => 'kanbkam-forgot password'


                ]
            ],


    'resetPassword' =>
        [
            'ar' => [
                'title' => 'كان بكام-لقد نسيت كلمــة الســر',
                'description' => 'كان بكام-لقد نسيت كلمــة الســر'
            ],
            'en' => [
                'title' => 'Kanbkam-Password Reset',
                'description' => 'Kanbkam-Password Reset'


            ]
        ],


    'forgetPassword' =>
        [
            'ar' => [
                'title' => 'كان بكام-لقد نسيت كلمــة الســر',
                'description' => 'كان بكام-لقد نسيت كلمــة الســر'
            ],
            'en' => [
                'title' => 'kanbkam-forgot password',
                'description' => 'kanbkam-forgot password'


            ]
        ],




];