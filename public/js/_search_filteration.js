///**
// * Created by root on 12/4/16.
// */
$('document').ready(function(){

    $('#search_category').on('keyup',function(){
        var search_value=$.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $('.filter_category').hide().filter(function(){
            var value=$(this).find('a').attr('basic_url').replace(/\s+/g, ' ').toLowerCase();
            if(value.indexOf(search_value)!==-1){
                    return $(this);
            }


        }).show();


    });

    $('#search_brand').on('keyup',function(){
        var search_value=$.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $('.filter_brand').hide().filter(function(){
            var value=$(this).find('a').attr('basic_url').replace(/\s+/g, ' ').toLowerCase();
            if(value.indexOf(search_value)!==-1){
                return $(this);
            }


        }).show();


    });

    window.path = $('#current_url_string').val();
    window.queryString= $('#current_query_string').val();
    console.log(queryString);
    if(queryString!=""){
        window.objectQueryString = JSON.parse('{"' + decodeURI(queryString).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');

    }
    else{
        window.objectQueryString={};
    }
    $('.category_filter').on('click',function(){
        var category = $(this).attr('basic_url');
        var parameter = 'filter_category';
        var class_filter_type = 'category_filter'
        var type='open';
        if($(this).parent().hasClass('active')){
            var type='close';
        }

        searchOfList(category,type,parameter,class_filter_type);

    });

    $('.brand_filter').on('click',function(){
        var brand = $(this).attr('basic_url');
        var parameter = 'brand';
        var class_filter_type = 'brand_filter'
        var type='open';
        if($(this).parent().hasClass('active')){
            var type='close';
        }

        searchOfList(brand,type,parameter,class_filter_type);

    });


    $('#price_drop_range').on('change',function(){
        searchOfPriceDropCahnge($(this),'price_drop')
    });

    $(".seller").on('change',function(){
        searchForCheckBox($(this),'seller')
    });

    $(".gender").on('change',function(){
        searchForCheckBox($(this),'target')
    });

    $("#price_range_form").on('submit',function(e){
        e.preventDefault();
       searchForPriceRange($(this));
    });

    $('#reset_filter_search').on('click',function(){

        if('search_text' in objectQueryString){
            window.location.href=path+'?search_text='+objectQueryString['search_text'];
        }
        else{
            window.location.href=path;
        }
    })

});


function searchOfList(field,type,parameter,class_filter_type){
    objectQueryString['page'] = 1;
    if(type=='close'){
        delete objectQueryString[parameter];


    }
    else{
        objectQueryString[parameter] = field

    }

    var queryStringFinal = serialize(objectQueryString);
    var redierct_url = path+'?'+queryStringFinal;
    window.location.href = redierct_url;

}

function searchOfPriceDropCahnge(self,field){
    objectQueryString['page'] = 1;
    objectQueryString[field] = self.val();
    var queryStringFinal = serialize(objectQueryString);
    var redierct_url = path+'?'+queryStringFinal;
    window.location.href = redierct_url;

}


function searchForCheckBox(self ,field){
    objectQueryString['page'] = 1;
    if(field in objectQueryString)
    {
        var splitField = decodeURIComponent(objectQueryString[field]).split(',')
        console.log(splitField);
        if(splitField.length == 1){
            if(splitField.includes(self.attr('search_text'))){
                delete objectQueryString[field];
            }
            else{
                splitField.push(self.attr('search_text')) ;
                var stringQuery =  splitField.join();
                objectQueryString[field] = stringQuery;
            }
        }
        else{
            if(splitField.includes(self.attr('search_text'))){
                var index = splitField.indexOf(self.attr('search_text'));
                if (index >= 0) {
                    splitField.splice(index,1);
                }

            }
            else{
                splitField.push(self.attr('search_text'));
            }

            var stringQuery =  splitField.join();
            objectQueryString[field] = stringQuery;

        }

    }


    else{
        objectQueryString[field] = self.attr('search_text');
    }
    var queryStringFinal = serialize(objectQueryString);
    var redierct_url = path+'?'+queryStringFinal;
    window.location.href = redierct_url;
}

function searchForPriceRange(self){
    objectQueryString['page'] = 1;
    var price_range_from = $('#price_range_from').val();
    var price_range_to = $('#price_range_to').val();
    if(validatePriceForm(price_range_from , price_range_to)){
        if(price_range_from=="" && "price_from" in objectQueryString){
            delete objectQueryString['price_from']
        }
        else if(price_range_from!=""){
            objectQueryString['price_from']=price_range_from;
        }

    if(price_range_to=="" && "price_to" in objectQueryString){
        delete objectQueryString['price_to']

    }
    else if(price_range_to!=""){
        objectQueryString['price_to']=price_range_to;
    }



        var queryStringFinal = serialize(objectQueryString);
        var redierct_url = path+'?'+queryStringFinal;
        window.location.href = redierct_url;

    }
}




serialize = function(obj) {
    var str = [];
    for(var p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
}


function validatePriceForm(price_from , price_to){
    if((price_from=="" && price_to=="" && !("price_from" in objectQueryString)&& !("price_to" in objectQueryString)) || (parseInt(price_from) > parseInt(price_to)) )
    {
        return false;
    }
    else{
        return true;
    }
}



































































