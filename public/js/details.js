/**
 * Created by ahmed on 10/26/16.
 */
function ajaxAddWatchValue(e,self){
    e.preventDefault();
        var id = $(self).attr('id');
        var arr=id.split('_');
        var productId=arr[0];
        var type = arr[1];
        var merchant = arr[2];
        var type_merchant=productId+'_'+type+'_'+merchant;
        var price = $('#'+type_merchant+'_price_desired').val();
        var current_price=$('#'+type_merchant+'_current_value').val();
        if(validateWatchForm(price,current_price)){
            return false;
        }
        else{

        if($('#'+type_merchant+'_price_desired').val() != '')
        {
            var url = $('#ajax_url_watch_form').val();
            $.ajax({
                type:"POST",
                url:url,
                data:{
                    '_token': $('input[name=_token]').val(),
                     price   :price ,
                     type    :type    ,
                     merchant : merchant,
                     productId:productId

                },
                dataType: 'json',
                success: function(data){
//                    alert(('#'+type_merchant+'_watch_price_update'));
//                    console.log(data);
                    var result= data.response;
//                    console.log(result);
                    if(result.ok == 1){
                        if(arr[5]=='add')
                        {
                            $('#'+type_merchant+'_watch_price_add').hide();
                            $('#'+type_merchant+'_watch_price_add_m').hide();
                        }

                        $('#'+type_merchant+'_watch_price_update').show();
                        $('#'+type_merchant+'_watch_price_update_m').show();
                        $('#'+type_merchant+'_watch_price_delete_m').show();
                        $('#'+type_merchant+'_watch_price_delete').show();
                        $('#watch_status').show();
                    }
                },
                error: function(data){

                }
            });
        }}
};

function ajaxDeleteWatchValue(e,self){
    e.preventDefault();
    var id = $(self).attr('id');
    var arr=id.split('_');
    var productId=arr[0];
    var type = arr[1];
    var merchant = arr[2];
    var type_merchant=productId+'_'+type+'_'+merchant;
    var url = $('#ajax_url_delete_watch_form').val();
    $.ajax({
        type:"POST",
        url:url,
        data:{
            '_token': $('input[name=_token]').val(),
            type    :type    ,
            merchant : merchant,
            productId:productId
        },
        dataType: 'json',
        success: function(data){
//            alert(1);
            var result= data.response;
            if(result.ok == 1){
                $('#'+type_merchant+'_watch_price_delete').hide();
                $('#'+type_merchant+'_watch_price_delete_m').hide();
                $('#'+type_merchant+'_watch_price_update').hide();
                $('#'+type_merchant+'_watch_price_update_m').hide();
                $('#'+type_merchant+'_watch_price_add_m').show();
                $('#'+type_merchant+'_watch_price_add').show();
                $('#'+type_merchant+'_difference').text('-');
                $('#'+type_merchant+'_price_desired').val("");
                $('#watch_status').hide();
            }
        },
        error: function(data){

        }
    })
}

function validateWatchForm(price,current_value){
    if(price==' '|| (current_value!='1' && current_value!=0 && parseInt(price)>= parseInt(current_value)))
    {
        alert('your price should be less than current price')
        return true;

    }
    else{
        return false;
    }
}



$(document).ready(function(){



    $('.numeric').on('keyup', function (event) {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
    $('#facebook').on('click',function(){

        FB.ui({
            method: 'feed',
            mobile_iframe: true,
            link: $('#current_url').val(),
            picture: $('#product_image').val(),
            caption: '',
            description: '',
            message: ''
            // href: 'https://developers.facebook.com/docs/',
        }, function(response){});
    });
    $('.watch_form').on('click',function(e){

        if(window.login==true)
        {
            ajaxAddWatchValue(e ,this);
        }
        else{
            $('#sign-in').modal('show');
        }

    });

    $('.delete_form').on('click',function(e){
        if(window.login==true)
        {
            ajaxDeleteWatchValue(e ,this);
        }
        else{
            $('#sign-in').modal('show');
        }

    });
    $('[name=price_desired]').on('change',function(){

        calculateDifference(this);
    });
});


function calculateDifference(self){

    var id = $(self).attr('id');
    var arr=id.split('_');
    var productId=arr[0];
    var type= arr[1];
    var merchant = arr[2];
    var type_merchant=productId+'_'+type+'_'+merchant;

    if($('#'+type_merchant+'_current_value').val()!=='1'&& parseInt($('#'+type_merchant+'_price_desired').val()) < parseInt($('#'+type_merchant+'_current_value').val())){

        var difference = parseInt($('#'+type_merchant+'_current_value').val()) - parseInt($('#'+type_merchant+'_price_desired').val());
        $('#'+type_merchant+'_difference').text(difference.toFixed(2));
    }
}



























