/**
 * Created by ahmed on 11/6/16.
 */

function displaySelectedMerchant(self){
    if($(self).find('option:selected').attr('merchant_type')== 'listed_by_souq'){
        $('#others_price').hide();
        $('#others_ul').hide();
        $('#souq_price').show();
        $('#souq_ul').show();
        $('#souq_buy').show();
        $('#others_buy').hide();
    }
    else{
        $('#others_price').show();
        $('#others_ul').show();
        $('#souq_price').hide();
        $('#souq_buy').hide();
        $('#others_buy').show();
        $('#souq_ul').hide();
    }

}


function changeMerchantTab(self){
    if($(self).find('option:selected').attr('merchant_type')=='listed_by_souq'){
        $('#others_tab').removeClass('active');
        $('#3rd-party-new').removeClass('active');
        $('#souq_tab').addClass('active');
        $('#souq_tab_pane').addClass('active');

    }
    else{
        $('#souq_tab').removeClass('active');
        $('#souq_tab_pane').removeClass('active');
        $('#others_tab').addClass('active');
        $('#3rd-party-new').addClass('active');



    }

}


$('document').ready(function(){
    $('#list_by_merchant').on('change' ,function(){
        displaySelectedMerchant(this);
        changeMerchantTab($(this));
    });
});


