/**
* Created by ahmed on 8/8/16.
*/

$(document).ready(function(){

    jQuery.validator.addMethod("accept", function(value, element, param) {
        return value.match(new RegExp("." + param + "$"));
    });
    $('.form-validation-group').each(function() {

        $(this).validate({

            rules: {
                first_name: {
                    required:true,
                    accept :"[a-zA-Z]+"
                },
                last_name: {
                    required:true,
                    accept :"[a-zA-Z]+"
                },
                email: {
                    required: true,
                    email: true
                },
                mobile_number:{
                    number:true
                },
                password: {
                    required: true,
                    minlength: 5
                },
                password_confirm : {
                    equalTo : "#password"
                }

            },
            messages: {
                first_name: {
                    required:"please enter your first name",
                    accept : "your name cannot contain a number"
                },
                last_name: {
                    required:"please enter your last name",
                    accept : "your name cannot contain a number"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                password_confirm:{
                    equalTo : "passwords don't match"
                } ,
                email: "Please enter a valid email address"
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });





});

