$('document').ready(function() {
    $('.category_homepage').on('click', function() {

        $('#ul_home_page_category li.active').removeClass('active');
        $(this).addClass("active");
        var url = $('#get_home_page_category_data_url').val();
        var category = $(this).find('a').attr('category');
        $('#loading_image').show();
        $('#category_data').hide();
        $.ajax({
            type: "Get",
            url: url,
            data: {
                category: category

            },
            dataType: 'json',

            success: function(data) {
                $('#category_data').html(data.html);
            },

            complete: function(response) {
                $('#loading_image').hide();
                $('#category_data').show();
                $('.responsive-slide').slick({
                    dots: false,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 5,
                    slidesToScroll: 5,
                    rtl:window.rtl,
                    responsive: [
                        {
                            breakpoint: 960,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3,

                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2,
                            }
                        },
                    ]
                });

            },
            error: function(data) {

            }
        });
    });
})