/**
 * Created by root on 12/14/16.
 */

$(document).ready(function() {

 if($('#product_data>li').length >= 32 ) {
     var pageNumber = 2;
     var requested_page =1;
$(window).scroll(function() {

    var last_li = $('#product_data').last();
    var hT = last_li.offset().top,
        hH = last_li.outerHeight(),
        wH = $(window).height(),
        wS = $(this).scrollTop();
    if (wS > (hT+hH-wH-30)){
        if(requested_page !== pageNumber ){
            $('#products_loading_image').show();

            var parameters = $.parseJSON($('#url_parameters').val());
            var allData =Object.assign(parameters,objectQueryString);
            allData['page']= pageNumber ;
            var url =$('#url_load_data').val();
            requested_page = pageNumber;
            $.ajax({
                url: url,
                cache: false,
                data:allData,
                dataType: 'json',
                success: function(data) {

                    $('#product_data').append(data['html']);
                    $('#products_loading_image').hide();
                    if(data['html'])
                        pageNumber++;

                }
            });
        }
    }
});

    }
});













