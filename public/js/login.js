/**
 * Created by zoka on 14/08/16.
 */
$('document').ready(function(){
    $("#user_login_form").validate({
        rules: {
            email: {required:true ,email:true },
            password: "required"

        },
        messages: {
            email: "Please enter a valid email address",
            password: "please enter your password"
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});