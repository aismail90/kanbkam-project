/**
 * Created by ahmed on 11/6/16.
 */
/**
 * Created by ahmed on 8/8/16.
 */

$(document).ready(function(){
        $('.user_login_form').each(function(){
            var form = $(this);
            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 5
                    }

                },
                messages: {
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    email: "Please enter a valid email address"
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });





    $('#subscribe_form').on('submit',function(e){
        $('#email_enter_success').hide();
        var email= $('#subscribe_email').val();
        if(!email.match(re) || email==" ") {
            $('#subscribe_email_error').show();
            $('#subscribe_email_exist').hide();
            $('#email_enter_success').hide();

            return false;
        }
        else{
            e.preventDefault();
            $('#subscribe_email_error').hide();
            var url= $('#subscribe_url').val();
            $.ajax({
                type:"post",
                url :url,
                data:{
                    '_token': $('#token').val(),
                    email:email
                },
                dataType:'json',
                success:function(data){

                    if(data['response'] == "1"){
                        // $('#subscribe_email_error').text('email already exist');

                        $('#subscribe_email_exist').show();
                        $('#email_enter_success').hide();
                        $('#subscribe_email_error').hide();
                    }
                    else if(data['response'] == "2"){
                        $('#email_enter_success').show();
                        $('#subscribe_email_error').hide();
                        $('#subscribe_email_exist').hide();
                    }
                    else{
                        $('#subscribe_error_message').show();
                        $('#subscribe_email_exist').hide();
                        $('#email_enter_success').hide();
                    }
                },
                error:function(error){

                }

            });
        }

    });


    $("#search_form").on('submit',function(){
       if($('#search_text').val() =="")
       {
           return false;
       }

    });

//    $('#search_text').autocomplete({
//
//        source: function( request, response ) {
//            var url = $('#auto_complete_url').val();
//            $.ajax({
//                url : url,
//                dataType: "json",
//                data: {
//                    name_startsWith: request.term
//
//                },
//                success: function( data ) {
//                    response( $.map( data, function( item ) {
//                        console.log(item)
//                        return {
////                            label: item,
//                            value: item.value
//                        }
//                    }));
//                }
//            });
//        },
//        autoFocus: false,
//        minLength: 3
//    });

});


var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


//}

