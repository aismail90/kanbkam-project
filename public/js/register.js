/**
 * Created by ahmed on 8/8/16.
 */
$('document').ready(function(){

    $("#register-form").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            },
            mobile_number:{
                number:true
            },
            password: {
                required: true,
                minlength: 5
            },
            password_confirm : {
                equalTo : "#password"
            }
        },
        messages: {
            name: "Please enter your name",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            password_confirm:{
                equalTo : "passwords don't match"
            } ,
            email: "Please enter a valid email address"
        },
        submitHandler: function(form) {
            form.submit();
        }
    });



});


