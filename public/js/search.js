function isUrl(s) {
    var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
    return regexp.test(s);
}
$('document').ready(function(){
    $('#search_button').on('click',function(){
         var search_text = $('#search_input').val();
        if($.isNumeric(search_text))
        {
            document.location.href=$('#index_route').val()+'/show/'+encodeURIComponent(search_text+'u');

        }
        else if(isUrl(search_text) && search_text.includes('souq'))
        {

                var array_url = search_text.split('-');
                var id = array_url[array_url.length-1];
                var array_id = id.split('/');
                var id = array_id[0]+array_id[1];

                document.location.href=$('#index_route').val()+'/search/'+encodeURIComponent(id);


        }
        else{
                search_text =search_text.replace(/\//g, "");
                search_text = search_text.split('.').join("");
                search_text=search_text.split(" ").join('-');
                if(search_text){
                  document.location.href=$('#index_route').val()+'/search/'+search_text;
                }
        }
    });
});