<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'home_page_Why'     => 'ليه'    ,
    'home_page_Kanbkam'     =>  'كان بكام' ,
    'home_page_description_of_site'     => 'موقع "كان بكام" يراقب أسعار منتجات سوق دوت كوم التاريخية و الحالية لكي تتأكد من حصولك علي تخفيضات حقيقية عند الشراء. "كان بكام" أيضا يعرفك أكثر منتجات سوق دوت كوم إنخفاضا في السعر و قيمة الإنخفاض.'    ,
    'home_page_price_drops'     => 'أكثر منتجات سوق دوت كوم إنخفاضا'    ,
    'home_page_price_drops_description'     => ' أكبر 20 منتج إنخفاضا في السعر في سوق دوت كوم (حسب نسبة % الإنخفاض)'    ,
    'home_page_popular_products'     => 'المنتجات الأكثر مشاهدة'    ,
    'home_page_viewAll' => 'شاهد الكل',
    'home_page_categories' => 'التصنيفات',


    'layout_search_placeholder'      => 'أدخل رابط منتج سوق دوت كوم أو قم بالبحث باسم المنتج',
    'layout_best_deals'      => 'أفضل العروض',
    'layout_categories'      => 'الفئات',
    'layout_sign_in'      => 'تسجيل الدخول',
    'layout_home'               => 'الرئيسية' ,
    'layout_register'               => 'تسجيل الدخول' ,
    'layout_login'               => 'الدخول' ,
    'layout_contact_us'               => 'اتصل بنا' ,
    'layout_forgot_password'      => 'نسيت كلمة المرور ؟',
    'layout_option_sign_in'      => '- أو الدخول بواسطة  -',
    'layout_home'               => 'Home' ,
    'layout_register'               => 'Register' ,
    'layout_login'               => 'Log in' ,
    'layout_contact_us'               => 'Contact Us' ,
    'layout_subscribe_newsletter'       =>'Subscribe Newsletter',
    'layout_enter_your_email'       =>'Enter Your Email',
    'layout_well_done'       =>    'Well done!',
    'layout_your_email_inserted_success'       =>'Your Email inserted success.',
    'layout_error'       =>'ERROR!',
    'layout_email_already_exist'       =>'Email already exist',
    'layout_there_is_an_error_try_again_later'       =>'there is an error try again later',
    'layout_your_email_inserted_success'       =>'Your Email inserted success',
    'layout_enter_valid_email'       =>'Enter valid email',




    'details_watch_button'      => 'راقب',
    'details_update_button'      => 'تعديل',
    'details_delete_button'      => 'حذف',
    'details_type'      => 'بالنوع',
    'details_price'      => 'بالسعر',
    'details_when'      => 'منذ',
    'details_highest_price'      => 'أعلى سعر',
    'details_lowest_price'      => 'أقل سعر',
    'details_no_change_in_price'      => 'لا يوجد تغير في السعر',
    'details_recent_price_drop'      => 'اخر تخفيض في السعر',
    'details_recent_price_raise'      => 'اخر ارتفاع في السعر',
    'details_desired_price'      => 'السعر المطلوب',
    'details_current_price'      => 'السعر الحالي',
    'details_difference'      => 'الفرق',
    'details_souq_merchant'      => 'سوق.كوم',
    'details_others_merchant'      => 'بائعين اخرين',
    'details_difference'      => 'الاختلاف',
    'details_buy_now'      => 'شراء الان',
    'details_product_details'      => 'تفاصيل المنتج',
    'details_description'      => 'التفاصيل',
    'details_physical_features'      => 'اخرى',
    'details_category'      => 'الفئة',
    'details_brand'      => 'النوع',
    'details_price_history'      => 'تاريخ الاسعار',
    'details_related_products'      => 'سلع متعلقة',
    'details_create_price_alert'      => 'Create Price Alert',
    'details_listed_by_souq' => 'بواسطة سوق',
    'details_listed_by_3rd Party' => 'بواسطة آخرون',




    'index_top_drop'                  =>'الأكثر إنخفاضا',
    'index_popularity'                  =>'الأكثر مشاهدة',
    'index_price_low_to_high'                  =>'سعر: من الأقل للأكثر',
    'index_price_high_to_low'                  =>'سعر: من الأكثر للأقل',
    'index_relevant'                  =>'الاكثر تطابقاً',



    'list_filter'                          => 'تصفية'  ,
    'list_category'                          => 'التصنيفات'  ,
    'list_submit'                          => 'تحديث'  ,
    'list_brand'                          => 'النوع'  ,
    'list_seller'                          => 'البائع'  ,
    'list_price_drop'                          => 'الخصم'  ,
    'list_price_range'                          => 'السعر'  ,
    'list_souq'                          => 'سوق.كوم'  ,
    'list_other'                          => 'أخرى'  ,
    'list_to'                          => 'من'  ,
    'list_from'                          => 'الى'  ,
    'list_results_found_for'              => 'نتائج البحث عنـ'  ,
    'list_no_products_found'                          => 'لا يوجد نتائج'  ,
    'list_go_back_or_try_different_keyword'                          => 'عد للخلف و قم بالحث'  ,
    'list_gender'                          => 'النوع'  ,
    'list_male'                          => 'ذكر'  ,
    'list_female'                          => 'انثى'  ,





    'register_first_name'                  => 'الاسم الاول' ,
    'register_last_name'                  => 'الاسم الاخير' ,
    'register_email_address'                  => 'البريد الاكتروني' ,
    'register_password'                  => 'كلمة السر' ,
    'register_confirm_password'                  => 'تاكيد كلمة السر' ,
    'register_register'                  => 'تسجيل' ,
    'register_sign_up'                  => 'دخول' ,
    'register_re_enter_password'                  => 'اعادة كلمة المرور' ,


];
