<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'home_page_Why'     => 'ليه'    ,
    'home_page_Kanbkam'     =>  'كان بكام' ,
    'home_page_description_of_site'     => 'موقع "كان بكام" يراقب أسعار منتجات سوق دوت كوم التاريخية و الحالية لكي تتأكد من حصولك علي تخفيضات حقيقية عند الشراء. "كان بكام" أيضا يعرفك أكثر منتجات سوق دوت كوم إنخفاضا في السعر و قيمة الإنخفاض.'    ,
    'home_page_price_drops'     => 'أكثر منتجات سوق دوت كوم إنخفاضا'    ,
    'home_page_price_drops_description'     => ' أكبر 20 منتج إنخفاضا في السعر في سوق دوت كوم (حسب نسبة % الإنخفاض)'    ,
    'home_page_popular'     => 'المنتجات الأكثر مشاهدة'    ,
    'home_page_viewAll' => 'شاهد الكل',
    'home_page_categories' => 'التصنيفات',


    'layout_search_placeholder'      => 'أدخل رابط منتج سوق دوت كوم أو قم بالبحث باسم المنتج',
    'layout_best-deals'      => 'أفضل العروض',
    'layout_popular'      => 'المنتجات الأكثر مشاهدة'    ,
    'layout_categories'      => 'الفئات',
    'layout_sign_in'      => 'تسجيل الدخول',
    'layout_home'               => 'الرئيسية' ,
    'layout_register'               => 'تسجيل الدخول' ,
    'layout_login'               => 'الدخول' ,
    'layout_contact_us'               => 'اتصل بنا' ,
    'layout_forgot_password'      => 'نسيت كلمة المرور ؟',
    'layout_option_sign_in'      => '- أو الدخول بواسطة  -',
    'layout_home'               => 'الرئيسية' ,
    'layout_register'               => 'التسجيل' ,
    'layout_login'               => 'دخول' ,
    'layout_contact_us'               => 'اتصل بنا' ,
    'layout_subscribe_newsletter'       =>'اشترك في النشرة الاخبارية',
    'layout_enter_your_email'       =>'ادخل البريد الالكتروني',
    'layout_email_already_exist'       =>'البريد الالكتروني موجود',
    'layout_there_is_an_error_try_again_later'       =>'هناك خطا في الصفحة , حاول مرة اخري',
    'layout_your_email_inserted_success'       =>'تم الاشتراك بنجاح',
    'layout_enter_valid_email'       =>'اكتب البريد الإلكتروني بالطريقة المطلوبة',
    'layout_send'                    =>'تابع',
    'layout_logout'                    =>'تسجيل الخروج',
    'layout_my_watch_list'                    =>'قائمة المشاهدات',



    'details_watch_button'      => 'راقب',
    'details_update_button'      => 'تعديل',
    'details_delete_button'      => 'حذف',
    'details_type'      => 'بالنوع',
    'details_price'      => 'بالسعر',
    'details_when'      => 'منذ',
    'details_highest_price'      => 'أعلى سعر',
    'details_lowest_price'      => 'أقل سعر',
    'details_no_change_in_price'      => 'لا يوجد تغير في السعر',
    'details_recent_price_drop'      => 'اخر تخفيض في السعر',
    'details_recent_price_raise'      => 'اخر ارتفاع في السعر',
    'details_desired_price'      => 'السعر المطلوب',
    'details_current_price'      => 'السعر الحالي',
    'details_difference'      => 'الفرق',
    'details_souq_merchant'      => 'سوق.كوم',
    'details_others_merchant'      => 'بائعين اخرين',
    'details_difference'      => 'الاختلاف',
    'details_buy_now'      => 'شراء الان',
    'details_product_details'      => 'تفاصيل المنتج',
    'details_description'      => 'التفاصيل',
    'details_physical_features'      => 'اخرى',
    'details_category'      => 'الفئة',
    'details_brand'      => 'النوع',
    'details_price_history'      => 'تاريخ الاسعار',
    'details_related_products'      => 'سلع متعلقة',
    'details_create_price_alert'      => 'انشأ تنبيه للسعر',
    'details_very_good'      => 'جيد جداً',
    'details_listed_by_souq' => 'بواسطة سوق',
    'details_listed_by_3rd Party' => 'بواسطة آخرون',
    'details_good'      => 'جيد',
    'details_not_good'      => 'غير جيد',
    'details_erence'      => '',
    'details_diff'      => 'الفرق',
    'details_watch_price'      => '',
    'details_watch_type'      => 'التاجر',
    'details_watch_current'    => 'الجارى' ,
    'details_watch_low'    => 'الاقل' ,
    'details_watch_average'    => 'المتوسط' ,
    'details_watch_high'    => 'الاعلى' ,





    'index_top_drop'                  =>'الأكثر إنخفاضا',
    'index_popularity'                  =>'الأكثر مشاهدة',
    'index_price_low_to_high'                  =>'سعر: من الأقل للأكثر',
    'index_price_high_to_low'                  =>'سعر: من الأكثر للأقل',
    'index_relevant'                  =>'الاكثر تطابقاً',



    'list_filter'                          => 'تصفية'  ,
    'list_category'                          => 'التصنيفات'  ,
    'list_submit'                          => 'تحديث'  ,
    'list_brand'                          => 'النوع'  ,
    'list_seller'                          => 'البائع'  ,
    'list_price_drop'                          => 'الخصم'  ,
    'list_price_range'                          => 'السعر'  ,
    'list_souq'                          => 'سوق.كوم'  ,
    'list_other'                          => 'أخرى'  ,
    'list_to'                          => 'الي'  ,
    'list_from'                          => 'من'  ,
    'list_results_found_for'              => 'نتائج البحث عنـ'  ,
    'list_no_products_found'                          => 'لا يوجد نتائج'  ,
    'list_go_back_or_try_different_keyword'                          => 'عد للخلف و قم بالبحث'  ,
    'list_gender'                          => 'النوع'  ,
    'list_male'                          => 'ذكر'  ,
    'list_female'                          => 'انثى'  ,
    'list_more_than'                       => '+',





    'register_first_name'                  => 'الاسم الاول' ,
    'register_last_name'                  => 'الاسم الاخير' ,
    'register_email_address'                  => 'البريد الاكتروني' ,
    'register_password'                  => 'كلمة السر' ,
    'register_confirm_password'                  => 'تاكيد كلمة السر' ,
    'register_register'                  => 'تسجيل' ,
    'register_sign_up'                  => 'التسجيل' ,
    'register_re_enter_password'                  => 'اعادة كلمة المرور' ,
    'register_success_message'           =>'لقد اشتركت بطريقة صحيحة , برجاء تفقد بريدك الالكترونى لتفعيل الحساب',
    'register_update_password_success'           =>'لقد تم تغيير كلمة السر بنجاح',
    'register_error_email_exist'        =>'هذا الايميل موجود بالفعل ',
    'register_error'                     =>'وجد خطأ , برجاء المحاولة فى وقت اخر',
    'login_error_password_mismatch'      =>'يوجد خطأ فى كلمة السر او البريد الالكترونى',
    'login_error_mail_not_activated'      =>'من فضلك فعّل حسابك',
    'login_error_mail_not_found'      =>'هذا الايميل غير موجود',
    'register_sign_up_button'                        => 'سجّل'   ,



    'alerts_omg'         => 'OMG!',
    'alerts_you_have_no_alerts_yet'         => 'لا يوجد لديك تنبيهات',
    'alerts_your_alerts'         => 'التنبيهات',
    'alerts_triggered'         => 'Triggered',
    'alerts_non_triggered'         => 'Non Triggered',
    'alerts_desired'         => 'السعر المطلوب',
    'alerts_current'         => 'السعر الحالى',


    'forget_find_your_kanbkam_account'            => 'استرجع حسابك ',
    'forget_send'            => 'ارسل',
    'forget_check_your_email'            => 'تفقد بريدك الالكترونى',
    'forget_mail_not_activated'            => 'بريدك الالكترونى لم يفعل حتى الآن',
    'forget_mail_not_registered'            => 'هذا الايميل غير موجود',

    'login_login_with_facebook'       =>'Login with Facebook',
    'reset_reset_password'       =>'تغيير كلمة السر',
    'reset_reset_submit'       =>'تحديث',


    'product_status_out_of_stock' => 'غير متوفر',
    'kanbkam_404_page'  =>'كان بكام صفحة 404',
    'page_not_found'  =>'الصفحة غير موجودة',
    'go_to_kanbkam'  =>'اذهب الي Kanbkam.com',
    'error'  =>'خطأ'


];
