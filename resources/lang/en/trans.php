<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'home_page_Why'     => 'Why'    ,
    'home_page_Kanbkam'     => 'Kanbkam'    ,
    'home_page_description_of_site'     => "Kanbkam tracks ALL Souq.com products historical & current prices for you so that you ensure that you are getting real DEALS and DISCOUNTS. Kanbkam also tells you which products on Souq.com recently dropped in price and by how much."    ,
    'home_page_price_drops'     => 'Top Souq.com Price Drops'    ,
    'home_page_price_drops_description'     => 'Top 20 Price Drops from all over Souq.com (Ranked by % Price Drop)'    ,
    'home_page_popular_products'     => 'Popular Products'    ,
    'home_page_viewAll' => 'View All'                       ,
    'home_page_categories' => 'Categories',





    'layout_search_placeholder'      => 'Enter URL Or keywords to find Products',
    'layout_best-deals'      => 'Best Deals',
    'layout_popular'      => 'Popular',
    'layout_categories'      => 'Categories',
    'layout_sign_in'      => 'Sign In',
    'layout_forgot_password'      => 'Forgot Password?',
    'layout_option_sign_in'      => '- Or Sign In With  -',
    'layout_home'               => 'Home' ,
    'layout_register'               => 'Register' ,
    'layout_login'               => 'Log in' ,
    'layout_contact_us'               => 'Contact Us' ,
    'layout_subscribe_newsletter'       =>'Subscribe Newsletter',
    'layout_enter_your_email'       =>'Enter Your Email',
    'layout_well_done'       =>    'Well done!',
    'layout_your_email_inserted_success'       =>'Your Email inserted success.',
    'layout_error'       =>'ERROR!',
    'layout_email_already_exist'       =>'Email already exist',
    'layout_there_is_an_error_try_again_later'       =>'there is an error try again later',
    'layout_your_email_inserted_success'       =>'Your Email inserted success',
    'layout_enter_valid_email'       =>'Enter valid email',
    'layout_send'                    =>'Send',
    'layout_logout'                    =>'logout',
    'layout_my_watch_list'                    =>'My Watch List',



    'details_watch_button'      => 'Watch',
    'details_update_button'      => 'Update',
    'details_delete_button'      => 'Delete',
    'details_type'      => 'Type',
    'details_price'      => 'Price',
    'details_when'      => 'When',
    'details_highest_price'      => 'Highest Price',
    'details_lowest_price'      => 'Lowest Price',
    'details_no_change_in_price'      => 'No change in price',
    'details_recent_price_drop'      => 'Recent Price Drop',
    'details_recent_price_raise'      => 'Recent Price Raise',
    'details_desired_price'      => 'Desired Price',
    'details_current_price'      => 'Current Price',
    'details_difference'      => 'Difference',
    'details_souq_merchant'      => 'Souq.com',
    'details_others_merchant'      => '3rd Party',
    'details_difference'      => 'Difference',
    'details_buy_now'      => 'Buy Now',
    'details_product_details'      => 'Product Details',
    'details_description'      => 'Description',
    'details_physical_features'      => 'Physical Features',
    'details_category'      => 'Category',
    'details_brand'      => 'Brand',
    'details_price_history'      => 'Price History',
    'details_related_products'      => 'Related Products',
    'details_create_price_alert'      => 'Create Price Alert',
    'details_very_good'      => 'Very Good',
    'details_create_price_alert'      => 'Create Price Alert',
    'details_listed_by_souq' => 'Listed by Souq',
    'details_listed_by_3rd Party' => 'Listed by 3rd Party',
     'details_good'      => 'Good',
     'details_not_good'      => 'Not Good',
     'details_erence'      => 'erence',
     'details_diff'      => 'Diff',
     'details_watch_price'      => 'Price',
     'details_watch_type'    => 'Merchant' ,
     'details_watch_current'    => 'current' ,
     'details_watch_low'    => 'low' ,
     'details_watch_average'    => 'average' ,
     'details_watch_high'    => 'high' ,










    'index_top_drop'                  =>'Top Drop',
    'index_popularity'                  =>'Popularity',
    'index_price_low_to_high'                  =>'Price: Low to High',
    'index_price_high_to_low'                  =>'Price: High to Low',
    'index_relevant'                  =>'Relevant',




    'list_filter'                          => 'Filter'  ,
    'list_category'                          => 'Category'  ,
    'list_submit'                          => 'Apply'  ,
    'list_brand'                          => 'Brand'  ,
    'list_seller'                          => 'Seller'  ,
    'list_price_drop'                          => 'Price Drop'  ,
    'list_price_range'                          => 'Price Range'  ,
    'list_souq'                          => 'Souq'  ,
    'list_other'                          => 'Other'  ,
    'list_to'                          => 'To'  ,
    'list_from'                          => 'From'  ,
    'list_results_found_for'              => 'Results found for'  ,
    'list_no_products_found'                          => 'No Products Found'  ,
    'list_go_back_or_try_different_keyword'                          => 'Go back or try a different keyword !'  ,
    'list_gender'                          => 'Gender'  ,
    'list_male'                          => 'Male'  ,
    'list_female'                          => 'Female'  ,
    'list_more_than'                       => '+',





    'register_first_name'                  => 'First Name' ,
    'register_last_name'                  => 'Last Name' ,
    'register_email_address'                  => 'Email Address' ,
    'register_password'                  => 'Password' ,
    'register_confirm_password'                  => 'Confirm Password' ,
    'register_register'                  => 'Register' ,
    'register_sign_up'                  => 'Sign Up' ,
    'register_re_enter_password'                  => 'Re- enter Password' ,
    'register_success_message'           =>'You are register successfully , please check your mail ',
    'register_update_password_success'           =>'your password updated successfully',
    'register_error_email_exist'        =>'This email already exist ',
    'register_error'                     =>'there is an error , try again later',
    'login_error_password_mismatch'      =>'your email and password do not match',
    'login_error_mail_not_activated'      =>'please activate your account',
    'login_error_mail_not_found'      =>'this email not found',
    'register_sign_up_button'                               => 'Sign Up'    ,



    'alerts_omg'         => 'OMG!',
    'alerts_you_have_no_alerts_yet'         => 'You have no alerts yet Lets change that!',
    'alerts_your_alerts'         => 'Your Alerts',
    'alerts_triggered'         => 'Triggered',
    'alerts_non_triggered'         => 'Non Triggered',
    'alerts_desired'         => 'Desired',
    'alerts_current'         => 'Current',


    'forget_find_your_kanbkam_account'            => 'Find your Kanbkam account',
    'forget_send'            => 'Send',
    'forget_check_your_email'            => 'Check your mail',
    'forget_mail_not_activated'            => 'Mail not activated yet',
    'forget_mail_not_registered'            => 'Mail not registered',


    'login_login_with_facebook'       =>'Login with Facebook',
    'reset_reset_password'       =>'Reset Password',
    'reset_reset_submit'       =>'Submit',


    'product_status_out_of_stock' => 'out of stock',
    'kanbkam_404_page'  =>'Kanbkam 404 page',
    'page_not_found'  =>'Page Not Found',
    'go_to_kanbkam'  =>'Go to Kanbkam.com',
    'error'  =>'error'





];
