<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <title>Kanbkam 404 page</title>
    <!-- Bootstrap core CSS -->
    {{ Html::style('css/bootstrap.min.css'.'?v='.Config::get('constants.assets_cache')) }}
    <!-- Custom styles for this template -->
    {{ Html::style('css/animate.css'.'?v='.Config::get('constants.assets_cache')) }}

    {{ Html::style('css/en/app.css'.'?v='.Config::get('constants.assets_cache')) }}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    {{ Html::script('js/html5shiv.min.js'.'?v='.Config::get('constants.assets_cache')) }}

    {{ Html::script('js/respond.min.js'.'?v='.Config::get('constants.assets_cache')) }}
    <![endif]-->
</head>
<body>
<main>
    <div class="error-main">
        {{ Html::image('img/oops.png','',array('class' => 'animated')) }}
        <div class="error-message">
            <span>{{ Html::image('img/no-search-result.svg') }}</span>
            <p>
                404<strong><?php echo Lang::get('trans.error');?></strong>
                <span><?php echo Lang::get('trans.page_not_found');?></span>
            </p>
        </div>
            <a href="{{route('default')}}" class="btn btn-default"><?php echo Lang::get('trans.go_to_kanbkam');?></a>
    </div>
</main>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
{{ Html::script('js/jquery-2.1.1.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/bootstrap.min.js'.'?v='.Config::get('constants.assets_cache')) }}
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
{{ Html::script('js/ie10-viewport-bug-workaround.js'.'?v='.Config::get('constants.assets_cache')) }}

<!--menu -->
{{ Html::script('js/modernizr.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/jquery.menu-aim.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/main.js'.'?v='.Config::get('constants.assets_cache')) }}

{{ Html::script('js/wow.min.js'.'?v='.Config::get('constants.assets_cache')) }}
<script>
    new WOW().init();
</script>



</body>
</html>