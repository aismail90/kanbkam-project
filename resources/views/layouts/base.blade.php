<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    @if($metaData!=null)
    <meta name="description" content="{{$metaData['description']}}">
    <title>{{$metaData['title']}}</title>
    @else
    @if(Request::route()->getName() == 'product.show')
    <meta property="og:title" content="{{$detailsMetaData['title']}}"/>
    <meta property="og:description" content="{{$detailsMetaData['description']}}"/>
    <meta property="og:image" content="{{$searchData[0]['image']}}"/>
    <meta name="description" content="{{$detailsMetaData['description']}}">
    <title>{{$detailsMetaData['title']}}</title>
    @endif

    @endif
    {{ Html::favicon('img/favicon.ico') }}
    @section('style')
    <!-- Bootstrap core CSS -->
    {{ Html::style('css/bootstrap.min.css'.'?v='.Config::get('constants.assets_cache')) }}
    @if($local=='ar')
    {{ Html::style('css/ar/bootstrap-rtl.css'.'?v='.Config::get('constants.assets_cache')) }}
    @endif
    {{ Html::style('css/select2.min.css'.'?v='.Config::get('constants.assets_cache')) }}
     {{ Html::style('css/slick.css'.'?v='.Config::get('constants.assets_cache')) }}
    {{ Html::style('css/animate.css'.'?v='.Config::get('constants.assets_cache')) }}
    {{ Html::style('css/mega-dropdown.css'.'?v='.Config::get('constants.assets_cache')) }}
    {{ Html::style('css/rangeslider.css'.'?v='.Config::get('constants.assets_cache')) }}
    {{ Html::style('css/'.$local.'/app.css'.'?v='.Config::get('constants.assets_cache')) }}
    {{ Html::style('css/jquery-ui.css'.'?v='.Config::get('constants.assets_cache')) }}

    <!-- Custom styles for this template -->
    @show


</head>
<body>

<main>
<header>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand"
                   href="{{ route('homepage',['country'=>Request::segment(1),'local'=>Request::segment(2)]) }}">
                    {{ Html::image('img/kanbkam-logo.svg') }}

                </a>
            </div>


            <div>

                @section('countries')
                <ul class="nav navbar-nav country-list">
                    @foreach(Config::get('constants.countries') as $key =>$value)
                    <?php
                    $parameters1=$parameters;
                    if ($key == Request::segment(1)) {
                        $active = "active";


                    } else {
                        $active = "";

                    }
                    $parameters1['country'] = $key;

                    ?>
                    <li class="{{$active}}">
                        <a href="{{route(Request::route()->getName(),$parameters1)}}<?php if (Request::getQueryString()) echo "?" ?>{{\Request::getQueryString()}}">{{$value['view'][$local]}}</a>
                    </li>
                    @endforeach

                </ul>
                @show

                <ul class="nav navbar-nav navbar-right hidden-sm hidden-xs">
                    <li>
                        <a href="{{ route('homepage',['country'=>Request::segment(1),'local'=>Request::segment(2)]) }}"><?php echo Lang::get('trans.layout_home'); ?></a>
                    </li>

                    @if($local=='ar')
                    <?php $parameters_local=$parameters;
                    $parameters_local['local'] = 'en';

                    ?>
                    <li>
                        <a href="{{route(Request::route()->getName(),$parameters_local)}}<?php if (Request::getQueryString()) echo '?' ?>{{\Request::getQueryString()}}"
                           class="switch-lang">EN</a></li>
                    @else
                    <?php
                    $parameters_local=$parameters;
                    $parameters_local['local'] = 'ar';
                    ?>
                    <li>
                        <a href="{{route(Request::route()->getName(),$parameters_local)}}<?php if (Request::getQueryString()) echo '?' ?>{{\Request::getQueryString()}}"
                           class="switch-lang">العربية</a></li>
                    @endif

                    <li class="separetor">
                        <span></span>
                    </li>

                    @if(Session::has('user_data'))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="glyphicon glyphicon-user"></i> {{Session::get('user_data')->fullname}} </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{route('finduserwatchvalues',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}"><?php echo Lang::get('trans.layout_my_watch_list'); ?></a></li>
                            <li>
                                <a href="{{route('logout',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}"><?php echo Lang::get('trans.layout_logout'); ?></a>
                            </li>
                        </ul>
                    </li>

                    @else
                    <li>
                        <a href="{{route('registeration',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}"><?php echo Lang::get('trans.layout_register'); ?></a>
                    </li>
                    <li><a href="#" data-toggle="modal"
                           data-target="#sign-in"><?php echo Lang::get('trans.layout_login'); ?></a></li>

                    @endif

                    <li class="alert-div">
                        @if(Session::has('user_data'))
                        <a href="{{route('finduseralerts',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}">@if($countOfAlerts!=null)<span>{{$countOfAlerts[0]->count}}</span>@endif<i
                                class="icon icon-alert"></i></a>
                        @else
                        <a href="#" data-toggle="modal" data-target="#sign-in"><i class="icon icon-alert"></i></a>
                        @endif
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

</header>
<input type="hidden"  id="auto_complete_url" value="{{route('auto')}}">
<div class="search-row">

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-6 categories-nav">
                <ul class="list-inline">
                    <li>
                        <div class="cd-dropdown-wrapper">
                            <a class="cd-dropdown-trigger" href="#"><i class="icon-categ-icon"></i><span
                                    class="hidden-xs hidden-sm"><?php echo Lang::get('trans.home_page_categories'); ?></span></a>
                            <nav class="cd-dropdown">
                                @if(Session::has('user_data'))
                                <h2>
                                    <div class="account-info">
                                        <h3><a href="{{route('logout',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}">Logout</a> {{Session::get('user_data')->fullname}} </h3>
                                        <p>
                                          <a  href="{{route('finduserwatchvalues',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}">Watch List 
                                              <!--<span class="badge">22</span>-->
                                              </a>
                                          <a ref="{{route('finduseralerts',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}">Alert<span class="badge">@if($countOfAlerts!=null)<span>{{$countOfAlerts[0]->count}}</span>@endif</span></a>
                                        </p>
                                    </div>
                                  </h2>
                                @endif
                                <a href="#0" class="cd-close">Close</a>
                                <ul class="cd-dropdown-content list-unstyled">
                                    <li class="visible-sm visible-xs"><a
                                            href="{{ route('homepage',['country'=>Request::segment(1),'local'=>Request::segment(2)]) }}"><?php echo Lang::get('trans.layout_home'); ?></a>
                                    </li>
                                    <li class="visible-sm visible-xs"><a
                                            href="{{route('index',['country'=>Request::segment(1),'local'=>Request::segment(2),'category'=>'best-deals'])}}"><?php echo Lang::get('trans.layout_best-deals'); ?></a>
                                    </li>
                                    <li class="visible-sm visible-xs"><a
                                            href="{{route('index',['country'=>Request::segment(1),'local'=>Request::segment(2),'category'=>'popular'])}}"><?php echo Lang::get('trans.layout_popular'); ?></a>
                                    </li>
                                    <!--@if(!Session::has('user_data'))-->
                                    <!--<li><a class="visible-sm visible-xs"
                                           href="{{route('registeration',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}"><?php echo Lang::get('trans.layout_register'); ?></a>
                                    </li>
                                    <li class="visible-sm visible-xs"><a href="#" data-toggle="modal" data-target="#sign-in"><?php echo Lang::get('trans.layout_login'); ?></a>
                                    </li>-->
                                    <!--@else
                                    <li><a class="visible-sm visible-xs"
                                           href="{{route('finduserwatchvalues',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}">My
                                            Watch List</a></li>
                                    <li><a class="visible-sm visible-xs"
                                           href="{{route('logout',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}">logout</a>
                                    </li>
                                    <li><a class="visible-sm visible-xs"
                                           href="{{route('finduseralerts',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}">My
                                            Alerts</a></li>
                                    @endif-->
                                    @if($local=='ar')
                                    <?php
                                    $parameters_local=$parameters;
                                    $parameters_local['local'] = 'en'; ?>
                                    <li>
                                        <a class="visible-sm visible-xs" href="{{route(Request::route()->getName(),$parameters_local)}}<?php if (Request::getQueryString()) echo '?' ?>{{\Request::getQueryString()}}"
                                           class="switch-lang">EN</a></li>
                                    @else
                                    <?php

                                    $parameters_local=$parameters;
                                    $parameters_local['local'] = 'ar';
                                    ?>
                                    <li>
                                        <a class="visible-sm visible-xs" href="{{route(Request::route()->getName(),$parameters_local)}}<?php if (Request::getQueryString()) echo '?' ?>{{\Request::getQueryString()}}"
                                           class="switch-lang">العربية</a></li>
                                    @endif


                                    @foreach($menu_json as $key=>$value)
                                    @if($value["parent_title"][$local])
                                    <li class="has-children">
                                        <a href="#">{{$value["parent_title"][$local]}} </a>

                                        <ul class="cd-dropdown-icons is-hidden list-unstyled">
                                            <li class="go-back"><a href="#0">Menu</a></li>
                                            @foreach($value["categories"] as $key1=>$value1)
                                            <li>
                                                <a class="cd-dropdown-item"
                                                   href="{{route('index',['country'=>Request::segment(1),'local'=>Request::segment(2),'category'=>$value1['url']])}}">
                                                    {{ $value1[$local] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @else
                                    @foreach($value["categories"] as $key1=>$value1)
                                    <li>
                                        <a href="{{route('index',['country'=>Request::segment(1),'local'=>Request::segment(2),'category'=>$value1['url']])}}">{{
                                            $value1[$local] }}</a></li>
                                    @endforeach
                                    @endif
                                    @endforeach
                                     @if(!Session::has('user_data'))
                                        <li class="visible-sm visible-xs">
                                        <div class="register-div">
                                            <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#sign-in"><?php echo Lang::get('trans.layout_login'); ?></button>
                                            <button class="btn btn-default" onclick="location.href='{{route('registeration',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}';"><?php echo Lang::get('trans.layout_register'); ?></button>
                                            <!--<button type="button" class="btn btn-default">Sign Up</button>-->
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="social-log">
                                            <p>
                                                <?php echo Lang::get('trans.layout_option_sign_in'); ?>
                                            </p>
                                            <a href="{{route('provider')}}" class="facebook"></a>
                                            </div>
                                        </div>
                                        </li>
                                     @endif

                                </ul>
                                <!-- .cd-dropdown-content -->
                            </nav>
                            <!-- .cd-dropdown -->
                        </div>
                        <!-- .cd-dropdown-wrapper -->
                    </li>


                    <li class="hidden-sm hidden-xs"><a
                            href="{{route('index',['country'=>Request::segment(1),'local'=>Request::segment(2),'category'=>'best-deals'])}}"><?php echo Lang::get('trans.layout_best-deals'); ?></a>
                    </li>
                    <li class="hidden-sm hidden-xs"><a
                            href="{{route('index',['country'=>Request::segment(1),'local'=>Request::segment(2),'category'=>'popular'])}}"><?php echo Lang::get('trans.layout_popular'); ?></a>
                    </li>
                    <!--                    <li class="hidden-sm hidden-xs"><a href="#">Offers</a></li>-->
                </ul>
            </div>


            <div class="col-md-6">

                <form id="search_form"
                      action="{{route('search',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}"
                      method="get">
                    <div class="input-group">
                        <input id="search_text" name="search_text" type="text" class="form-control"
                               placeholder="<?php echo Lang::get('trans.layout_search_placeholder'); ?>"
                               value="<?php if (isset($searchText)) echo $searchText; else echo "" ?>">
                        <!--                              <span class="input-group-btn">-->
                        <!--                                <button id="search_button" class="btn btn-default" type="submit">Go!</button>-->
                        <!--                              </span>-->
                            <span class="input-group-btn">
                                 <button id="search_button" class="btn btn-default" type="submit"><i
                                         class="icon icon-search"></i></button>
                             </span>

                    </div>
                    <!-- /input-group -->
                </form>
            </div>
        </div>
    </div>
</div>


@yield('content')

</main>
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <figure>{{ Html::image('img/kanbkam-logo.svg') }}</figure>
                <ul class="list-inline">
                    <li>
                        <a href="{{ route('homepage',['country'=>Request::segment(1),'local'=>Request::segment(2)]) }}"><?php echo Lang::get('trans.layout_home'); ?></a>
                    </li>
                    <li><a href="mailto:info@kanbkam.com"><?php echo Lang::get('trans.layout_contact_us'); ?></a></li>

                @section('countries')

                    @foreach(Config::get('constants.countries') as $key =>$value)
                    <?php
                    $parameters1=$parameters;
                    if ($key == Request::segment(1)) {
                        $active = "active";

                        $parameters1['country'] = $key;
                    } else {
                        $active = "";

                    }
                    $parameters1['country'] =$key;
                    ?>
                    <li class="{{$active}}">
                        <a href="{{route(Request::route()->getName(),$parameters1)}}<?php if (Request::getQueryString()) echo "?" ?>{{\Request::getQueryString()}}">{{$value['view'][$local]}}</a>
                    </li>
                    @endforeach
                    </ul>
                    <ul class="list-inline social-links">
                        <li><a href="http://www.facebook.com/kanbkam" class="facebook"></a></li>
                        <!--                  <li><a href="#" class="twitter"></a></li>
                                          <li><a href="#" class="linkedin"></a></li>-->
                    </ul>
                    <p>© 2016-2017 Kanbkam.com</p>
            </div>
            <input id="subscribe_url"  type="hidden" value="{{route('addnewsletter')}}">
            <div class="col-sm-7">
                <form class="subscribe" id="subscribe_form" type="Post">
                    <div class="form-group">
                        <label>
                           <?php echo Lang::get('trans.layout_subscribe_newsletter'); ?>
                        </label>

                        <div class="input-group">
                            <input id="subscribe_email" name="email" class="form-control" placeholder="<?php echo Lang::get('trans.layout_enter_your_email'); ?>" type="text">
              <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><?php echo Lang::get('trans.layout_send'); ?></button>
              </span>
                        </div>
               
                    </div>
              <div  id="email_enter_success" style="display: none" class="alert alert-success alert-dismissible fade in" role="alert">
                <!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                <?php echo Lang::get('trans.layout_your_email_inserted_success'); ?>
              </div>
              <div  id="subscribe_email_error"  style="display: none"  class="alert alert-danger alert-dismissible fade in" role="alert">
                <!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                <?php echo Lang::get('trans.layout_enter_valid_email'); ?>
              </div>
               <div  id="subscribe_email_exist" style=" display: none" class="alert alert-danger alert-dismissible fade in" role="alert">
                <!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                <?php echo Lang::get('trans.layout_email_already_exist');?>
              </div>           
               <div  id="subscribe_error_message"  style="display: none" class="alert alert-danger alert-dismissible fade in" role="alert">
                <!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                <?php echo Lang::get('trans.layout_there_is_an_error_try_again_later'); ?>
              </div>   
                    <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">

                </form>

            </div>
        </div>
    </div>

</footer>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<!--Signup & Signin -->
<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="sign-in" tabindex="-1" role="dialog" aria-labelledby="sign-inLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content register-div">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="sign-inLabel"><?php echo Lang::get('trans.layout_sign_in'); ?></h4>
            </div>
            <div class="modal-body">
                <form class="user_login_form" method="POST"
                      action="{{route('doLogin',['country' => Request::segment(1), 'local' => Request::segment(2)])}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="email" name="email" class="form-control register-form-control" id="email"
                               placeholder="Email Address">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control register-form-control" id="password"
                               placeholder="Password">
                    </div>
                    <div class="form-group">
                        <button id="my_sumbit_button" type="submit"
                                class="btn btn-default"><?php echo Lang::get('trans.layout_sign_in'); ?></button>
                    </div>
                </form>
                <div class="form-group">
                    <a href="{{route('forget-info',['country' => Request::segment(1), 'local' => Request::segment(2)])}}"
                       class="forger-password"><?php echo Lang::get('trans.layout_forgot_password'); ?></a>
                </div>

                <div class="social-log">
                    <p>
                        <?php echo Lang::get('trans.layout_option_sign_in'); ?>
                    </p>
                    <a href="{{route('provider')}}" class="facebook"></a>
                </div>

            </div>

        </div>
    </div>
</div>

@section('scripts')
{{ Html::script('js/jquery-2.1.1.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/jquery-ui.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/jquery.validate.min.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/bootstrap.min.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/ie10-viewport-bug-workaround.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/modernizr.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/jquery.menu-aim.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/main.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/wow.min.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/select2.full.min.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/login_validation.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/slick.min.js'.'?v='.Config::get('constants.assets_cache')) }}
@if(Session::has('user_data'))
<script>window.login = true;</script>
@else
<script>window.login = false;</script>
@endif
@show
<![endif]-->
<!---->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-20948164-11', 'auto');
    ga('send', 'pageview');

</script>
<script type="text/javascript">
    //close mega menu
    $(document).mouseup(function (e){
        var container = $('.cd-dropdown, .cd-dropdown-trigger');
        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            if($('.cd-dropdown, .cd-dropdown-trigger').hasClass('dropdown-is-active'))
                $('.cd-dropdown, .cd-dropdown-trigger').removeClass('dropdown-is-active');
        }
    });
    $(document).ready(function(){
        $(".has-children").click(function() {
            $('.cd-dropdown-content').animate({
                    scrollTop: $(".dropdown-is-active").offset().top},
                'slow');
        });
    })

</script>

</body>
</html>