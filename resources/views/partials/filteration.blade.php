<div>
    <header>
        <h4>
			<?php echo Lang::get('trans.list_filter');?>
            <a class="reset_filter_search" href="javascript:void(0)"><i class="icon icon-reset-filtration"></i></a>
        </h4>
    </header>
    @if(Request::route()->getName()!='index' || (Request::route()->getName()=='index' &&(Request::route()->getParameter('category')=='popular' || Request::route()->getParameter('category')=='best-deals'))  )
    <div class="panel panel-default active">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#{{$category_id}}" aria-expanded="true" aria-controls="collapseOne">
                    <i class="glyphicon glyphicon-chevron-right"></i>
                    <?php echo Lang::get('trans.list_category');?>
                </a>
                <form class="filter-search">
                    <input id="search_category" class="form-control filter_search_category" type="search" placeholder="Search">
                    <span class="icon-search"></span>
                </form>
            </h4>
        </div>

        <div id="{{$category_id}}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <?php
                $categoryHasActive="";
                if(Request::input('filter_category')|| count($categories)==1){$categoryHasActive="has-active";}
                ?>
                <ul class="list-unstyled {{$categoryHasActive}}">
                    @if($categories!=null)

                    <?php
                    if(Request::input('filter_category') && !checkMyArrayOfObjectsContain($categories,Request::input('filter_category'))){
                        $object = new stdClass();
                        $object->_id = Request::input('filter_category');
                        $object->num_category = 0;
                        array_unshift($categories,$object);

                    }

                    ?>

                    @foreach($categories as $value)
                    <?php

                    $cachCategory = \Cache::get($value->_id);
                    $category = $cachCategory['text'][$local];

                    $active="";

                    if(urldecode($value->_id)==Request::input('filter_category') || count($categories)==1){
                        $active="active";
                    }
                    ?>
                    @if($category)
                    <li class="{{$active}} filter_category" >
                        <a basic_url="{{$value->_id}}" class="category_filter" href="javascript:void(0)">
                            <span>{{$category}}</span>
                            @if($active=='active')
                            <i class="glyphicon glyphicon-remove"></i>
                            @endif
                            <span class="badge">@if($value->num_category!=0){{$value->num_category}}@endif</span>

                        </a>
                    </li>
                    @endif
                    @endforeach


                    @endif



                </ul>
            </div>
        </div>

    </div>
    @endif

    <div class="panel panel-default active">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#{{$brand_id}}" aria-expanded="true" aria-controls="collapseOne">
                    <i class="glyphicon glyphicon-chevron-right"></i>
                    <?php echo Lang::get('trans.list_brand');?>
                </a>
                <form class="filter-search">
                    <input class="form-control filter_search_brand" type="search" placeholder="Search">
                    <span class="icon-search"></span>
                </form>
            </h4>
        </div>
        <div id="{{$brand_id}}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <?php
                $brandHasActive="";
                if(Request::input('brand')|| count($brands)==1){$brandHasActive="has-active";}
                ?>
                <ul class="list-unstyled {{$brandHasActive}}">
                    @if($brands!=null)
                    <?php
                    if(Request::input('brand') && !checkMyArrayOfObjectsContain($brands,Request::input('brand')) ){
                        $object = new stdClass();
                        $object->_id = Request::input('brand');
                        $object->num_brand = 0;
                        array_unshift($brands,$object);

                    }


                    ?>
                    @foreach($brands as $brand)
                    <?php $active=""; if($brand->_id == Request::input('brand') || count($brands)==1) $active="active"; ?>
                    @if($brand->_id!="")
                    <li class="{{$active}} filter_brand" >
                        <a basic_url="{{$brand->_id}}" class="brand_filter" href="javascript:void(0)">
                            <span>{{$brand->_id}}</span> @if($active=='active')<i class="glyphicon glyphicon-remove"></i>@endif
                            <span class="badge">@if($brand->num_brand!=0){{$brand->num_brand}}@endif</span>
                        </a>
                    </li>
                    @endif
                    @endforeach
                    @endif

                </ul>
            </div>
        </div>
    </div>

    <div class="panel panel-default active">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#{{$price_range_id}}" aria-expanded="true" aria-controls="collapseOne">
                    <i class="glyphicon glyphicon-chevron-right"></i>
                    <?php echo Lang::get('trans.list_price_range');?>
                </a>
            </h4>
        </div>
        <div id="{{$price_range_id}}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">

            <div class="panel-body">
                <form id="{{$form_price_range}}">
                    <div class="form-group col-xs-6">
                        <input  id={{$filter_price_from}} type="text" class="filter_price_range_from form-control numeric " placeholder="<?php echo Lang::get('trans.list_from');?>" value="{{Request::input('price_from') }}" >
                    </div>
                    <div class="form-group col-xs-6">
                        <input id={{$filter_price_to}} type="text" class="filter_price_range_to form-control numeric " placeholder="<?php echo Lang::get('trans.list_to');?>" value="{{Request::input('price_to') }}">
                    </div>
                    <button type="submit" class="btn btn-default"><?php echo Lang::get('trans.list_submit');?></button>
                </form>
            </div>
        </div>
    </div>

    <div class="panel panel-default active price-range-panel">
        <div class="panel-heading" role="tab" id="headingFour">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#{{$price_drop_id}}" aria-expanded="true" aria-controls="collapseOne">
                    <i class="glyphicon glyphicon-chevron-right"></i>
                    <?php echo Lang::get('trans.list_price_drop');?>
                </a>
            </h4>
        </div>
        <div id="{{$price_drop_id}}" class="panel-collapse collapse in price-drop" role="tabpanel" aria-labelledby="headingFour">
            <div class="panel-body">
                <div class="price-range">
                    <input type="range" class="price_drop_range" value="<?php if(Request::input('price_drop'))echo(Request::input('price_drop'));else echo("0"); ?>">
                </div>
            </div>
        </div>
    </div>





    <div class="panel panel-default active">
        <div class="panel-heading" role="tab" id="headingfive">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#{{$seller_id}}" aria-expanded="true" aria-controls="collapseOne">
                    <i class="glyphicon glyphicon-chevron-right"></i>
                    <?php echo Lang::get('trans.list_seller');?>
                </a>
            </h4>
        </div>
        <div id="{{$seller_id}}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingfive">
            <?php $souqChecked=""; $othersChecked=""; if(strpos(Request::input('seller'), 'souq') !== false){$souqChecked='checked';} if(strpos(Request::input('seller'), 'others') !== false){$othersChecked='checked';}?>
            <div class="panel-body">
                <form>
                    <div class="checkbox">
                        <label>
                            <?php echo Lang::get('trans.list_souq');?> <input search_text="souq" type="checkbox" class="type seller" id="souq_checkbox" {{$souqChecked}}>
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <?php echo Lang::get('trans.list_other');?> <input search_text="others" type="checkbox" class="type seller" id="others_checkbox" {{$othersChecked}}>
                        </label>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if(in_array(Request::input('filter_category'),Config::get('target.categories')) ||in_array(Request::route()->getParameter('category'),Config::get('target.categories')) )
    <div class="panel panel-default active">
        <div class="panel-heading" role="tab" id="headingSex">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" href="#gender" aria-expanded="true" aria-controls="collapseOne">
                    <i class="glyphicon glyphicon-chevron-right"></i>
                    <?php echo Lang::get('trans.list_gender');?>
                </a>
            </h4>
        </div>


        <div id="gender" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingfive">
            <?php $maleChecked=""; $femaleChecked=""; if(strpos(Request::input('target'), 'Men') !== false){$maleChecked='checked';} if(strpos(Request::input('target'), 'Women') !== false){$femaleChecked='checked';}?>
            <div class="panel-body">
                <form>
                    <div class="checkbox">
                        <label>
                            <?php echo Lang::get('trans.list_male');?> <input search_text="Men" type="checkbox" class="type gender" id="male_checkbox" {{$maleChecked}}>
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <?php echo Lang::get('trans.list_female');?> <input  search_text="Women" class="type gender" type="checkbox" id="female_checkbox" {{$femaleChecked}}>
                        </label>
                    </div>
                </form>
            </div>
        </div>

    </div>
    @endif



</div>