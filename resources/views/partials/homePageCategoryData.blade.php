


<div class="row">
    <div class="col-md-2 categ-discription hidden-sm hidden-xs">
        <div class="thumbnail">
            <div class="caption">
                <h2><?php echo Lang::get('trans.home_page_price_drops');?></h2>
                <p><?php echo Lang::get('trans.home_page_price_drops_description');?></p>
                @if(Request::input('category'))
                <a class="more" href="{{route('index',['country'=>Request::segment(1),'local'=>Request::segment(2),'category'=>Request::input('category')])}}?sort=chan_desc"><?php echo Lang::get('trans.home_page_viewAll');?></a>
                @else
                <a class="more" href="{{route('index',['country'=>Request::segment(1),'local'=>Request::segment(2),'category'=>$defaultCategory])}}?sort=chan_desc"><?php echo Lang::get('trans.home_page_viewAll');?></a>
                @endif
                
            </div>
        </div>


    </div>
            <div class="col-md-10 list-container">
                    <ul class="list-inline products-list responsive-slide">
                            @for ($i = 0; $i < count($productsOrderedBYPriceDrops); $i++)
                    <?php
                    if($productsOrderedBYPriceDrops[$i]['changePercent']>0){$chartStatus='icon-chart-up'; $color='red';}else{$chartStatus='icon-chart-down'; $color='green';}
                     ?>
                        <li class="thumbnail">
                            <ul class="list-inline actions">
                                <li>
                                    <a target="_blank" href="{{$productsOrderedBYPriceDrops[$i]['souqLink']}}/io/"><i class="icon-alert"></i> <i class="plus">+</i></a>
                                </li>
                                <li>
<!--                                    <a href="{{route('search',['country'=>Request::segment(1),'local'=>Request::segment(2),'search_text'=>$productsOrderedBYPriceDrops[$i]['item_id']])}}"><i class="icon-add-to-cart"></i></a>-->
                                    <a href="{{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>$productsOrderedBYPriceDrops[$i]['item_id'] ])}}#{{$productsOrderedBYPriceDrops[$i]['id']}}_new_souq_price_desired"><i class="icon-add-to-cart"></i></a>

                                </li>
                            </ul>
<!--                                <a href="{{route('search',['country'=>Request::segment(1),'local'=>Request::segment(2),'search_text'=>$productsOrderedBYPriceDrops[$i]['item_id']])}}">-->
                                <a href="{{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>$productsOrderedBYPriceDrops[$i]['item_id'] ])}}">

                                <figure>
                                    <img src="{{$productsOrderedBYPriceDrops[$i]['image']}}" alt="...">
                                </figure>
                                <div class="caption">
                                    <h3>{{$productsOrderedBYPriceDrops[$i]['title']}}</h3>
                                    <p>{{$productsOrderedBYPriceDrops[$i]['currentValue']}} <span>{{$currency}}</span></p>
                                    @if($productsOrderedBYPriceDrops[$i]['changePercent'] != 0 || $productsOrderedBYPriceDrops[$i]['changePercent'] != -0)
                                    <div>
                                        <span class="old-price">{{number_format((($productsOrderedBYPriceDrops[$i]['currentValue']))/((($productsOrderedBYPriceDrops[$i]['changePercent'])/100)+1),1)}}</span>
                                        <ul class="list-inline change-percent {{$color}}">
                                            <li>
                                                <span>{{ number_format($productsOrderedBYPriceDrops[$i]['changePercent'], 1)}}%</span>
                                            </li>
                                            <li>
                                                <i class="{{$chartStatus}}"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    @endif
                                </div>

                            </a>
                        </li>
                         @endfor
                    </ul>
        </div>

</div>


