@if($items != null)
@foreach($items as $product)
<?php if($product['currentValue'] != 'out of stock')if($product['changePercent']>0){$chartStatus='icon-chart-up'; $color='red';}else{$chartStatus='icon-chart-down';$color='green';} ?>
<li class="thumbnail col-md-3 col-sm-4 col-xs-6">
    <ul class="list-inline actions">
        <li>
            <a href="{{$product['souqLink']}}/i/"><i class="icon-alert"></i> <i class="plus">+</i></a>
        </li>
        <li>

	@if (Request::segment(1) == 'getFilterData')
            <a href="{{route('product.show',['country'=>Request::input('country'),'local'=>Request::input('local'),'number'=>$product['item_id'] ])}}#{{$product['id']}}_new_souq_price_desired"><i class="icon-add-to-cart"></i></a>

	@else
        <a href="{{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>$product['item_id'] ])}}#{{$product['id']}}_new_souq_price_desired"><i class="icon-add-to-cart"></i></a>
	@endif
        </li>
    </ul>
	@if (Request::segment(1) == 'getFilterData')
    <a href="{{route('product.show',['country'=>Request::input('country'),'local'=>Request::input('local'),'number'=>$product['item_id'] ])}}">
	@else
        <a href="{{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>$product['item_id'] ])}}">
	@endif
        <figure>
            @if($product['image'] && $product['image'] != "/")
            <img src="{{$product['image']}}"/>
            @else
            {{ Html::image('img/no-img-Product.png') }}
            @endif

        </figure>

        <div class="caption">
            <h3>{{$product['fullTitle']}}</h3>
            @if($product['currentValue'] !== "1")
            <p>{{$product['currentValue']}} <span>{{$currency}}</span></p>
            @if($product['changePercent']!=0)
            <div>
                <span class="old-price">{{number_format((($product['currentValue']))/((($product['changePercent'])/100)+1),2)}}</span>
                <ul class="list-inline change-percent {{$color}}">
                    <li>
                        <span>{{number_format($product['changePercent'],2)}}</span>
                    </li>
                    <li>
                        <i class="{{$chartStatus}}"></i>
                    </li>
                </ul>
            </div>
            @endif
            @else
            <div>out of stock</div>
            @endif
        </div>

    </a>
</li>
@endforeach
@endif
