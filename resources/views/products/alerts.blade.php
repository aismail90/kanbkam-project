@extends('layouts.base')
@section('style')
@parent
@endsection
@section('scripts')
@parent
<script>
    $(document).ready(function () {
        $(".select2").select2({
            minimumResultsForSearch: -1
        });
    });
    $(document).ready(function () {
        $("#e1").select2();
        //Bootstrap Accordion Active
        (function () {
            $(".panel").on("show.bs.collapse hide.bs.collapse", function (e) {
                if (e.type == 'show') {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });


        }).call(this);
    });
</script>
{{ Html::script('js/details.js') }}
@endsection

@section('content')


<div class="container-fluid content ">
    @if($items!=null)
    <input id="ajax_url_watch_form" type="hidden" value="{{route('addWatchValues')}}">
    <input id="ajax_url_delete_watch_form" type="hidden" value="{{route('deleteWatchValues')}}">
    <section class="col-md-10 wow fadeIn animated alert-page">
        <header>
            <h2><span><i class="icon-alert"></i><i class="icon-sand-clock"></i></span><?php echo Lang::get('trans.alerts_your_alerts');?></h2>
        </header>
        <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
            <?php $active = "";
            $i = 1; ?>

            @foreach($items as $product)
            <?php if ($i == 1) {
                $active = 'active';
                $expanded = true;
                $collapse ="collapse in";

            } else {
                $active = "";
                $expanded=false;
                $collapse ="collapse";
            } ?>
            <div class="panel panel-default {{$active}}">
                <div class="panel-heading" role="tab" id="heading_{{$i}}">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$i}}"
                           aria-expanded="{{$expanded}}" aria-controls="collapse_{{$i}}">
                            <?php $prod = $product->product ?>
                            <input type="hidden" name="product_id" id="product_id" value="{{$prod[0]->_id}}">
                            <i class="icon-expand"></i>{{$prod[0]->title->$local}}
                        </a>
                        <a href="{{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>$prod[0]->item_id ])}}" class="product-page"><i class="glyphicon glyphicon-share"></i></a>
                    </h4>
                </div>
                <div id="collapse_{{$i}}" class="panel-collapse {{$collapse}}" role="tabpanel"
                     aria-labelledby="heading_{{$i}}">
                    <div class="panel-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <th><?php echo Lang::get('trans.details_watch_type'); ?></th>
                            <th><?php echo Lang::get('trans.alerts_desired');?> <span class="hidden-xs"><?php echo Lang::get('trans.details_watch_price'); ?></span></th>
                            <th><?php echo Lang::get('trans.alerts_current');?> <span class="hidden-xs"><?php echo Lang::get('trans.details_watch_price'); ?></span></th>
                            <th><?php echo Lang::get('trans.details_diff');?><span class="hidden-xs"><?php echo Lang::get('trans.details_erence');?></span></th>
                            <th></th>
                            <th></th>
                            </thead>
                            <tbody>

                            @foreach(Config::get('constants.merchants') as $key =>$value)
                            @foreach($value as $val)
                            <tr>
                                <td><?php echo Lang::get('trans.details_'.$val.'_merchant');?></td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">{{$currency}}</div>
                                            <?php $watched = false;
                                            if (property_exists($product->watch->new, $val)) $watched = true;
                                            if ($watched && $prod[0]->stats->new->$val->current->value != 0) {
                                                $difference = $prod[0]->stats->new->$val->current->value - $product->watch->new->$val->value;
                                            } else {
                                                $difference = '-';
                                            }


                                            ?>

                                            <input class="form-control numeric" name="price_desired"
                                                   id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_price_desired" type="text"
                                                   value="<?php if ($watched) {
                                                       echo $product->watch->new->$val->value;
                                                   } ?>">
                                            <input type="hidden" name="current_value"
                                                   id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_current_value"
                                                   value="{{$prod[0]->stats->$key->$val->current->value}}">
                                        </div>
                                    </div>
                                </td>
                                @if($prod[0]->stats->new->$val->current->value!='0')
                                <td>{{$prod[0]->stats->new->$val->current->value}}</td>
                                @else
                                <td><?php echo Lang::get('trans.product_status_out_of_stock'); ?></td>
                                @endif
                                <td id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_difference">{{$difference}}</td>
                                <td>
                                    @if($watched)
                                    @if($product->watch->new->$val->status == 1)
                                    <span id="watch_status" class="badge"><?php echo Lang::get('trans.alerts_triggered');?></span>
                                    @else
                                    <span id="watch_status" class="badge"><?php echo Lang::get('trans.alerts_non_triggered');?></span>
                                    @endif
                                    @endif
                                </td>
                                <td class="actions">

                                    <ul class="list-inline actions-menu hidden-xs">
                                        @if($watched)
                                        <li>
                                            <a id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_watch_price_add"
                                               href="javascript:void(0);" class="btn btn-default watch_form"
                                               style="display: none"><?php echo Lang::get('trans.details_watch_button');?></a>
                                        </li>
                                        <li>
                                            <a id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_watch_price_update"
                                               href="javascript:void(0);" class="watch_form"><i class="icon-edit"></i><?php echo Lang::get('trans.details_update_button');?></a>
                                        </li>
                                        <li>
                                            <a id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_watch_price_delete"
                                               href="javascript:void(0);" class="delete_form"><i class="icon-delet"></i><?php echo Lang::get('trans.details_delete_button');?></a>
                                        </li>

                                        @else
                                        <li>
                                            <a id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_watch_price_add"
                                               href="javascript:void(0);" class="btn btn-default watch_form"><?php echo Lang::get('trans.details_watch_button');?>
                                            </a>
                                        </li>
                                        <li>
                                            <a id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_watch_price_update"
                                               href="javascript:void(0);" class="watch_form"
                                               style="display: none"><i class="icon-edit"></i><?php echo Lang::get('trans.details_update_button');?></a>
                                        </li>
                                        <li>
                                            <a id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_watch_price_delete"
                                               href="javascript:void(0);" class="delete_form"
                                               style="display: none"><i class="icon-delet"></i><?php echo Lang::get('trans.details_delete_button');?></a>
                                        </li>
                                        @endif

                                    </ul>

                                    <ul class="list-inline actions-menu visible-xs">
                                        @if($watched)
                                        <li>
                                            <a id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_watch_price_add_m"
                                               href="javascript:void(0);" class="btn btn-default watch_form"
                                               style="display: none"><?php echo Lang::get('trans.details_watch_button');?></a>
                                        </li>
                                        <li>
                                            <a id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_watch_price_update_m"
                                               href="javascript:void(0);" class="watch_form"><i class="icon-edit"></i></a>
                                        </li>
                                        <li>
                                            <a id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_watch_price_delete_m"
                                               href="javascript:void(0);" class="delete_form"><i class="icon-delet"></i></a>
                                        </li>

                                        @else
                                        <li>
                                            <a id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_watch_price_add_m"
                                               href="javascript:void(0);" class="btn btn-default watch_form"><?php echo Lang::get('trans.details_watch_button');?>
                                            </a>
                                        </li>
                                        <li>
                                            <a id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_watch_price_update_m"
                                               href="javascript:void(0);" class="watch_form"
                                               style="display: none"><i class="icon-edit"></i></a>
                                        </li>
                                        <li>
                                            <a id="{{$prod[0]->_id}}_{{$key}}_{{$val}}_watch_price_delete_m"
                                               href="javascript:void(0);" class="delete_form"
                                               style="display: none"><i class="icon-delet"></i></a>
                                        </li>
                                        @endif

                                    </ul>
                                </td>




                            </tr>
                            @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                <?php $i++ ?>
            @endforeach

        </div>
        <nav aria-label="Page navigation">
            {!! $items->render() !!}

    </section>
    @else
    <section class="col-md-10 wow fadeIn animated alert-page">
        <div class="no-data">
            <span>{{Html::image('img/add-alert.svg')}}</span>
<!--            <h4>--><?php //echo Lang::get('trans.alerts_omg');?><!--</h4>-->

            <p>
                <?php echo Lang::get('trans.alerts_you_have_no_alerts_yet');?>
            </p>
        </div>
    </section>
    @endif
    <aside class="col-md-2 banner in-listing hidden-sm hidden-xs wow fadeIn animated">
        {{ Html::image('img/160x600.png') }}
    </aside>
</div>

@endsection


