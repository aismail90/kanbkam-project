<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="../../favicon.ico">
    <title>price indexing - @yield('title')</title>
@section('scripts')
    {{ Html::script('js/jquery.min.js') }}
    {{ Html::script('js/jquery.validate.min.js') }}
    {{ Html::script('js/form_validation.js') }}
    {{ Html::script('js/bootstrap.min.js') }}
    {{ Html::script('js/holder.min.js') }}
    {{ Html::script('js/ie10-viewport-bug-workaround.js') }}
    {{ Html::script('js/ie10-viewport-bug-workaround.js') }}
    @show
@section('style')
    {{ Html::style('css/bootstrap.min.css') }}
    {{ Html::style('css/ie10-viewport-bug-workaround.css') }}
    {{ Html::style('css/registeration.css') }}
    {{ Html::script('js/ie-emulation-modes-warning.js') }}
    {{ Html::style('css/carousel.css') }}
    @show
</head>
<!-- NAVBAR
================================================== -->
<body>

<div class="navbar-wrapper">
    <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    @if(Request::segment(2)== 'ar' || Request::segment(2) == 'en')
                    <a class="navbar-brand" href="{{ route('product.index',['country'=>Request::segment(1),'local'=>Request::segment(2)]) }}">Price indexing</a>
                    @else
                    <a class="navbar-brand" href="{{ route('product.index',['country'=>'eg','local'=>'ar']) }}">Price indexing</a>
                    @endif
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="{{ route('product.index',['country'=>Request::segment(1),'local'=>Request::segment(2)]) }}">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                    @if(Session::has('user_data'))
                    <div class="col-lg-5" style="padding-top: 8px; float: right;width: 30%">
                     <div> Hi {{Session::get('user_data')->fullname}}</div>
                       <a href="">logout</a>
                    </div>



                    @else
                    <div class="col-lg-5" style="padding-top: 8px; float: right;width: 30%">
                        <a href="{{route('userlogin')}}">login</a>
                    </div>
                    @endif
                    <div class="col-lg-5" style="padding-top: 8px; float: right;width: 30%">

                        @if(Session::has('user_data'))
                        <form action="{{route('search',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}" method="get">
                         @else
                            <form action="{{route('search',['country'=>'eg','local'=>'ar'])}}" method="get">
                         @endif
                        <div class="input-group">
                            <input id="search_text" name="search_text" type="text" class="form-control" placeholder="Search for product..." value="<?php if(isset($searchText)) echo $searchText;else echo"" ?>">
                              <span class="input-group-btn">
                                <button id="search_button" class="btn btn-default" type="submit">Go!</button>
                              </span>

                        </div><!-- /input-group -->
                                </form>

                    @if(Session::has('user_data'))
                    <a href="{{route('finduserwatchvalues',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}">view my watches</a>
                    @endif
                    </div>

                @show

            </div>

                </div>
            </div>
        </nav>

    </div>
</div>


<!-- Carousel
================================================== -->


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container marketing" style="margin-top: 117px;">
    <input type="hidden" id="index_route" value="{{ route('product.index',['country'=>Request::segment(1),'local'=>Request::segment(2)]) }}">

    <!-- Three columns of text below the carousel -->


        @yield('content')


    <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2015 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
    </footer>

</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
</body>
</html>

