@extends('layouts.base')
@section('style')
@parent
@endsection
@section('scripts')
@parent
<script type="text/javascript">


    function setResponsiveSlide() {
        console.log($("#relatedProductsByBrand ul:first"));
        setTimeout(function () {

            $("#Brand ul:first").slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 5,
                slidesToScroll: 5,
                rtl:window.rtl,
                responsive: [
                    {
                        breakpoint: 960,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3

                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }
                ]
            });
        }, 500);

    }
    $(document).ready(function () {
        var local = "{{Request::segment(2)}}" ;
         window.rtl =false;
        if(local=='ar'){
            window.rtl= true;
        }

        $(".select2").select2({
            minimumResultsForSearch: -1
        });
        $('.responsive-slide').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 5,
            rtl:rtl,
            responsive: [
                {
                    breakpoint: 960,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3

                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        });
    });

</script>
{{ Html::script('js/facebook_share.js') }}
{{ Html::script('js/details.js') }}
{{ Html::script('js/details_switch_merchant.js') }}


<!-- end -->
@endsection
@section('content')
@if(count($searchData))
<input id="ajax_url_watch_form" type="hidden" value="{{route('addWatchValues')}}">
<input id="ajax_url_delete_watch_form" type="hidden" value="{{route('deleteWatchValues')}}">
<input type="hidden" name="product_id" id="product_id" value="{{$searchData[0]['id']}}">
<input type="hidden" name="current_url" id="current_url"
       value="{{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>Request::segment(5)])}}">
<input id="facebook_id" type="hidden" value="{{Config('constants.FACEBOOK_CLIENT_ID')}}">
<div class="container-fluid content wow fadeIn animated ">
<div class="row breadcrumb-row">
    <div class="col-sm-8">
        <ol class="breadcrumb">
            <li><a href="{{route('homepage',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}"><?php echo Lang::get('trans.layout_home'); ?></a>
            </li>
            <li>
                <a href="{{route('index',['country'=>Request::segment(1),'local'=>Request::segment(2),'category'=>$searchData[0]['category']])}}">{{$categoryText}}</a>
            </li>
            <li class="active"><a class="active" href="javascript:void(0)">{{$searchData[0]['title']}}</a></li>
        </ol>
    </div>
    <div class="col-sm-4">
        <form class="sort-by">
            <select id="list_by_merchant" class="form-control select2">
                @foreach($listByArray as $option)
                <?php $selected = "";
                if ($option == $defaultListValue) {
                    $selected = 'selected';
                } ?>
                <option merchant_type="{{$option}}"
                {{$selected}} >
                <?php echo Lang::get('trans.details_' . $option); ?>
                </option>
                @endforeach

            </select>
        </form>
    </div>
</div>

<section class="product-info">
<div class="col-md-6">
    <figure>
        @if($searchData[0]['image'] && $searchData[0]['image'] != "/")
        <img src="{{$searchData[0]['image']}}"/>
        @else
        {{ Html::image('img/no-img-Product.png') }}
        @endif
    </figure>
    <span class="separator"></span>
</div>
<div class="col-md-6">
<header>
    <h1>{{ $searchData[0]['fullTitle'] }}</h1>
</header>
<article>
<?php $souqStatus = "";
$othersStatus = "";
if ($defaultListValue == 'listed_by_souq') {
    $showSouq = '';
    $souqStatus = 'active';;
    $showOthers = 'display:none';
} else {
    $showSouq = 'display:none';
    $showOthers = '';
    $othersStatus = 'active';
} ?>
<div id="souq_price" class="price" style="{{$showSouq}}">
    @if($searchData[0]['newSouqStats']->inventory == 0 )
    <span class="current-price"><?php echo Lang::get('trans.product_status_out_of_stock'); ?></span>
    @elseif($searchData[0]['souqCurrentValue']!=0 ||$searchData[0]['newSouqStats']->low->value !=
    $searchData[0]['newSouqStats']->high->value )
    <span class="current-price"><?php echo Lang::get('trans.details_current_price'); ?>
        ,@if($searchData[0]['souqCurrentValue']<=$searchData[0]['newSouqStats']->low->value)<span class="green"><?php echo Lang::get('trans.details_very_good'); ?></span>@elseif($searchData[0]['souqCurrentValue']<=$searchData[0]['newSouqStats']->average->value)<span
            class="green"><?php echo Lang::get('trans.details_good'); ?></span>@else<span class="red"><?php echo Lang::get('trans.details_not_good'); ?></span>@endif</span>
    @endif
    <p>
        @if($searchData[0]['souqCurrentValue']!=="1")
            {{$searchData[0]['souqCurrentValue']}}<span class="currency">{{$currency}}</span>
        @else
        <?php echo Lang::get('trans.product_status_out_of_stock'); ?>
        @endif



    </p>
    @if($searchData[0]['souqChangePercent']!=0)
    <span class="old-price">{{number_format((($searchData[0]['souqCurrentValue']))/((($searchData[0]['souqChangePercent'])/100)+1),2)}}</span>
    @endif
</div>

<div id="others_price" class="price" style="{{$showOthers}}">

    @if($searchData['0']['newOthersStats']->inventory == 0 )
    <span class="current-price"><?php echo Lang::get('trans.product_status_out_of_stock'); ?></span>
    @elseif($searchData[0]['othersCurrentValue']!=0 && ($searchData[0]['newOthersStats']->low->value !=
    $searchData[0]['newOthersStats']->high->value))
    <span class="current-price"><?php echo Lang::get('trans.details_current_price'); ?>
        ,@if($searchData[0]['othersCurrentValue']<=$searchData[0]['newOthersStats']->low->value)<span class="green"><?php echo Lang::get('trans.details_very_good'); ?></span>@elseif($searchData[0]['othersCurrentValue']<=$searchData[0]['newOthersStats']->average->value)<span
            class="green"><?php echo Lang::get('trans.details_good'); ?></span>@else<span class="red"><?php echo Lang::get('trans.details_not_good'); ?></span>@endif</span>
    @endif
    <p>

        @if($searchData[0]['othersCurrentValue']!=="1")
            {{$searchData[0]['othersCurrentValue']}}<span class="currency">{{$currency}}</span>
        @else
        <?php echo Lang::get('trans.product_status_out_of_stock'); ?>
        @endif
    </p>
    @if($searchData[0]['othersChangePercent']!=0)
    <span class="old-price">{{number_format((($searchData[0]['othersCurrentValue']))/((($searchData[0]['othersChangePercent'])/100)+1),2)}}</span>
    @endif
</div>
@if($searchData[0]['newSouqStats']->inventory != 0 )
<div id="souq_buy" class="buy-now" style="{{$showSouq}}">
    <a target="_blank" href="{{$searchData[0]['souqLink']}}/io/" class="btn btn-default"><i
            class="icon-add-to-cart"></i> <span><?php echo Lang::get('trans.details_buy_now'); ?></span> <span
            class="souq-logo"></span>
    </a>
</div>
@endif
@if($searchData[0]['newOthersStats']->inventory != 0 )
<div id="others_buy" class="buy-now" style="{{$showOthers}}">
    <a target="_blank" href="{{$searchData[0]['souqLink']}}/io/" class="btn btn-default"><i
            class="icon-add-to-cart"></i> <span><?php echo Lang::get('trans.details_buy_now'); ?></span> <span
            class="souq-logo"></span>
    </a>
</div>
@endif


<div class="clearfix"></div>
<ul id="souq_ul" class="list-inline price-summary" style="{{$showSouq}}">
    <li>
        <div>
            <h6><?php echo Lang::get('trans.details_highest_price'); ?></h6>
            <span class="red value">{{$searchData[0]['newSouqStats']->high->value}}</span>
        </div>

    </li>
    <li>
        <div>
            <h6><?php echo Lang::get('trans.details_lowest_price'); ?></h6>
            <span class="green value">{{$searchData[0]['newSouqStats']->low->value}}</span>
        </div>

    </li>
    <li>
        @if($searchData[0]['souqChangePercent']!=0 ||$searchData[0]['souqChangePercent']!='out of stock')
        <?php $classDrop = 'green';
        if ($searchData[0]['souqChangePercent'] > 0) $classDrop = 'red'; ?>
        <div>
            <h6><?php if ($searchData[0]['souqChangePercent'] < 0) {
                    echo Lang::get('trans.details_recent_price_drop');
                } else {
                    echo Lang::get('trans.details_recent_price_raise');
                } ?></h6>
            <span class="{{$classDrop}} value">{{number_format($searchData[0]['souqChangePercent'],1)}}%</span>

            <div class="currency-value">
                <span class="equal">=</span>
                <span class="">{{-1*(number_format(((($searchData[0]['souqCurrentValue']))/((($searchData[0]['souqChangePercent'])/100)+1)) - $searchData[0]['souqCurrentValue'],1)) }}</span>
                <span class="currency">{{$currency}}</span>
            </div>

        </div>
        @else
        <div><?php echo Lang::get('trans.details_no_change_in_price'); ?></div>
        @endif
    </li>
</ul>

<div class="clearfix"></div>
<ul id="others_ul" class="list-inline price-summary" style="{{$showOthers}}">
    <li>
        <div>
            <h6><?php echo Lang::get('trans.details_highest_price'); ?></h6>
            <span class="red value">{{$searchData[0]['newOthersStats']->high->value}}</span>
        </div>

    </li>
    <li>
        <div>
            <h6><?php echo Lang::get('trans.details_lowest_price'); ?></h6>
            <span class="green value">{{$searchData[0]['newOthersStats']->low->value}}</span>
        </div>

    </li>
    <li>
        @if($searchData[0]['othersChangePercent']!=0)
        <?php $classDrop = 'green';
        if ($searchData[0]['othersChangePercent'] > 0) $classDrop = 'red'; ?>
        <div>
            <h6><?php $sign = "";
                if ($searchData[0]['othersChangePercent'] < 0) {
                    echo Lang::get('trans.details_recent_price_drop');
                } else {
                    echo Lang::get('trans.details_recent_price_raise');
                    $sign = "+";
                } ?></h6>
            <span
                class="{{$classDrop}} value">{{$sign}}{{number_format($searchData[0]['othersChangePercent'],1)}}%</span>

            <div class="currency-value">
                <span class="equal">=</span>
                <span class="">{{$sign}}{{number_format(-1*(((($searchData[0]['othersCurrentValue']))/((($searchData[0]['othersChangePercent'])/100)+1)) - $searchData[0]['othersCurrentValue']),1) }}</span>

                <span class="currency">{{$currency}}</span>
            </div>
        </div>
        @else
        <div><?php echo Lang::get('trans.details_no_change_in_price'); ?></div>
        @endif
    </li>
</ul>


<div class="expected-price">
    <ul class="list-inline social-share">
        <li>
            <a href="javascript:void(0)" class="facebook" id="facebook"></a>
            <!--                        <a href="http://www.facebook.com/sharer.php?u={{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>Request::segment(5)])}}"  onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=400');return false;" class="facebook"></a>-->
        </li>
        <li>
            <a href="https://twitter.com/intent/tweet?url={{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>Request::segment(5)])}}"
               onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
               class="twitter"></a>
        </li>
        <li>
            <a href="https://plus.google.com/share?url={{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>Request::segment(5)])}}"
               onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
               class="linked-in"></a>
        </li>
    </ul>
</div>

<div class="allert-div">
    <h3>
                  <span class="add-alert">
                  <i class="icon-alert"></i> <span class="plus">+</span>
                  </span>
        <?php echo Lang::get('trans.details_create_price_alert'); ?>

    </h3>
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>
                <?php echo Lang::get('trans.details_watch_type'); ?>
            </th>
            <th>
                <?php echo Lang::get('trans.alerts_desired'); ?> <span class="hidden-xs"><?php echo Lang::get('trans.details_watch_price'); ?></span>
            </th>
            <th>
                <?php echo Lang::get('trans.alerts_current'); ?> <span class="hidden-xs"><?php echo Lang::get('trans.details_watch_price'); ?></span>
            </th>
            <th><?php echo Lang::get('trans.details_diff');?><span class="hidden-xs"><?php echo Lang::get('trans.details_erence');?></span></th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <?php
        $merchants=config('constants.merchants');
        $newMerchants=$merchants['new'];

        ?>

        @foreach($newMerchants as $merchant)
            <tr>
                <td>
                    <?php echo Lang::get('trans.details_'.$merchant.'_merchant'); ?>
                </td>
                <td>
                    <?php $difference = '-';
                    $watched = false;
                    if (!empty($watchData) && property_exists($watchData, 'new') && property_exists($watchData->new, $merchant)) {
                        $watched = true;
                        if ($searchData[0][$merchant.'CurrentValue'] !== '1') {
                            $difference = $searchData[0][$merchant.'CurrentValue'] - $watchData->new->$merchant->value;
                        }
                    } ?>
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">{{$currency}}</div>
                            <input class="form-control numeric" type="text" pattern="[0-9]*" name="price_desired"
                                   id="{{$searchData[0]['id']}}_new_{{$merchant}}_price_desired"
                                   value="<?php if ($watched) echo($watchData->new->$merchant->value) ?>">
                        </div>
                    </div>
                    <input type="hidden" name="current_value" id="{{$searchData[0]['id']}}_new_{{$merchant}}_current_value"
                           value="{{$searchData[0][$merchant.'CurrentValue']}}">

                </td>
                <td>
                    @if($searchData[0][$merchant.'CurrentValue']!=='1')
                    {{$currency}}{{$searchData[0][$merchant.'CurrentValue']}}
                    @else
                    <?php echo Lang::get('trans.product_status_out_of_stock'); ?>
                    @endif
                </td>
                <td id="{{$searchData[0]['id']}}_new_{{$merchant}}_difference">
                    {{$difference}}
                </td>
                <td class="actions">
                    <ul class="list-inline actions-menu hidden-xs">
                        @if($watched)
                        <li><a id="{{$searchData[0]['id']}}_new_{{$merchant}}_watch_price_add" href="javascript:void(0);"
                               class="btn btn-default watch_form"
                               style="display: none"><?php echo Lang::get('trans.details_watch_button'); ?></a></li>
                        <li><a id="{{$searchData[0]['id']}}_new_{{$merchant}}_watch_price_update" href="javascript:void(0);"
                               class=" watch_form"><i
                                    class="icon-edit"></i><?php echo Lang::get('trans.details_update_button'); ?></a></li>
                        <li><a id="{{$searchData[0]['id']}}_new_{{$merchant}}_watch_price_delete" href="javascript:void(0);"
                               class=" delete_form"><i
                                    class="icon-delet"></i><?php echo Lang::get('trans.details_delete_button'); ?></a></li>
                        @else
                        <li><a id="{{$searchData[0]['id']}}_new_{{$merchant}}_watch_price_add" href="javascript:void(0);"
                               class="btn btn-default watch_form"
                            "><?php echo Lang::get('trans.details_watch_button'); ?></a></li>
                        <li><a id="{{$searchData[0]['id']}}_new_{{$merchant}}_watch_price_update" href="javascript:void(0);"
                               class="watch_form" style="display: none"><i
                                    class="icon-edit"></i><?php echo Lang::get('trans.details_update_button'); ?></a></li>
                        <li><a id="{{$searchData[0]['id']}}_new_{{$merchant}}_watch_price_delete" href="javascript:void(0);"
                               class="delete_form" style="display: none"><i
                                    class="icon-delet"></i><?php echo Lang::get('trans.details_delete_button'); ?></a></li>
                        @endif
                    </ul>

                    <ul class="list-inline actions-menu visible-xs">
                        @if($watched)
                        <li><a id="{{$searchData[0]['id']}}_new_{{$merchant}}_watch_price_add_m" href="javascript:void(0);"
                               class="btn btn-default watch_form"
                               style="display: none"><?php echo Lang::get('trans.details_watch_button'); ?></a></li>
                        <li><a id="{{$searchData[0]['id']}}_new_{{$merchant}}_watch_price_update_m" href="javascript:void(0);"
                               class=" watch_form"><i class="icon-edit"></i></a></li>
                        <li><a id="{{$searchData[0]['id']}}_new_{{$merchant}}_watch_price_delete_m" href="javascript:void(0);"
                               class=" delete_form"><i class="icon-delet"></i></a></li>
                        @else
                        <li><a id="{{$searchData[0]['id']}}_new_{{$merchant}}_watch_price_add_m" href="javascript:void(0);"
                               class="btn btn-default watch_form"
                            "><?php echo Lang::get('trans.details_watch_button'); ?></a></li>
                        <li><a id="{{$searchData[0]['id']}}_new_{{$merchant}}_watch_price_update_m" href="javascript:void(0);"
                               class="watch_form" style="display: none"><i class="icon-edit"></i></a></li>
                        <li><a id="{{$searchData[0]['id']}}_new_{{$merchant}}_watch_price_delete_m" href="javascript:void(0);"
                               class="delete_form" style="display: none"><i class="icon-delet"></i></a></li>
                        @endif
                    </ul>

                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
</div>
</article>
</div>
</section>
<section class="price-history  row">
    <article class="col-md-12">
        <header>
            <h3><?php echo Lang::get('trans.details_price_history'); ?></h3>
        </header>

        <!-- Nav tabs -->
        <ul id="merchant_tab_history" class="nav nav-tabs">
            <li id="souq_tab" class={{$souqStatus}}><a href="#souq_tab_pane" aria-controls="souq_tab_pane" role="tab"
                                                       data-toggle="tab"><?php echo Lang::get('trans.details_souq_merchant'); ?></a>
            </li>
            <li id="others_tab" class="{{$othersStatus}}"><a href="#3rd-party-new" aria-controls="3rd-party-new"
                                                             role="tab"
                                                             data-toggle="tab"><?php echo Lang::get('trans.details_others_merchant'); ?></a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane {{$souqStatus}} row" id="souq_tab_pane">
                <div class="col-sm-6">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                <?php echo Lang::get('trans.details_type'); ?>
                            </th>
                            <th>
                                <?php echo Lang::get('trans.details_price'); ?>
                            </th>
                            @if($searchData[0]['newSouqStats']->current->value!=0)
                            <th>
                                <?php echo Lang::get('trans.details_when'); ?>
                            </th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        <?php $number = 1; ?>
                        @if($searchData[0]['newSouqStats']!=null)
                        @foreach($searchData[0]['newSouqStats'] as $key=>$price)
                        <?php $class = "";
                        if ($key == 'high') {
                            $class = 'red';
                        } elseif ($key == 'low') {
                            $class = 'green';
                        } else {
                            $class = "";
                        } ?>
                        <tr class="{{$class}}">
                            @if($key!='change_percent' && $key!='inventory')
                            <td><?php echo Lang::get('trans.details_watch_'.$key);?></td>
                            @if($price->value != '0')
                            <td>{{number_format($price->value,1)}} {{$currency}}</td>
                            <td>{{formateDate($price->date)}}</td>
                            @else
                            <td><?php echo Lang::get('trans.product_status_out_of_stock');?></td>
                            @endif
                            <?php $number += 1; ?>
                            @endif

                        </tr>

                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6 chart-div">
                    <div>
                        <img width="100%"
                             src="{{route('getChartImage',['id'=>$searchData[0]['id'],'type'=>'new','merchant'=>'souq'])}}">
                    </div>
                </div>
            </div>
            <div class="tab-pane row {{$othersStatus}}" id="3rd-party-new">
                <div class="col-sm-6">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                <?php echo Lang::get('trans.details_type'); ?>
                            </th>
                            <th>
                                <?php echo Lang::get('trans.details_price'); ?>
                            </th>
                            @if($searchData[0]['newOthersStats']->current->value!=0)
                            <th>
                                <?php echo Lang::get('trans.details_when'); ?>
                            </th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        <?php $number = 1; ?>
                        @if($searchData[0]['newOthersStats']!=null)
                        @foreach($searchData[0]['newOthersStats'] as $key=>$price)
                        <?php $class = "";
                        if ($key == 'high') {
                            $class = 'red';
                        } elseif ($key == 'low') {
                            $class = 'green';
                        } else {
                            $class = "";
                        } ?>
                        <tr class="{{$class}}">
                            @if($key!='change_percent' && $key!='inventory')
                            <td><?php echo Lang::get('trans.details_watch_'.$key);?></td>
                            @if($price->value != '0')
                            <td>{{number_format($price->value,2)}} {{$currency}}</td>
                            <td>{{formateDate($price->date)}}</td>
                            @else
                            <td><?php echo Lang::get('trans.product_status_out_of_stock'); ?></td>
                            @endif
                            <?php $number += 1; ?>
                            @endif

                        </tr>

                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6 chart-div">
                    <div>
                        <img width="100%"
                             src="{{route('getChartImage',['id'=>$searchData[0]['id'],'type'=>'new','merchant'=>'others'])}}">
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="3rd-party-used"><?php echo Lang::get('trans.details_others_merchant'); ?></div>
        </div>

    </article>
</section>
<section class="row">
    <div class="col-md-10">
        <article class="product-souq-info ">
            <header>
                <h3><?php echo Lang::get('trans.details_product_details'); ?></h3>
            </header>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#Description" aria-controls="Description" role="tab"
                                      data-toggle="tab"><?php echo Lang::get('trans.details_description'); ?></a></li>
                <li><a href="#physical-features" aria-controls="physical-features" role="tab"
                       data-toggle="tab"><?php echo Lang::get('trans.details_physical_features'); ?></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="Description">
                    <p>
                        {{$searchData[0]['description']}}
                    </p>
                </div>
                <div class="tab-pane" id="physical-features">
                    <dl class="dl-horizontal">
                        @if($searchData[0]['specification']!="")
                        @foreach($searchData[0]['specification'] as $key => $value)
                        <dt>{{$key}}</dt>

                        <dd>{{$value}}</dd>
                        <div class="clearfix"></div>
                        @endforeach
                        @endif
                    </dl>
                </div>
            </div>
        </article>
        <article class="related-products">
            <header>
                <h3><?php echo Lang::get('trans.details_related_products'); ?></h3>
            </header>


            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a id="show_related_category" href="#Category" aria-controls="Category" role="tab"
                                      data-toggle="tab"><?php echo Lang::get('trans.details_category'); ?></a></li>
                @if(!empty($relatedProductsByBrand))
                <li><a id="show_related_brand" href="#Brand" aria-controls="Brand" role="tab"
                       onclick="setResponsiveSlide()"
                       data-toggle="tab"><?php echo Lang::get('trans.details_brand'); ?></a></li>
                @endif
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="Category">

                    <ul class="list-inline products-list responsive-slide">
                        @for ($i = 0; $i < count($relatedProductsByCategory); $i++)
                        <?php
                        if ($relatedProductsByCategory[$i]['changePercent'] > 0) {
                            $chartStatus = 'icon-chart-up';
                            $color = 'red';
                        } else {
                            $chartStatus = 'icon-chart-down';
                            $color = 'green';
                        }
                        ?>
                        <li class="thumbnail">
                            <a href="{{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>$relatedProductsByCategory[$i]['item_id']])}}">
                                <figure>
                                    <img src="{{$relatedProductsByCategory[$i]['image']}}" alt="...">
                                </figure>
                                <div class="caption">
                                    <h3>{{$relatedProductsByCategory[$i]['title']}}</h3>

                                    <p>{{$relatedProductsByCategory[$i]['currentValue']}} <span>{{$currency}}</span></p>
                                    @if($relatedProductsByCategory[$i]['changePercent'] != 0 ||
                                    $relatedProductsByCategory[$i]['changePercent'] != -0)
                                    <div>
                                        <span class="old-price">{{(($relatedProductsByCategory[$i]['currentValue']))/((($relatedProductsByCategory[$i]['changePercent'])/100)+1)}}</span>
                                        <ul class="list-inline change-percent {{$color}}">
                                            <li>
                                                <span>{{ number_format($relatedProductsByCategory[$i]['changePercent'], 1)}}%</span>
                                            </li>
                                            <li>
                                                <i class="{{$chartStatus}}"></i>
                                            </li>
                                        </ul>
                                    </div>

                                    @endif
                                </div>
                            </a>
                        </li>
                        @endfor
                    </ul>

                </div>
                @if(!empty($relatedProductsByBrand))
                <div class="tab-pane" id="Brand">
                    <ul class="list-inline products-list ">

                        @for ($i = 0; $i < count($relatedProductsByBrand); $i++)
                        <?php
                        if ($relatedProductsByBrand[$i]['changePercent'] > 0) {
                            $chartStatus = 'icon-chart-up';
                            $color = 'red';
                        } else {
                            $chartStatus = 'icon-chart-down';
                            $color = 'green';

                        }
                        ?>
                        <li class="thumbnail">
                            <a href="{{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>$relatedProductsByBrand[$i]['item_id']])}}">
                                <figure>
                                    <img id="product_image" src="{{$relatedProductsByBrand[$i]['image']}}" alt="...">
                                </figure>
                                <div class="caption">
                                    <h3>{{$relatedProductsByBrand[$i]['title']}}</h3>

                                    <p>{{$relatedProductsByBrand[$i]['currentValue']}} <span>{{$currency}}</span></p>
                                    @if($relatedProductsByBrand[$i]['changePercent'] != 0 ||
                                    $relatedProductsByBrand[$i]['changePercent'] != -0)
                                    <div>
                                        <span class="old-price">{{number_format((($relatedProductsByBrand[$i]['currentValue']))/((($relatedProductsByBrand[$i]['changePercent'])/100)+1),2)}}</span>
                                        <ul class="list-inline change-percent {{$color}}">
                                            <li>
                                                <span>{{ number_format($relatedProductsByBrand[$i]['changePercent'], 1)}}%</span>
                                            </li>
                                            <li>
                                                <i class="{{$chartStatus}}"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    @endif
                                </div>
                            </a>
                        </li>
                        @endfor
                    </ul>
                </div>
                @endif
        </article>
    </div>
    <aside class="col-md-2 hidden-sm hidden-xs banner-indetails">
        {{ Html::image('img/160x600.png') }}
    </aside>

</section>

</div>
@else
<div>No products found</div>
@endif


@endsection

