@extends('layouts.base')
@section('style')
@parent
@endsection

@section('countries')
@parent
@endsection
@section('content')
<input id="get_home_page_category_data_url"type="hidden" value="{{route('getHomePageCategory',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}">
<div class="cover-image">
    <div class="container-fluid ">
        <div class="row">
            <div class="col-md-7 col-sm-6">
                <h2 class="animated wow fadeInLeft"><b><?php echo Lang::get('trans.home_page_Why');?></b> <?php echo Lang::get('trans.home_page_Kanbkam');?></h2>
                <p class="animated wow fadeInLeft">
                    <?php echo Lang::get('trans.home_page_description_of_site');?>
                </p>
            </div>
            <div class="col-md-5 col-sm-6 kanbkam-service hidden-xs">
                <ul class="list-inline">
                    <li class="wow fadeInRight animated">
                        {{ Html::image('img/header-souq-logo.png') }}
                    </li>
                    <li class="wow fadeInDown animated">
                        {{ HTML::image('img/alert-price-drop.png') }}
                    </li>
                    <li class="wow fadeInUp animated">
                        {{ HTML::image('img/charts-price-history.png') }}
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid content">
    <div class="row">
        <div class="col-md-12 categ-tabs wow fadeIn animated">
            <!-- Nav tabs -->
            <ul id="ul_home_page_category" class="nav nav-tabs">
                @foreach(Config::get('constants.homePageCategories') as $key=>$category)


                <li class="<?php if($key==$defaultCategory)echo "active" ?> category_homepage"><a category={{$key}} href="javascript:void(0);">{{$category[$local]}}</a></li>
                @endforeach
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                
                <div id="loading_image" class="loading-products">
                    {{ Html::image('img/loader.gif') }}
               </div>
                <div role="tabpanel" class="tab-pane active"  style="display: none;"  id="category_data">
                    @include('partials.homePageCategoryData')
                </div>
            </div>

        </div>
    </div>
    <div class="row section wow fadeIn animated">
        <div class="col-md-12">
            <h3>
                <?php echo Lang::get('trans.layout_popular');?>
            </h3>
            <ul class="list-unstyled products-list">
                @foreach($productsOrderedBYViews as $product_view)
                <?php if($product_view['changePercent']>0){$chartStatus='icon-chart-up'; $color='red';}else{$chartStatus='icon-chart-down'; $color='green';} ?>
                <li class="thumbnail col-md-2 col-sm-4 col-xs-6">
                    <ul class="list-inline actions">
                        <li>
                            <a target="_blank" href="{{$product_view['souqLink']}}/io/"><i class="icon-alert"></i> <i class="plus">+</i></a>
                        </li>
                        <li>
<!--                            <a href="{{route('search',['country'=>Request::segment(1),'local'=>Request::segment(2),'search_text'=>$product_view['item_id'] ])}}"><i class="icon-add-to-cart"></i></a>-->
                            <a href="{{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>$product_view['item_id'] ])}}#{{$product_view['id']}}_new_souq_price_desired"><i class="icon-add-to-cart"></i></a>

                        </li>
                    </ul>

<!--                        <a href="{{route('search',['country'=>Request::segment(1),'local'=>Request::segment(2),'search_text'=>$product_view['item_id'] ])}}">-->
                        <a href="{{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>$product_view['item_id'] ])}}">

                        <figure>
                            <img src="{{$product_view['image']}}" alt="...">
                        </figure>

                        <div class="caption">
                            <h3>{{$product_view['fullTitle']}}</h3>
                            @if($product_view['currentValue'] !== '1')
                            <p>
                                {{$product_view['currentValue']}} <span> {{$currency}}</span></p>

                            @if($product_view['changePercent']!=0)
                            <div>
                                <span class="old-price">{{number_format((($product_view['currentValue']))/((($product_view['changePercent'])/100)+1),1)}}</span>
                                <ul class="list-inline change-percent {{$color}}">
                                    <li>
                                        <span>{{number_format($product_view['changePercent'],1)}}%</span>
                                    </li>
                                    <li>
                                        <i class="{{$chartStatus}}"></i>
                                    </li>
                                </ul>
                            </div>
                            @endif
                            @else
                            <div><?php echo Lang::get('trans.product_status_out_of_stock'); ?></div>
                            @endif
                        </div>

                    </a>
                </li>
                @endforeach

            </ul>
            <div class="clearfix"></div>
            <div class="more-row">
                <a class="more" href="{{route('index',['country'=>Request::segment(1),'local'=>Request::segment(2),'category'=>'popular'])}}"><?php echo Lang::get('trans.home_page_viewAll');?></a>
            </div>

        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
        //    $('#loading_image').show();
        // $('#category_data').hide();
    $('document').ready(function(){
        new WOW().init();

           $('#loading_image').hide();
           $('#category_data').show();
           var local="{{Request::segment(2)}}";
            window.rtl =false;
            if(local=='ar')
            {
                window.rtl=true;
            }

       $('.responsive-slide').slick({
          dots: false,
          infinite: false,
          speed: 300,
          slidesToShow: 5,
          slidesToScroll: 5,
          rtl:window.rtl,
          responsive: [
            {
              breakpoint: 960,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3

              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            }
          ]
        });


    });


</script>
{{ Html::script('js/homepage.js'.'?v='.Config::get('constants.assets_cache')) }}
@endsection

