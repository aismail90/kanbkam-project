@extends('products.default')
@section('title', 'list products')
@section('scripts')
@parent
@endsection
@section('style')
    @parent
@endsection
@section('countries')
@parent
@endsection
@section('content')
@if(!empty($items->items()))
<div class="row">
@foreach($items as $product)
<div class="col-lg-4">
    <img  src="{{$product['image']}}" alt="Generic placeholder image" width="140" height="140">
    <h2 title="{{$product['fullTitle']}}"><a href="{{ route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'id'=>$product['item_id']]) }}" style="text-decoration:none;">{{ $product['title'] }}</a></h2>
    <p>{{ $product['description'] }}</p>
    @if($product['souqCurrentValue']!=0)
    <p>souq current price : {{$product['souqCurrentValue']}} {{$product['currency']}}</p>
    @else
    <p>souq current price : out of stock</p>
    @endif
    @if($product['othersCurrentValue']!=0)
    <p>others current price : {{$product['othersCurrentValue']}}{{$product['currency']}}</p>
    @else
    <p> others current price: out of stock </p>
    @endif
    <p><a class="btn btn-default" href="{{$product['souqLink']}}" role="button">View details &raquo;</a></p>
    @if(!empty($watchData))
    @foreach($watchData as $data)
    @if($data->product == $product['id'])
    <?php $new= checkProperty('new',$data->watch);if($new!=""){$souq=checkProperty('souq',$data->watch->new);$others=checkProperty('others',$data->watch->new);} else{$souq="" ; $others=""; }  ?>
    @if($souq!="")
    <p> your souq watch value :{{$souq->value}}</p>
    @endif
    @if($others!="")
    <p> your others watch value :{{$others->value}}</p>
    @endif
    @endif
    @endforeach
    @endif
</div><!-- /.col-lg-4 -->

@endforeach

</div>
<div class="row">
    {!! $items->render() !!}
</div>

@else
<p>No Products to show</p>
@endif
@endsection

