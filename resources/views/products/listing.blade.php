@extends('layouts.base')
@section('style')
@parent

@endsection

@section('countries')
@parent
@endsection
@section('content')
<input type="hidden" id="current_url" value="{{route(Request::route()->getName(),$parameters)}}<?php if (Request::getQueryString()) echo "?" ?>{{Request::getQueryString()}}">
<input type="hidden" id="current_query_string" value="{{Request::getQueryString()}}">
<?php
$jsonParameters = json_encode(Request::route()->parameters());
?>
<input type="hidden" id="current_url_string" value="{{route(Request::route()->getName(),$parameters)}}">
<input type="hidden" id="url_load_data" value="{{route('getFilterData')}}">
<input type="hidden" id="url_parameters" value="{{$jsonParameters}}">
<div class="container-fluid content ">

    @if($items!=null)
    <div class="row">
    @endif
        <?php $queryString= Request::getQueryString(); parse_str($queryString,$output); ?>

        @if($items!=null||array_has($output, 'filter_category') ||array_has($output, 'price_drop')||array_has($output, 'price_from')||array_has($output, 'price_to')||array_has($output, 'brand')||array_has($output, 'seller')||array_has($output, 'seller')||array_has($output, 'target') )
        <aside class="col-md-3 hidden-sm hidden-xs filter">
            @include('partials.filteration',['category_id' => "category" , 'brand_id'=>'brand' ,'search_brand'=>'search_brand' ,'filter_price_from'=>'filter_price_from','filter_price_to'=>'filter_price_to','form_price_range'=>'form_price_range',
            'price_range_id' => "price_range_id",'price_drop_id' => "price_drop_id",'seller_id' => "seller_id"
            ])

        </aside>
        @endif



        @if($items!=null)
        <?php $searchTypes=Config::get('constants.searchSortTypes');?>
        <section class="col-md-9 search-result">
            <header>
                <h2>
                    @if(Request::route()->getName()=='search')
                    <?php if(isset($searchText)) echo $searchText;else echo"" ?>

                    @else
                    {{$categoryText}}
                    @endif
                </h2>



                @if(Request::route()->getName() == 'search')
                <input id="current_route" type="hidden" name="search" value="{{route(Request::route()->getName(),['country'=>Request::segment(1),'local'=>Request::segment(2)])}}?search_text={{$_GET['search_text']}}">
                @else
                <input id="current_route" type="hidden"  name="list" value="{{route(Request::route()->getName(),['country'=>Request::segment(1),'local'=>Request::segment(2),'category'=>Request::segment(3)])}}">
                {{array_forget($searchTypes,'index_relevant')}}
                @endif

                <form class="sort-by">
                    <select class="form-control select2" id="sort">
                        @foreach($searchTypes as $key=>$value)
                        <?php  $selectedValue=''; if($value == $selected) $selectedValue='selected'; ?>
                        <option sort-by="{{$value}}" {{$selectedValue}} >
                        <?php echo Lang::get('trans.'.$key.'');?>
                        </option>
                        @endforeach
                    </select>
                    <a class="btn btn-default visible-xs visible-sm" role="button" data-toggle="collapse" href="#filter-xs" aria-expanded="false" aria-controls="filteration">
                        <i class="glyphicon glyphicon-filter"></i>
                    </a>
                </form>
                <div class="clearfix"></div>





            </header>
            @endif
            @if($items!=null||array_has($output, 'filter_category') ||array_has($output, 'price_drop')||array_has($output, 'price_from')||array_has($output, 'price_to')||array_has($output, 'brand')||array_has($output, 'seller')||array_has($output, 'seller')||array_has($output, 'target') )
                <div class="collapse " id="filter-xs">
                    <div class="filter-xs-main visible-xs visible-sm">
                        <!--             <aside class="filter"> aside filer in mobile -->

                        <aside class="filter">
                            @include('partials.filteration',['category_id' => "m_category" , 'brand_id'=>'m_brand','search_brand'=>'m_search_brand','filter_price_from'=>'m_filter_price_from','filter_price_to'=>'m_filter_price_to','form_price_range'=>'m_form_price_range',
                            'price_range_id' => "m_price_range_id",'price_drop_id' => "m_price_drop_id",'seller_id' => "m_seller_id"

                            ])

                        </aside>

                    </div>
                </div>
            @endif

            @if($items!=null)
            <ul class="list-unstyled products-list" id="product_data">
                @foreach($items as $product)
                <?php if($product['currentValue'] != '1')if($product['changePercent']>0){$chartStatus='icon-chart-up'; $color='red';}else{$chartStatus='icon-chart-down';$color='green';} ?>
                <li class="thumbnail col-md-3 col-sm-4 col-xs-6">
                    <ul class="list-inline actions">
                        <li>
                            <a href="{{$product['souqLink']}}/i/"><i class="icon-alert"></i> <i class="plus">+</i></a>
                        </li>
                        <li>
                            <a href="{{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>$product['item_id'] ])}}#{{$product['id']}}_new_souq_price_desired"><i class="icon-add-to-cart"></i></a>
<!--                            <a href="{{route('search',['country'=>Request::segment(1),'local'=>Request::segment(2),'search_text'=>$product['item_id'] ])}}"><i class="icon-add-to-cart"></i></a>-->
                        </li>
                    </ul>
<!--                    <a href="{{route('search',['country'=>Request::segment(1),'local'=>Request::segment(2),'search_text'=>$product['item_id'] ])}}">-->
                        <a href="{{route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'number'=>$product['item_id'] ])}}">
                        <figure>
                            @if($product['image'] && $product['image'] != "/")
                            <img src="{{$product['image']}}"/>
                            @else
                            {{ Html::image('img/no-img-Product.png') }}
                            @endif

                        </figure>

                        <div class="caption">
                            <h3>{{$product['fullTitle']}}</h3>
                            @if($product['currentValue'] !== "1")
                            <p>{{$product['currentValue']}} <span>{{$currency}}</span></p>
                            @if($product['changePercent']!=0)
                            <div>
                                <span class="old-price">{{number_format((($product['currentValue']))/((($product['changePercent'])/100)+1),2)}}</span>
                                <ul class="list-inline change-percent {{$color}}">
                                    <li>
                                        <span>{{number_format($product['changePercent'],2)}}</span>
                                    </li>
                                    <li>
                                        <i class="{{$chartStatus}}"></i>
                                    </li>
                                </ul>
                            </div>
                            @endif
                            @else
                            <div><?php echo Lang::get('trans.product_status_out_of_stock'); ?></div>
                            @endif
                        </div>

                    </a>
                </li>
                @endforeach
                <div class="clearfix"></div>
            </ul>

            <div id="products_loading_image" class="loading ow fadeInUp animated" style="display: none;">
                {{ Html::image('img/loader.gif') }}
            </div>

            <!--<div class="load-more-btn">-->
            <!--    <a href="#" class="btn btn-default">Load More</a>-->
            <!--</div>-->

        </section>
    </div>
    @else
    <section class="col-md-9 search-result">
        <div class="no-data">
            <!--            <span>{{ Html::image('img/no-search-result.svg') }}</span>-->
            <span>{{ Html::image('img/no-search-result.svg') }}</span>
            <h4><?php echo Lang::get('trans.list_no_products_found');?></h4>
            <p>
                <?php echo Lang::get('trans.list_go_back_or_try_different_keyword');?>
            </p>
        </div>
    </section>
@endif
</div>

<!--<aside class="col-md-2 banner in-listing hidden-sm hidden-xs wow fadeIn animated">-->
<!--    {{ Html::image('img/160x600.png') }}-->
<!--</aside>-->
</div>


@endsection

@section('scripts')
@parent
<script type="text/javascript">
    $(document).ready(function() {
        $(".select2").select2({
            minimumResultsForSearch: -1
        });
    });
</script>
{{ Html::script('js/listing.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/rangeslider.min.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/search_filteration.js'.'?v='.Config::get('constants.assets_cache')) }}
{{ Html::script('js/loading_data.js'.'?v='.Config::get('constants.assets_cache')) }}
<script type="text/javascript">
    var valueBubble = '<output class="rangeslider__value-bubble" />';
//    var sliderText = "'"+@lang('trans.list_more_than')+"'" ;
    var sliderText = "{{ trans('trans.list_more_than') }}";
    var local= "{{Request::segment(2)}}" ;
    function updateValueBubble(pos, value, context) {
        pos = pos || context.position;
        value = value || context.value;
        var $valueBubble = $('.rangeslider__value-bubble', context.$range);
        var tempPosition = pos + context.grabPos;
        var position = (tempPosition <= context.handleDimension) ? context.handleDimension : (tempPosition >= context.maxHandlePos) ? context.maxHandlePos : tempPosition;

        if ($valueBubble.length) {
            $valueBubble[0].style.left = Math.ceil(position) + 'px' ;
            $valueBubble[0].innerHTML = sliderText+" "+value+"%";

        }
    }

    $('input[type="range"]').rangeslider({
        polyfill: false,
        onInit: function() {
            this.$range.append($(valueBubble));
            updateValueBubble(null, null, this);
        },
        onSlide: function(pos, value) {
            updateValueBubble(pos, value, this);
        }
    });
</script>
@endsection