@extends('products.default')
@section('title', 'list products')
@section('scripts')
    @parent

@endsection
@section('style')
@parent
{{ Html::style('css/fomlogin.css') }}
@endsection
@section('content')
    <form class="form-signin" method="post" action="{{ route('authenticate',['url'=>$url]) }}">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputName" class="sr-only">Name</label>
        <input type="text" id="inputName" name="name" class="form-control" placeholder="user name" required="" autofocus="">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="">
        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
        <button id="my_sumbit_button" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
@endsection
