@extends('products.default')
@section('title', 'list products')
@section('scripts')
@parent
@endsection
@section('style')
@parent
@endsection
@section('content')
@if(!empty($items))
<div class="row">
    @foreach($items as $product)
    <div class="col-lg-4">
        <img  src="{{$product['image']}}" alt="Generic placeholder image" width="140" height="140">
        <h2><a href="{{ route('product.show',['country'=>Request::segment(1),'local'=>Request::segment(2),'id'=>$product['item_id']]) }}" style="text-decoration:none;">{{ $product['title'] }}</a></h2>
        <p>{{ $product['description'] }}</p>
        <p>souq current price : {{$product['souqCurrentValue']}} {{$product['currency']}}</p>
        <p>others current price : {{$product['souqCurrentValue']}}{{$product['currency']}}</p>
        <p><a class="btn btn-default" href="{{$product['souqLink']}}" role="button">View details &raquo;</a></p>
    </div><!-- /.col-lg-4 -->
    @endforeach

</div>
<div class="row">
    {!! $items->render() !!}
</div>
@else
<p>No Products to show</p>
@endif
@endsection

