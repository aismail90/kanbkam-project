@extends('products.default')
@section('title', 'product details')
@section('scripts')
@parent
<script>
    $('document').ready(function(){

    })
</script>
{{ Html::script('js/highcharts.js') }}
{{ Html::script('js/exporting.js') }}
{{ Html::script('js/chart.js') }}
@endsection
@section('style')
    @parent
{{ Html::style('css/show.css') }}
@endsection
@section('content')
@if(count($searchData))
<?php $local = Request::segment(2);?>
<input id="ajax_url_watch_form" type="hidden" value="{{route('addWatchValues')}}">
<input id="ajax_url_delete_watch_form" type="hidden" value="{{route('deleteWatchValues')}}">

<div class="row featurette">
    <div class="col-md-7 col-md-push-5">
        <h2 class="featurette-heading"><a href="{{$searchData[0]['souqLink']}}" style="text-decoration:none;">{{ $searchData[0]['fullTitle'] }}</a></h2>

        <div class="vip-product-info">
            <section class="price-messaging">
                <div class="text-default price-container">
                    <h3 class="price">
                        <small></small>
                        @if($searchData[0]['souqCurrentValue'] != '0')
                        {{$searchData[0]['souqCurrentValue']}}  &nbsp;<span class="currency-text">{{$searchData[0]['currency']}}</span>&nbsp;
                        @elseif($searchData[0]['othersCurrentValue']!='0')
                        {{$searchData[0]['othersCurrentValue']}} &nbsp;<span class="currency-text">{{$searchData[0]['currency']}} </span>&nbsp;
                        @else
                        out of stock
                        @endif
                    </h3>
                </div>
            </section>    <!-- Product Info Widget  -->

            <div class="item-details-mini">

                    <dl class="stats {{$local}} ">
                    @if($searchData[0]['specification']!="")
                    @foreach($searchData[0]['specification'] as $key => $value)
                    <dt>{{$key}}</dt>

                    <dd>{{$value}}</dd>
                    <div class="clearfix"></div>
                    @endforeach
                    @endif
                    </dl>



                <h6><strong>Description</strong></h6>
                <p class="DarkGrey">
                    {{$searchData[0]['description']}}
            </div><!-- ENDOF Product Info Widget  -->


        </div>
    </div>
    <div class="col-md-5 col-md-pull-7">
        <img class="featurette-image img-responsive center-block" src="{{$searchData[0]['image']}}"
             alt="<?php if(isset($searchData[0]['fullTitle'])) echo $searchData[0]['fullTitle']; else echo "" ?>">
    </div>



</div>
<div class="row">
<div class="panel panel-default" style="width:35%;float:left;margin-right: 306px;" >
    <!-- Default panel contents -->
    <div class="panel-heading" style="text-align: center;">souq prices</div>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Date</th>
            <th>Value</th>
        </tr>
        </thead>
        <tbody>
        <?php $number = 1 ;?>
        @if($searchData[0]['souqPrices']!=null)
        @foreach($searchData[0]['souqPrices'] as $price)
        <tr>
            <th scope="row">{{$number}}</th>
            <td>{{formateDate($price->date)}}</td>
            @if($price->value != '0')
            <td>{{$price->value}} {{$searchData[0]['currency']}}</td>
            @else
            <td>out of stock</td>
            @endif
            <?php $number += 1; ?>
        </tr>

        @endforeach
        @endif
        </tbody>
    </table>
</div>

<div class="panel panel-default" style="width:35%;float: left;">
    <!-- Default panel contents -->
    <div class="panel-heading" style="text-align: center;">other prices</div>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Date</th>
            <th>Value</th>
        </tr>
        </thead>
        <tbody>
        <?php $number = 1 ?>
        @if($searchData[0]['otherPrices']!=null)
        @foreach($searchData[0]['otherPrices'] as $price)
        <tr>
            <th scope="row">{{$number}}</th>
            <td>{{formateDate($price->date)}}</td>
            @if($price->value != '0')
            <td>{{$price->value}} {{$searchData[0]['currency']}} </td>
            @else
            <td>out of stock</td>
            @endif
            <?php $number += 1; ?>
        </tr>
        @endforeach
        @endif
        </tbody>
    </table>
</div>
</div>

<div class="row">
    <div class="panel panel-default" style="width:35%;float:left;margin-right: 306px;" >
        <!-- Default panel contents -->
        <div class="panel-heading" style="text-align: center;">souq inventory</div>
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Value</th>
            </tr>
            </thead>
            <tbody>
            <?php $number = 1; ?>
            @if($searchData[0]['souqInventory']!=null)
            @foreach($searchData[0]['souqInventory'] as $price)
            <tr>
                <th scope="row">{{$number}}</th>
                <td>{{formateDate($price->date)}}</td>
                @if($price->value != '0')
                <td>{{$price->value}} </td>
                @else
                <td>out of stock</td>
                @endif
                <?php $number += 1; ?>
            </tr>

            @endforeach
            @endif
            </tbody>
        </table>
    </div>

    <div class="panel panel-default" style="width:35%;float: left;">
        <!-- Default panel contents -->
        <div class="panel-heading" style="text-align: center;">others inventory</div>
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Value</th>
            </tr>
            </thead>
            <tbody>
            <?php $number1 = 1; ?>
            @if($searchData[0]['othersInventory'] !=null)
            @foreach($searchData[0]['othersInventory'] as $price)
                <th scope="row">{{$number1}}</th>
                <td>{{formateDate($price->date)}}</td>
                @if($price->value != '0')
                <td>{{$price->value}}</td>
                @else
                <td>out of stock</td>
                @endif
                <?php $number1 += 1; ?>
            </tr>
            @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
    <div class="row">
        <div class="panel panel-default" style="width:35%;float: left;margin-right: 306px;">
            <div class="panel-heading" style="text-align: center;" > souq  summary</div>
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Value</th>
                    <th>watch</th>
                </tr>
                </thead>
                <tbody>
                @if($searchData[0]['newSouqStats']!=null)
                @foreach($searchData[0]['newSouqStats'] as $key => $value)
                <tr>
                    @if($key!='change_percent' && $key != 'inventory')
                    <th scope="row">{{$key}}</th>
                    <td>{{formateDate($value->date)}}</td>
                    <td>
                    @if($value->value != '0')
                    <span id="new-souq_current_value_price" >{{$value->value}}</span> <span>{{$searchData[0]['currency']}}</span>
                     @else <span id="new-souq_current_value_price">out of stock</span> @endif
                    </td>
                    @if($key=='current')
                    <td>
                        <?php $watched = false ;if(!empty($watchData)&& property_exists($watchData,'new') && property_exists($watchData->new,'souq')) $watched=true; $souqcurrentValue =$searchData[0]['newSouqStats']->current;?>
                        <a id="new-souq-create-price-watch" style="<?php if(!$watched) echo "display:block;" ; else echo "display:none;";?>">create</a>
                        <form id="new-souq-watch_price_form" class="iform watch_form"  method="post" action=""
                               style="<?php if($watched) echo "display:block;" ; else echo "display:none;" ;?>">
                            {{ csrf_field() }}
                            <input class="iform" type="text" pattern="[0-9]*" name="price_desired" id="new-souq-price_desired" value="<?php if($watched) echo($watchData->new->souq->value) ?>">
                            <input type="hidden" name="product_id" id="souq-others-product_id" value="{{$searchData[0]['id']}}">
                            <input type="hidden" name="merchant" id="new-souq-merchant_type" value="souq">
                            <input type="hidden" name="type" id="new-souq-merchant_type" value="new">
                            <input type="hidden" name="current_value" id="new_souq-current_value" value="{{$souqcurrentValue->value}}">
                            <button id="new-souq-submit_watch_souq_form">submit</button>
                        </form>

                        <form id="new-souq-delete-watch-price" class="delete_form" method="post" action="" style="<?php if($watched) echo "display:block;" ; else echo "display:none;" ;?>">
                            {{ csrf_field() }}
                            <input type="hidden" name="product_id" id="new-souq-product_id" value="{{$searchData[0]['id']}}">
                            <input type="hidden" name="merchant" id="new-souq-merchant" value="souq">
                            <input type="hidden" name="type" id="new-souq_type" value="new">
                            <button id="new-souq-submit_delete_souq_form">delete</button>
                        </form>

                    </td>
                    @endif
                    @endif

                </tr>
                @endforeach
                @endif
                <tr>
                    <th>inventory</th>
                    <td></td>
                    @if($searchData[0]['statsSouqInventory'] != '0')
                    <td>{{$searchData[0]['statsSouqInventory']}}</td>
                    @else
                    <td>out of stock</td>
                    @endif
                </tr>


                </tbody>
            </table>
        </div>
        <div class="panel panel-default" style="width:35%;float: left;">
            <div class="panel-heading" style="text-align: center;" > others  summary</div>
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Value</th>
                </tr>
                </thead>
                <tbody>

                @if($searchData[0]['newOthersStats'] != null)
                @foreach($searchData[0]['newOthersStats'] as $key => $value)

                <tr>
                    @if($key!='change_percent' && $key != 'inventory')
                    <th scope="row">{{$key}}</th>
                    <td>{{formateDate($value->date)}}</td>
                    <td>
                    @if($value->value != '0')
                      <span id="new-others_current_value_price">{{$value->value}}</span> <span>{{$searchData[0]['currency']}}</span>
                    @else
                        <spna id="new-others_current_value_price">out of stock</spna>
                    </td>
                    @endif
                    @if($key=='current')
                    <td>
                        <?php $watched = false ;if(!empty($watchData) && property_exists($watchData,'new') && property_exists($watchData->new,'others') ) $watched=true; $otherscurrentValue =$searchData[0]['newSouqStats']->current; ?>
                        <a id="new-others-create-price-watch" style="<?php if(!$watched) echo "display:block;" ; else echo "display:none;" ;?>"> create</a>
                        <form id="new-others-watch_price_form" class="iform watch_form" method="post" action=""
                              style="<?php if($watched) echo "display:block;" ; else echo "display:none;" ;?>">
                            {{ csrf_field() }}
                            <input class="iform" type="text" pattern="[0-9]*"  name="price_desired" id="new-others-price_desired" value="<?php if($watched) echo($watchData->new->others->value) ?>">
                            <input type="hidden" name="product_id" id="new-others-product_id" value="{{$searchData[0]['id']}}">
                            <input type="hidden" name="merchant" id="new-others-merchant" value="others">
                            <input type="hidden" name="type" id="new-others_type" value="new">
                            <button id="new-others-submit_watch_souq_form" class="">submit</button>
                        </form>
                        <form id="new-others-delete-watch-price" class="delete_form" method="post" action="" style="<?php if($watched) echo "display:block;" ; else echo "display:none;" ;?>">
                            {{ csrf_field() }}
                            <input type="hidden" name="merchant" id="new-others-merchant" value="others">
                            <input type="hidden" name="type" id="new-others_type" value="new">
                            <input type="hidden" name="product_id" id="new-others-product_id" value="{{$searchData[0]['id']}}">
                            <input type="hidden" name="current_value" id="new_others-current_value" value="{{$otherscurrentValue->value}}">
                            <button id="new-others-submit_delete_watch_form">delete</button>
                        </form>

                    </td>
                    @endif
                    @endif

                </tr>
                @endforeach
                @endif
                <tr>
                    <th>inventory</th>
                    <td></td>
                    @if($searchData[0]['statsOthersInventory'] != '0')
                    <td>{{$searchData[0]['statsOthersInventory']}} </td>
                    @else
                    <td>out of stock</td>
                    @endif
                </tr>

                </tbody>
            </table>
        </div>

    </div>
<!---->
<div id="container_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        <hr class="featurette-divider">

@else

<p class="lead">No Data Found For this product</p>


@endif
@endsection

