@extends('products.default')
@section('title', 'list products')
@section('scripts')
@parent
@endsection
@section('style')
@parent
@endsection
@section('content')
<input id="ajax_url_update_watch_form" type="hidden" value="{{route('addWatchValues')}}">
<input id="ajax_url_delete_watch_form" type="hidden" value="{{route('deleteWatchValues')}}">
<?php
$local = Request::segment(2);
?>
@if(!empty($data))
@foreach($data as $product)
<?php
$newExistence = checkProperty('new',$product->watch);
$usedExistence = checkProperty('used',$product->watch);
if(!empty($newExistence)){
    $newWatch = (array)($product->watch->new);
}else{
    $newWatch="";
}
if(!empty($usedExistence)){
    $newOthers = (array)($product->watch->others);
}
else{
    $newOthers="";
}
//echo('<pre>');
//var_dump($newWatch);
//var_dump($newOthers);
//exit;
?>
@if(!empty($newWatch) || !empty($newOthers))
<div class="row">
<div class="panel panel-default">
    <div class="panel-heading" style="text-align: center;" >  {{$product->product->title->$local}}</div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Type</th>
                <th>Desired Price </th>
                <th>Current Price</th>
                <th>Difference</th>
                <th>conditional met</th>
            </tr>
            </thead>
            <tbody>
            @foreach(Config::get('constants.merchants') as $key =>$value)
            @foreach($value as $val)
            <tr>
                <td>{{$key}} {{$val}}</td>
                <?php
                if(property_exists($product->watch->{$key} , $val) && $product->product->stats->{$key}->{$val}->current->value != '0')
                {
                    $difference = $product->product->stats->{$key}->{$val}->current->value - $product->watch->{$key}->{$val}->value ;
                    $current = $product->product->stats->{$key}->{$val}->current->value ;
                }
                else{
                    $difference = '-';
                    $current = 'out of stock';
                }


                ?>
                <form id="{{$product->id}}_{{$key}}_{{$val}}_watch_price_form" class="iform list_watch"  method="post" action="">
                    {{ csrf_field() }}
                    <td><input class="iform numeric" type="text" pattern="[0-9]*"  name="price_desired" id="{{$product->id}}_{{$key}}_{{$val}}_price_desired" value="<?php if(property_exists($product->watch->{$key} ,$val)){ echo$product->watch->{$key}->{$val}->value ;} ?>"> </td>
                    <input type="hidden" name="userproduct_id" id="{{$product->id}}_{{$key}}_{{$val}}_id" value="{{$product->id}}">
                    <td id="{{$product->id}}_{{$key}}_{{$val}}_current">{{$current}}</td>
                    <td id="{{$product->id}}_{{$key}}_{{$val}}_difference">{{$difference}}</td>
                    <td></td>
                    <td>
                        <input class="update" id="{{$product->id}}_{{$key}}_{{$val}}_update" type="submit" name="action" value="Update" />
                        <input class="delete" id="{{$product->id}}_{{$key}}_{{$val}}_delete" type="submit" name="action" value="Delete"/>
                    </td>
                </form>




            </tr>
            @endforeach
            @endforeach
            </tbody>
        </table>
</div>
    </div>
@endif
@endforeach
@else
<div>no watch data</div>
@endif
 @endsection