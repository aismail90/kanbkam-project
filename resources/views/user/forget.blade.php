@extends('layouts.base')
@section('scripts')
@parent
{{ Html::script('js/form_validation.js') }}
@endsection
@section('style')
{{ Html::style('css/old/fomlogin.css') }}
@parent
@endsection
@section('content')


<div class="container-fluid content">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="forget-password">
                <h4 class="modal-title" id="sign-inLabel"><?php echo Lang::get('trans.forget_find_your_kanbkam_account'); ?></h4>
                <form id="user_forrget_form" class="form-validation-group" method="post" action="{{route('doForget',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="email" name="email" id="email" class="form-control register-form-control" placeholder="<?php echo Lang::get('trans.register_email_address'); ?>" value="<?php if(isset($email)) echo($email) ?>">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-default" data-dismiss="modal" type="submit"><?php echo Lang::get('trans.forget_send'); ?></button>
                    </div>

                </form>
            </div>
            @if(Session::has('message'))
            <div class="forget-password">
                <div class="check">
                    @if(Session::get('message')=='check your message for reset password link')
                    <span class="send-confirmetion"><i class="glyphicon glyphicon-thumbs-up"></i></span>
                    <h3><?php echo Lang::get('trans.forget_check_your_email'); ?></h3>
                    @else
                    <span class="send-confirmetion"><i class="glyphicon glyphicon-remove"></i></span>
                    @if(Session::get('message')=='your email not activated yet')
                    <h3><?php echo Lang::get('trans.forget_mail_not_activated'); ?></h3>
                    @else
                    <h3><?php echo Lang::get('trans.forget_mail_not_registered'); ?></h3>
                    @endif
                    @endif
                </div>
            </div>
            {{Session::forget('message')}}
            @endif



        </div>
    </div>
</div>

@endsection