@extends('products.default')
@section('title', 'list products')
@section('scripts')
    @parent
@endsection
@section('style')
@parent
{{ Html::style('css/fomlogin.css') }}
@endsection
@section('countries')
@endsection
@section('content')
<div class="container">
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title"><?php echo Lang::get('trans.layout_sign_in'); ?></div>
                <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="forgetuserinfo"><?php echo Lang::get('trans.layout_forgot_password'); ?></a></div>
            </div>

            <div style="padding-top:30px" class="panel-body" >

                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                <form id="user_login_form" class="form-horizontal form-validation-group" role="form" method="post">

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="email" type="text" class="form-control" name="email" value="" placeholder="<?php echo Lang::get('trans.register_email_address');?>">
                    </div>

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="inputPassword" type="password" class="form-control" name="password" placeholder="<?php echo Lang::get('trans.register_password');?>">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    </div

                    <div style="margin-top:10px" class="form-group">
                        <!-- Button -->

                        <div class="col-sm-12 controls">
                            <button id="my_sumbit_button" class="btn btn-success" type="submit"><?php echo Lang::get('trans.layout_sign_in'); ?></button>
                            <a id="btn-fblogin" href="auth/facebook" class="btn btn-primary"><?php echo Lang::get('trans.login_login_with_facebook'); ?></a>
                            <a id="btn-register" href="register" class="btn btn-primary"><?php echo Lang::get('trans.layout_register'); ?></a>

                        </div>
                    </div>

                </form>



            </div>
        </div>
    </div>

</div>
c<div class="alert alert-success text-center">{{ Session::get('message') }} </div>
{{Session::forget('message')}}
@endif
@endsection
