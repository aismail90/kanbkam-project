@extends('products.default')
@section('title', 'list products')
@section('scripts')
@parent
{{ Html::script('js/form_validation.js') }}
@endsection
@section('style')
@parent
{{ Html::style('css/registeration.css') }}
@endsection
@section('countries')
@endsection
@section('content')

<div class="container">

    @if(Session::has('message'))
    <div class="alert alert-success text-center">{{ Session::get('message') }} </div>
        {{Session::forget('message')}}
    @endif
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        <form  id="register-form" class="form-validation-group form-horizontal" role="form" method="POST" action="{{route('sumbit')}}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label" > Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" required="required" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" required="required" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="mobile_number" class="col-md-4 control-label">Mobile Number</label>
                                <div class="col-md-6">
                                    <input id="mobile_number" type="text" class="form-control" name="mobile_number">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required="required">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password_confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password_confirm" type="password" class="form-control" name="password_confirm" >
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button id="register_button" type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i> Register
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection