@extends('layouts.base')
@section('style')
@parent
@endsection
@section('scripts')
@parent
{{ Html::script('js/login_register_validation.js') }}
<script type="text/javascript">
    $(document).ready(function() {
        $(".select2-gender").select2({
            minimumResultsForSearch: -1,
            placeholder: "Gender"
        });
    });
</script>
@endsection
@section('content')
<div class="container-fluid content separator">
    @if(Session::has('message'))
    <div class="row">
        <div class="col-md-12">
            @if(Session::get('message')=='register_success' )
            <div class="alert alert-success alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <?php echo Lang::get('trans.register_success_message');?>  </div>
            @elseif(Session::get('message')=='update_password_success')
            <div class="alert alert-success alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <?php echo Lang::get('trans.register_update_password_success');?>  </div>
            @else
            <div class="alert alert-danger alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>{{ Session::get('message') }}</div>
            @endif

        </div>
    </div>
    {{Session::forget('message')}}
    @endif
    <div class="row">
        <div class="col-sm-6">
            <div class="register-div hidden-xs">
                <h4 class="modal-title" id="sign-inLabel"><?php echo Lang::get('trans.layout_sign_in');?></h4>
                <form id="" class="user_login_form" method="POST" action="{{route('doLogin',['country' => Request::segment(1), 'local' => Request::segment(2)])}}">
                    <div class="form-group">
                        <input  type="email"  name="email" class="form-control register-form-control" id="login_email" placeholder="Email Address">
                    </div>
                    <div class="form-group">
                        <input  type="password" name="password" class="form-control register-form-control" id="login_password" placeholder="Password">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    </div>
                    <div class="form-group">
                        <button id="my_sumbit_button" type="submit" class="btn btn-default" ><?php echo Lang::get('trans.layout_sign_in');?></button>
                    </div>
                </form>
                <div class="form-group">
                    <a href="{{route('forget-info',['country' => Request::segment(1), 'local' => Request::segment(2)])}}" class="forger-password"><?php echo Lang::get('trans.layout_forgot_password');?></a>
                </div>
                <div class="social-log">
                    <p>
                        <?php echo Lang::get('trans.layout_option_sign_in');?>
                    </p>
                    <a href="{{route('provider')}}" class="facebook"></a>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="register-div">

                <h4 class="modal-title" id="sign-inLabel"> <?php echo Lang::get('trans.register_sign_up');?> </h4>
                <form  id="register-form" class="form-validation-group" role="form" method="POST" action="{{route('doRegister',['country' => Request::segment(1), 'local' => Request::segment(2)])}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <input type="text" class="form-control register-form-control" name="first_name" placeholder="<?php echo Lang::get('trans.register_first_name');?>">
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control register-form-control" name="last_name" placeholder="<?php echo Lang::get('trans.register_last_name');?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input id="register_email" type="email" class="form-control register-form-control" name="email" placeholder="<?php echo Lang::get('trans.register_email_address');?>">
                    </div>
                    <div class="form-group">
                        <input id="password" type="password" class="form-control register-form-control" name="password" placeholder="<?php echo Lang::get('trans.register_password');?>">
                    </div>
                    <div class="form-group">
                        <input id="register_repassword" type="password" class="form-control register-form-control" name="password_confirm" placeholder="<?php echo Lang::get('trans.register_re_enter_password');?>">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default" data-dismiss="modal"><?php echo Lang::get('trans.register_sign_up_button');?></button>
                    </div>
                    </form>
                    <div class="form-group">
                        <a href="{{route('forget-info',['country' => Request::segment(1), 'local' => Request::segment(2)])}}" class="forger-password"><?php echo Lang::get('trans.layout_forgot_password');?></a>
                    </div>
                    <div class="social-log visible-xs">
                        <p>
                            <?php echo Lang::get('trans.layout_option_sign_in');?>
                        </p>
                        <a href="{{route('provider')}}" class="facebook"></a>
                    </div>



            </div>


        </div>
    </div>
</div>
@endsection