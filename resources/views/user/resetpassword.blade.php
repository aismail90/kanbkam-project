@extends('layouts.base')
@section('style')
@parent
@endsection
@section('scripts')
@parent
{{ Html::script('js/login_register_validation.js') }}
@endsection
@section('countries')
@endsection
@section('content')


<div class="col-md-4 col-md-offset-4">
    <div class="register-div">
        <h4 class="modal-title" id="sign-inLabel"><?php echo Lang::get('trans.reset_reset_password'); ?></h4>
        <form id="user_resetpassword_form" class="form-validation-group form-signin" method="post" action="{{route('resetPassword',['country'=>Request::segment(1),'local'=>Request::segment(2)])}}">
            {{ csrf_field() }}
            <div class="form-group">
                <input id="password" type="password" class="form-control register-form-control" name="password" placeholder="<?php echo Lang::get('trans.register_password'); ?>">
            </div>

            <div class="form-group">
                <input id="register_repassword" type="password" class="form-control register-form-control" name="password_confirm" placeholder="<?php echo Lang::get('trans.register_re_enter_password'); ?>">
            </div>


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button id="reset_password__button" type="submit" class="btn btn-default"">
                        <i class="fa fa-btn fa-user"></i> <?php echo Lang::get('trans.reset_reset_submit'); ?>
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>



@if(Session::has('message'))
<div class="alert alert-success text-center">{{ Session::get('message') }} </div>
{{Session::forget('message')}}
@endif
@endsection

